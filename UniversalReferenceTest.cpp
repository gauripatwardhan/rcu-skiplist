#include <iostream>
#include <string>

template <typename T>
class object {
    public:

    object() {};

    object(const object & other) : value(other.value) {
        DEBUG_LOG(INFO)<< "1 : object(const object &)" << std::endl;
    }

    object(object && other) : value(other.value) {
        DEBUG_LOG(INFO)<< "2 : object(object &&)" << std::endl;
    }

    T value;
};

template <typename T>
class object1 {
    public:

    object1() {};

    object1(const object1 & other) : value(other.value) {
        DEBUG_LOG(INFO)<< "1 : object1(const object1 &)" << std::endl;
    }

    object1(object1 && other) : value(other.value) {
        DEBUG_LOG(INFO)<< "2 : object1(object1 &&)" << std::endl;
    }

    template <typename U>
        object1(U && other) : value(other.value) {
        DEBUG_LOG(INFO)<< "3 : object1(U &&)" << std::endl;
    }

    T value;
};

int main() {

    {
        DEBUG_LOG(INFO)<< std::endl << "Invoking constructor of object NOT using Universal reference constructor." << std::endl;
        const object<int> const_obj;
        object<int> obj;

        object<int> takes_const_lval1(const_obj); //1 : without templated constructor: 1
        object<int> takes_lval1(obj); //3 : without templated constructor: 1
        object<int> takes_const_rval1(std::move(const_obj)); // 3 because we pass const object : without templated constructor: 1
        object<int> takes_non_const_rval1(std::move(obj)); // 2 : without templated constructor: 2
    }

    // Object1 has constructor with universal reference.
    {
        DEBUG_LOG(INFO)<< std::endl << std::endl << std::endl << "Invoking constructor of object USING Universal reference constructor." << std::endl << std::endl << std::endl;
        const object1<int> const_obj;
        object1<int> obj;

        object1<int> takes_const_lval1(const_obj); //1 : without templated constructor: 1
        object1<int> takes_lval1(obj); //3 : without templated constructor: 1
        object1<int> takes_const_rval1(std::move(const_obj)); // 3 because we pass const object1 : without templated constructor: 1
        object1<int> takes_non_const_rval1(std::move(obj)); // 2 : without templated constructor: 2
    }

    return 0;
}
