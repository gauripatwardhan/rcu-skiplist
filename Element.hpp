/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

#include "IsGenericPointer.hpp"
#include "IsSharedPtr.hpp"
#include "GenericPointer.hpp"
#include "Likely.hpp"
#include "LockingMetrics.hpp"
#include "Logging.hpp"
#include "OperationType.hpp"

#include <assert.h>
#include <atomic>
#include <iostream>
#include <sstream>
#include <sys/types.h>
#include <pthread.h>
#include <thread>
#include <type_traits>


/**
 * This class implements a double linked list by wrapping the payload and inserting it
 * into a Read-Copy-Update doubly linked list (DLL). The list supports find(), insert()
 * and remove() operations that are lockless independent of CPU architecture on which
 * the code is compiled to run.
 * For additional details refer to the README.txt file
 */


template <typename T, typename std::enable_if<is_shared_ptr<T>::value, void>::type* = nullptr>
class Element {
public:
	using PointerType = Element<T>*;

	template <typename Q = T>
	inline Element(Q&& payload)
	  :  payload(payload),
	     back(),
	     front(),
	     removeList(GenericPointer<PointerType, false>()),
	     operationCounter(0UL),
	     removeGenerationCount(0UL),
	     deferredRemove(false),
	     readDeferredRemove(false),
       	     lockingThreadId(0L),
	     rangeKill(false),
	     rangeStop(false),
	     uniqueRangeRemoveValue(0UL),
	     anchored(false) {
		//TODO: ensure payloads has methods <(less than) and >(greater than) with argument of same type as payload

		DASSERT(payload != nullptr);
		DASSERT(operationCounter == 0UL);
		DASSERT(removeGenerationCount <= 0UL);
		DASSERT(uniqueRangeRemoveValue == 0UL);
		DASSERT(deferredRemove == false);
		DASSERT(readDeferredRemove == false);
		DASSERT(rangeKill == false);

		/***********************************************************************
		if (not this->front.isNullPointer() && not this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::MiddleElement);
		} else if (this->front.isNullPointer() && not this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::TailElement);
		} else if (not this->front.isNullPointer() && this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::HeadElement);
		} else if (this->front.isNullPointer() && this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::SoleElement);
		}
		***********************************************************************/
	}

	/**
	 * Default constructor to create an Element. An Element when getting constructed always knows
	 * the Element it points to forward and backward.
	 *
	 * @param payload
	 * 		Payload is the object that Element wraps. The Element object gets queued into
	 * 		the DLL. Payload has methods used to detect the location where the Element that
	 * 		wraps it should be inserted into the DLL
	 *
	 * @param front
	 * 		The Element object that we point to at the time of inserting this->Element into
	 * 		the DLL. It can change over time and can be nullptr to begin with or change to
	 * 		become a nullptr afterwards. It is also possible for this->pointer to start out
	 * 		as a nullptr and then become non-nullptr afterwards.
	 *
	 * @param back
	 * 		The Element object that we point to at the time of inserting this->Element into
	 * 		the DLL. It can change over time and can be nullptr to begin with or change to
	 * 		become a nullptr afterwards. It is also possible for this->pointer to start out
	 * 		as a nullptr and then become non-nullptr afterwards.
	 */
	template <typename Q = T, typename U = GenericPointer<T, true>, typename std::enable_if<is_generic_pointer<U>::value, void>::type* = nullptr>
	inline Element(Q&& payload, U&& back, U&& front)
	  :  payload(payload),
	     back(back),
	     front(front),
	     removeList(GenericPointer<PointerType, false>()),
	     operationCounter(0UL),
	     removeGenerationCount(0UL),
	     deferredRemove(false),
	     readDeferredRemove(false),
       	     lockingThreadId(0L),
	     rangeKill(false),
	     rangeStop(false),
	     uniqueRangeRemoveValue(0UL),
	     anchored(false) {
		//TODO: ensure payloads has methods <(less than) and >(greater than) with argument of same type as payload

		DASSERT(payload != nullptr);
		DASSERT(operationCounter == 0UL);
		DASSERT(removeGenerationCount <= 0UL);
		DASSERT(uniqueRangeRemoveValue == 0UL);
		DASSERT(deferredRemove == false);
		DASSERT(readDeferredRemove == false);
		DASSERT(rangeKill == false);

		/***********************************************************************
		if (not this->front.isNullPointer() && not this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::MiddleElement);
		} else if (this->front.isNullPointer() && not this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::TailElement);
		} else if (not this->front.isNullPointer() && this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::HeadElement);
		} else if (this->front.isNullPointer() && this->back.isNullPointer()) {
			setElementPositionInDLL(ElementPosition::SoleElement);
		}
		***********************************************************************/
	} 

	template <typename Q = T, typename U = PointerType, 
		 	typename std::enable_if<std::is_same<typename std::remove_reference<U>::type, PointerType>::value, void>::type* = nullptr>
	inline Element(Q&& payload, U&& back, U&& front)
	  :  payload(payload),
	     back(back),
	     front(front),
	     operationCounter(0UL),
	     removeGenerationCount(0UL),
	     deferredRemove(false),
	     readDeferredRemove(false),
       	     lockingThreadId(0L),
	     rangeKill(false),
	     rangeStop(false),
	     uniqueRangeRemoveValue(0UL),
	     anchored(false) {
	     }

	template<bool W>
	inline static void deleteElement(GenericPointer<PointerType, W>& deleteMe) {
        	// Just call the appropriate deleteElement() methods as selected by Template specialization.
	        deleteElement(deleteMe());
		return;
	}

	template<typename Q = PointerType, typename std::enable_if<is_shared_ptr<Q>::value, void>::type* = nullptr>
	inline static void deleteElement(Q&& deleteMe) {
		deleteMe.reset();
		return;
	}

	template<typename Q = PointerType, typename std::enable_if<std::is_pointer<typename std::remove_reference<Q>::type>::value, void>::type* = nullptr>
	inline static void deleteElement(Q&& deleteMe) {
		delete(deleteMe);
		return;
	}

	template<typename Q = PointerType, bool W, typename std::enable_if<is_shared_ptr<Q>::value, void>::type* = nullptr>
	inline static Q& newElement(T& payload, GenericPointer<Q, W>& back, GenericPointer<Q, W>& front) { 
		return std::make_shared<Element<T>> (payload, back, front);
	}

	template<typename Q = PointerType, bool W, typename std::enable_if<std::is_pointer<Q>::value, void>::type* = nullptr>
	inline static Q newElement(T& payload, GenericPointer<Q, W>& back, GenericPointer<Q, W>& front) { 
		return (new Element<T>(payload, back, front));
	}

	template<typename Q = PointerType, typename std::enable_if<std::is_pointer<Q>::value, void>::type* = nullptr>
	inline static Q newElement(T& payload, Q& back, Q& front) { 
		return (new Element<T>(payload, back, front));
	}

	template<typename Q = PointerType, typename std::enable_if<is_shared_ptr<Q>::value, void>::type* = nullptr>
	inline static Q& newElement(T& payload, Q& back, Q& front) { 
		return std::make_shared<Element<T>> (payload, back, front);
	}


	/**
	 * Return payload that is wrapped in Element. Payload is always a shared_ptr.
	 */
	inline T& getPayload() {
		return payload;
	}

	/**
	 * Return the payload wrapped in Element with the assumption that it may be nullptr.
	 * The caller assumes all risk in dereferencing the return pointer.
	 */
	inline T& unsafeGetPayload() {
		return payload;
	}

	/**
	 * Compares Element payload with the argument.
	 *
	 *@param otherPayload
	 *	The payload to be compared.
	 *
	 *@return
	 * 	Return true if Element payload is greater than otherPaylaod, else false.
	 */
	template<typename V>
	inline bool payloadGreaterThan(V&& otherPayload) {
		return (*payload > otherPayload);
	}


	/**
	 * Compares payload of element with otherPayload.
	 *
	 * @param otherPayload
	 *	The payload functor that is to be compared with payload of this element
	 *
	 *@param lessThan
	 *	Return true by reference if functor < Element::payload
	 *
	 *@param greaterThan
	 *	Return true by reference if functor > Element::payload
	 */
	template<bool nonRangeType, typename V, typename std::enable_if<nonRangeType>::type* = nullptr>
	inline void comparePayloadValue(V&& otherPayload, bool& lessThan, bool& greaterThan, bool& skipElement, bool* match = nullptr,
					bool* shortenRange = nullptr) {	
		if (not payload) {
			skipElement = true;
			return;
		}
		skipElement = false;

		assert(payload);
		greaterThan = false;
		lessThan = false;
		if (match) *match = false;

		auto& myPayloadValue = *payload;

		lessThan = myPayloadValue > otherPayload;
		if (not lessThan) {
			greaterThan = myPayloadValue < otherPayload;
			if (otherPayload < 10) {	
				DEBUG_LOG(INFO) << "\t\t" << otherPayload << " greater than " << myPayloadValue << " = " << greaterThan;
			}
			if (match && not greaterThan) {
				*match = true;
			}
		}
		return;
	}

	template<bool nonRangeType, typename V, typename std::enable_if<not nonRangeType>::type* = nullptr>
	inline void comparePayloadValue(V&& otherPayload, bool& lessThan, bool& greaterThan, bool& skipElement, bool* match = nullptr,
					bool* shortenRange = nullptr) {	
		assert(match);
		if (not payload) {
			skipElement = true;
			return;
		}
		skipElement = false;

		greaterThan = false;
		lessThan = false;
		*match = false;
		auto& myPayloadValue = *payload;

		if (shortenRange) otherPayload(myPayloadValue, lessThan, greaterThan, *shortenRange);

		if (not shortenRange) otherPayload(myPayloadValue, lessThan, greaterThan);

		if (not lessThan && not greaterThan) *match = true;
		return;
	}

	/**
	 * Given a operation type and holdCount, combine them together such that they
	 * can then be CAS assigned to operationCounter.
	 *
	 * @param tag
	 * 		An enum that describes the operation that needs to be performed on this->Element.
	 *
	 * @param holdCount
	 * 		The number of operations that are on-going concurrently on this->Element.
	 */
	template <typename U, typename std::enable_if<std::is_enum<U>::value, uint64_t>::type* = nullptr>
	inline uint64_t generateOperationCount(U tag, uint32_t holdCount) {
//		DASSERT(payload);
		if (holdCount == 0) {
			return 0UL;
		}
		return (static_cast<uint64_t>(static_cast<typename std::underlying_type<U>::type>(tag)) << 32 | holdCount);
	}

	inline uint64_t generateOperationCount(uint32_t tag, uint32_t holdCount) {
		register uint64_t operation = tag;
		if (holdCount == 0) {
			return 0UL;
		}
		operation = ((operation << 32) | holdCount);
		return operation;
	}

	/**
	 * Called by ListHead::processRemove() method to determine whether this->Element
	 * is ripe enough to be reaped based on the active generation in ListHead.
	 *
	 * @return
	 * 		The generation count at the time this->Element was placed on
	 * 		ListHead::removeList. To remove element from that list, the
	 * 		current generation count has to be higher than that in the
	 * 		Element placed in ListHead::removeList.
	 */
	inline uint64_t getRemoveGenerationCount() {
		DASSERT(payload);
		uint64_t removeGenCount = removeGenerationCount;
		bool setForRemove = readDeferredRemove;
		DASSERT((removeGenCount >= 0UL && setForRemove) || (removeGenCount == 0UL && not setForRemove));
		return removeGenerationCount;
	}

	/**
	 * Called by ListHead::remove() method to set the active generation in ListHead
	 * at the time this->Element object was placed in ListHead::removeList.
	 *
	 * @param generationCount
	 * 		The generation counter value required of any thread to remove
	 * 		this->element from ListHead::removeList.
	 */
	inline void setRemoveGenerationCount(uint64_t generationCount) {
		DASSERT(payload);
		DASSERT(removeGenerationCount == 0UL);
		setDeferredRemove();
		uint64_t zerouint64 = 0UL;
		removeGenerationCount.compare_exchange_weak(zerouint64, generationCount, std::memory_order_release, std::memory_order_consume);
		DASSERT(readDeferredRemove && removeGenerationCount >= 0UL);
		return;
	}
	
	/**
	 * Get deferredRemove flag.
	 */
	inline bool getDeferredRemove() {
//		DASSERT(payload);
//		DASSERT((not deferredRemove && removeGenerationCount <= 0UL) ||
//			(deferredRemove && removeGenerationCount >= 0UL));
		return readDeferredRemove;
	}

	/**
	 * Returns removeList
	 */
	inline GenericPointer<PointerType, true>& getRemoveList() {
		return this->removeList;
	}

	/** 
	 * Changes the value of removeList pointer for this->Element.
	 * Should be called before an Element gets added to removeList
	 * of ListHead.
	 *
	 * @param pointTo
	 *      Change this->removeList to point towards pointTo
	 */
	inline void setInRemoveList(PointerType pointTo) {
		DASSERT(payload);
		removeList = pointTo;
	}

	/**
 	 * Set the rangeKill flag to true using cas.
	 */
	inline bool setRangeKill() {
		bool _false = false;
		return rangeKill.compare_exchange_weak(_false, true, std::memory_order_release, std::memory_order_consume);
	}

	/**
	 * Return value of rangeKill.
	 */
	inline bool getRangeKill() {
		return rangeKill;
	}

	/*
	 * Set a unique value on each Element targetted to be removed for a rangeRemove
	 * operation. The last Element should have stopRange set to one. This is needed
	 * so that an insert operation inserting an Element within a range getting range
	 * removed, can remove itself from the RCU-DLL.
	 */
	inline void setRangeRemoveUnique(uint64_t rangeRemoveValue, bool stopRange) {
		register uint64_t expected = 0UL;
		assert(rangeRemoveValue);
		uniqueRangeRemoveValue.compare_exchange_weak(expected, rangeRemoveValue, std::memory_order_release, std::memory_order_consume);
		assert(uniqueRangeRemoveValue);
		rangeStop = stopRange;
	}

	/*
	 * Retrieve the uniqueValue and stopRange flags set by a rangeRemove operation
	 */
	inline void getRangeRemoveUnique(uint64_t uniqueValue, bool stopRange) {
		uniqueValue = uniqueRangeRemoveValue;
		stopRange = rangeStop;
	}

	/**
	 * Return value of stopRange.
	 */
	inline bool getRangeStop () {
		return rangeStop;
	}


	/**
	 * Set stopRange to true.
	 */
	inline void setRangeStop () {
		rangeStop = true;
	}

	
	/**
	 * Retrieve uniqueValue as set by rangeRemove operation
	 */
	inline uint64_t getUniqueValue() {
		return uniqueRangeRemoveValue;
	}


	/*
	 * Clears all fields that can potentially get set on an Element when it marked
	 * for a range remove operation. Should the rangeRemove operation fail (due to
	 * lock failure) we clear the run of Element identified for rangeRemove of all
	 * rangeRemove fields.
	 */
	inline void clearRangeRemove() {
		rangeStop = false;
		uniqueRangeRemoveValue = 0UL;
		rangeKill = false;
	}
	/** 
	 * Changes the value of removeList pointer for this->Element.
	 *
	 * @param pointTo
	 *      The object this->removeList should point.
	 */
	template<bool W>
	inline void setInRemoveList(GenericPointer<PointerType, W>& pointTo) {
		removeList = pointTo;
	}

	/**
	 * Change the value of back pointer for this Element. The way back pointer gets modified in
	 * RCU DLL, we would never have to restart the algorithm for just about any reason. For the
	 * insert case, A -> (B) -> C where B is getting inserted, we want C's back to point to A.
	 * Should A.front->CAS(iexpected::C, changeTo::B) fail, we would need to restart the insert
	 * all over again and find the appropriate prev (in this case A) to issue the CAS on. Given
	 * this logic, should CAS succeed, we issue C.back->CAS(expected::A, changeTo::B) in a loop
	 * since it is expected that C.back will point to A.
	 * What could happen are interim inserts: A -> (A1) -> (B) -> (B1) -> C after A's front CAS
	 * changes to B (from C). In situation thereof, B's decision to change C's back to itself
	 * remains invariant of the addition of A1 and B1. In circumstances such, there would also
	 * exist another CAS on C's back to make it point to B1 (C.back->CAS(expect:B -> set:B1))
	 * which would be looping with CAS initiated by B (C.back->cas(expect:A -> set:B)) and also
	 * by A1 (B.back->CAS(expect:A, changeTo:A1).
	 * In the context of A1, its CAS on B's back would not loop since B's back is initialized to
	 * point to A.
	 *
	 * B's CAS on C's back point will succeed since C's back did point to A at the time B won
	 * CAS on A's front. Consequently, B's insert will succeed at C.back->CAS(expect:A, set:B).
	 * As soon as  B's CAS on C's back pointer succeeds making C's back point to B, B1's CAS on
	 * C's back pointer will also succeed (C.back->CAS(expect:B -> set:B1)).
	 *
	 * @param reference
	 *      The Element to which the back pointer of this->Element should already be pointing to.
	 *
	 * @param changeBackTo
	 *      The Element to which the back pointer of this->Element should be changed to point to.
	 *
	 * @return
	 *      If the back pointer is successfully changed then return true.
	 */
	template <typename Q = T, typename U = GenericPointer<T, true>, typename std::enable_if<is_generic_pointer<U>::value, void>::type* = nullptr>
	inline bool setBackPointer(U&& prev, PointerType changeBackTo) {
		register PointerType prevElement = prev();
		//register uint64_t counter = 0;
		while (not (back.cas(prevElement, changeBackTo))) {
			/**
			counter++;
			if (counter % SPIN_YIELD_COUNT == 0) {
				std::this_thread::yield();
			}
			**/
		}
		return true;
	}

	/**
	 * Changes back pointer of this Element by taking the expected value (prev) as a RValue.
	 *
	 *
	 * @param reference
	 *      The Element to which the back pointer of this->Element should already be pointing to.
	 *
	 * @param changeBackTo
	 *      The Element to which the back pointer of this->Element should be changed to point to.
	 *
	 * @return
	 *      If the back pointer is successfully changed then return true.
	 */
	inline bool setBackPointer(PointerType& prevPointer, PointerType& changeBackTo) {
		while (not (back.cas(prevPointer, changeBackTo))) {
		}
		return true;
	}

	/**
	 * Changes this->back to point to object given as argument. This unlike the method above does
	 * no employ CAS when changing "back". This will only be called by ListHead::remove_impl as it
	 * will not know the object "back" is pointing to, hence cannot provide the arguments for CAS.
	 *
	 * @param changeBackTo
	 *      The value to which this->back should be changed, to point to.
	 *
	 */
	template<bool W>
	inline void setBackPointer(GenericPointer<PointerType, W>& changeBackTo) {
		back = changeBackTo;
	}

	inline void setBackPointer(PointerType changeBackTo) {
		back = changeBackTo;
	}

	/**
	 * Returns a copy of the back pointer
	 */
	inline GenericPointer<PointerType, true>& getBackPointer() {
		return back;
	}

	/**
	 * Returns a copy of the Element<T> in the back pointer
	 */
	inline PointerType getBackElementPointer() {
		return back();
	}

	/**
	 * Change the value of front pointer for this Element.
	 *
	 * @param reference
	 *      The expected GenericPointer<PointerType, bool> that the current front pointer of this
	 *      Element should already be pointing to.
	 *
	 * @param changeFrontTo
	 *      The ElementT>* the front pointer of should be changed to point to.
	 *
	 * @return
	 *      If the front pointer is successfully changed then return success, else return
	 *      false.
	 */
	inline bool setFrontPointer(GenericPointer<PointerType, true>& reference, PointerType changeFrontTo) {
		return front.cas(reference, changeFrontTo);
	}

	/**
	 * Change the value of front pointer for this Element.
	 *
	 * @param reference
	 *      The expected GenericPointer<PointerType>, bool that the current front pointer of this
	 *      Element should already be pointing to, passed as rValue reference.
	 *
	 * @param changeFrontTo
	 *      The ElementT>* the front pointer of should be changed to point to.
	 *
	 * @return
	 *      If the front pointer is successfully changed then return success, else return
	 *      false.
	 */
	inline bool setFrontPointer(GenericPointer<PointerType, true>&& reference, PointerType changeFrontTo) {
		return front.cas(reference, changeFrontTo);
	}

	/**
	 * Change the value of front pointer for this->Element based on Element<T> expected.
	 *
	 * @param reference
	 *      The expected Element<T>* that the front of this Element should already be pointing
     	 *      to.
	 *
	 * @param changeFrontTo
	 *      The Element<T> that the front pointer should be changed to point to.
	 *
	 * @return
	 *      If the front pointer is successfully changed then return success, else return
	 * 	false.
	 */
	inline bool setFrontPointer(PointerType reference, PointerType changeFrontTo) {
		return front.cas(reference, changeFrontTo);
	}

	/**
	 * Changes this->front to point to object given as argument. This unlike the method above does
	 * no employ CAS when changing "front". This will only be called by ListHead::insert_impl as it
	 * reinitializes the front/back pointers before attempting an insert on a previously allocated
	 * PointerType.
	 *
	 * @param changeFrontTo
	 *      The value to which this->front should be changed, to point to.
	 *
	 */
	template<bool W>
	inline void setFrontPointer(GenericPointer<PointerType, W>& changeFrontTo) {
		front = changeFrontTo;
	}

	inline void setFrontPointer(PointerType& changeFrontTo) {
		front = changeFrontTo;
	}

	/**
	 * Returns the front pointer by reference
	 */
	inline GenericPointer<PointerType, true>& getFrontPointer() {
		return front;
	}

	/**
	 * Returns the Element pointer embedded in front pointer
	 */
	inline PointerType getFrontElementPointer() {
		return front();
	}

	/**
	 * Tries to use atomic compare and swap instruction to change the operationCounter to the
	 * new value decided by (operation << 32 | holdCount). The CAS operation may fail. If it
	 * does, we check that the element is not locked under a conflicting operation before the
	 * function retries the CAS operation.
	 *
	 * @param operation
	 * 		The operation (Insert/Remove) for which this->Element should be locked.
	 *
	 * @return
	 * 		Success if the Element was locked for the operation intended on the Element
	 * 		by the caller and false if the Element was found locked in a conflicting
	 * 		operation type. Unless Element got locked in a conflicting operation type
	 * 		this->function will retry CAS for as long as required. Retry conflicts with
	 * 		itself as well as insert operation. Insert, only with retry operation.
	 */

	inline bool lockElementForOperation(OperationType operation, LockingMetrics* metrics, bool tryStrong = false) {
		register uint32_t ongoingOperation;
		register uint32_t holdCount = 0;


		static uint64_t expectedOperation = generateOperationCount(OperationType::NoOperation, 0);
		static uint64_t expectedSingleInsert = generateOperationCount(OperationType::InsertOperation, 1);
		static uint64_t expectedInsertBack = generateOperationCount(OperationType::InsertBackOperation, 1);
		static uint64_t expectedRemoveBack = generateOperationCount(OperationType::RemoveBackOperation, 1);
		static uint64_t expectedRemoveFront = generateOperationCount(OperationType::RemoveFrontOperation, 1);

		uint64_t preOperationExpected = OperationType::NoOperation;

		uint64_t expectedOperationInsert = expectedOperation;
		register uint64_t newOperationValue = expectedSingleInsert;

		if (operation == OperationType::InsertBackOperation) {
			newOperationValue = expectedInsertBack;
		}
		if (operation == OperationType::RemoveBackOperation) {
			newOperationValue = expectedRemoveBack;
		}
		if (operation == OperationType::RemoveFrontOperation) {
			newOperationValue = expectedRemoveFront;
		}


		while (true) {
			switch (operation) {
				case OperationType::InsertOperation: {
					assert(static_cast<uint32_t>(newOperationValue & 0xffffffff) - static_cast<uint32_t>(expectedOperationInsert & 0xffffffff) == 1); 
					bool success = operationCounter.compare_exchange_weak(expectedOperationInsert, newOperationValue, std::memory_order_release, std::memory_order_consume);
					if (LIKELY(success)) {
						assert(operationCounter != 0UL && newOperationValue != 0UL);
						return success;
					}
					// We will either encounter a conflicting operation or CAS will succeed.
					// In either case the function returns and till such while we have to
					// retry.
					if (metrics) metrics->incrementCounter(operation, expectedOperationInsert);

					ongoingOperation = getOperationStatus(expectedOperationInsert, &holdCount);

					if (UNLIKELY(ongoingOperation & (OperationType::RemoveOperation | OperationType::RemoveFrontOperation))) {
						return false;
					}

					newOperationValue = ongoingOperation | OperationType::InsertOperation;

					newOperationValue = generateOperationCount(static_cast<uint32_t>(newOperationValue), holdCount+1);
					continue;
				}
				case OperationType::InsertBackOperation: {
					assert(static_cast<uint32_t>(newOperationValue & 0xffffffff) - static_cast<uint32_t>(preOperationExpected & 0xffffffff) == 1); 
					bool success = operationCounter.compare_exchange_weak(preOperationExpected, newOperationValue, std::memory_order_release, std::memory_order_consume);
					if (LIKELY(success)) {
						lockingThreadId = syscall(SYS_gettid);
						assert(operationCounter != 0UL && newOperationValue != 0UL);
						return true;
					}
					if (metrics) metrics->incrementCounter(operation, preOperationExpected);
					/* 
					 * If the element was locked for remove operation, it is guaranteed to have deferredRemove set and thus,
					 * prefectly useless for whatever operation we are currently trying to lock it for. So why bother locking
					 * it.
					 */
					if (getOperationStatus(preOperationExpected) &
						(OperationType::RemoveOperation | OperationType::RemoveBackOperation | OperationType::InsertBackOperation)) {
						return false;
					}
					ongoingOperation = getOperationStatus(preOperationExpected, &holdCount);
					newOperationValue = ongoingOperation | OperationType::InsertBackOperation;

					newOperationValue = generateOperationCount(static_cast<uint32_t>(newOperationValue), holdCount+1);

//					if (lockingThreadId) DEBUG_LOG(INFO) << "Next for insert was previosly locked by thread " << lockingThreadId;
					continue;	
				}
				case OperationType::RemoveOperation: {
					static uint64_t newOperationValue = generateOperationCount(OperationType::RemoveOperation, 1);
					register uint64_t preOperationExpected = expectedOperation;
					bool success = operationCounter.compare_exchange_weak(preOperationExpected, newOperationValue, std::memory_order_release, std::memory_order_consume);
					if (LIKELY(success)) {
						lockingThreadId = syscall(SYS_gettid);
						return true;
					}
					if (not tryStrong) return false;

					if (metrics) metrics->incrementCounter(operation, preOperationExpected);

					/* 
					 * If the element was locked for remove operation, it is guaranteed to have deferredRemove set and thus,
					 * prefectly useless for whatever operation we are currently trying to lock it for. So why bother locking
					 * it.
					 * If the element was locked for insert operation, cas on next's back pointer in ListHead::remove() is 
					 * guaranteed to fail or we may implicitly remove a newly inserted element.
					 */
					if ((getOperationStatus(preOperationExpected) == OperationType::RemoveOperation) ||
						(getOperationStatus(preOperationExpected) & OperationType::InsertOperation)) {
						return false;
					}

					continue;
				}
				case OperationType::RemoveBackOperation: {
					bool success = operationCounter.compare_exchange_weak(preOperationExpected, newOperationValue, std::memory_order_release, std::memory_order_consume);
					if (LIKELY(success)) {
						lockingThreadId = syscall(SYS_gettid);
						return true;
					}

					if (metrics) metrics->incrementCounter(operation, preOperationExpected);
					/* 
					 * If the element was locked for remove operation, it is guaranteed to have deferredRemove set and thus,
					 * prefectly useless for whatever operation we are currently trying to lock it for. So why bother locking
					 * it.
					 */
					if (getOperationStatus(preOperationExpected) &
						(OperationType::RemoveOperation | OperationType::RemoveBackOperation | OperationType::InsertBackOperation)) {
						return false;
					}

					ongoingOperation = getOperationStatus(preOperationExpected, &holdCount);
					newOperationValue = ongoingOperation | OperationType::RemoveBackOperation;

					newOperationValue = generateOperationCount(static_cast<uint32_t>(newOperationValue), holdCount+1);

					continue;
				}
				case OperationType::RemoveFrontOperation: {
					bool success = operationCounter.compare_exchange_weak(preOperationExpected, newOperationValue, std::memory_order_release, std::memory_order_consume);
					if (LIKELY(success)) {
						lockingThreadId = syscall(SYS_gettid);
						return true;
					}
					if (metrics) metrics->incrementCounter(operation, preOperationExpected);
					/* 
					 * If the element was locked for remove operation, it is guaranteed to have deferredRemove set and thus,
					 * prefectly useless for whatever operation we are currently trying to lock it for. So why bother locking
					 * it.
					 * If the element was locked for insert operation, cas on this element's front pointer in ListHead::remove() 
					 * is guaranteed to fail.
					 */
					if (getOperationStatus(preOperationExpected) == OperationType::RemoveOperation) {
						return false;
					}
					if (not tryStrong && getOperationStatus(preOperationExpected) &
						(OperationType::InsertOperation | OperationType::RemoveFrontOperation)) {
						return false;
					}

					ongoingOperation = getOperationStatus(preOperationExpected, &holdCount);
					ongoingOperation = ongoingOperation & ~(OperationType::RemoveFrontOperation|OperationType::InsertOperation);

					newOperationValue = ongoingOperation | OperationType::RemoveFrontOperation;
					preOperationExpected = ongoingOperation;

					newOperationValue = generateOperationCount(static_cast<uint32_t>(newOperationValue), holdCount+1);
					preOperationExpected = generateOperationCount(static_cast<uint32_t>(preOperationExpected), holdCount);

					continue;
				}
				case OperationType::NoOperation:
					assert(operation != OperationType::NoOperation);
					return false;
				case OperationType::InvalidOperation:
       		        		assert(operation != OperationType::InvalidOperation);
					return false;
			};
		}
		return false;
	}
	

	/**
	 * Returns the operation that the Element was locked-in at the time the method was invoked.
	 * Typically, this method only assists in debugging on static Element object since a racing
	 * insert/remove operation may potentially have been locked/unlocked the Element as soon as
	 * this method checked the OperationStstus.
	 */
	inline OperationType getElementOperationLockStatus() {
		uint32_t ongoingOperation;
		uint32_t holdCount;

		getOperationStatus(ongoingOperation, holdCount);
		switch (ongoingOperation) {

		case OperationType::NoOperation:
			return OperationType::NoOperation;

		case OperationType::InsertOperation:
			return OperationType::InsertOperation;

		case OperationType::RemoveOperation:
			return OperationType::RemoveOperation;

		case OperationType::RemoveFrontOperation:
			return OperationType::RemoveFrontOperation;

		case OperationType::RemoveBackOperation:
			return OperationType::RemoveBackOperation;

		default:
			std::cout << "All is lost. Somehow we ended up with operation lock value : " << operationTypeAsString(ongoingOperation) << " aka : " << ongoingOperation << " (only for mortals to understand)" << std::endl;
			return OperationType::InvalidOperation;
		};
	}

	/**
	 * Tries to unlock the Element by setting operationCounter to 0. If unable to do so due to 
	 * a holdCount on the Element larger that 1, then decrements the holdCount.
	 *
	 * @param operation
	 * 	The operation (Insert/Remove) that called this->method.
	 *
	 * @return
	 * 	If the Element is unlocked/holdCount is decremented, returns success. Continues to try 
	 * 	till it achieves success.
	 */
	 #define SET_VARIABLES_FOR_UNLOCKING(ongoing, condition, hCount, singleHoldLock, expectedArgument1, expectedArgument2, expectedHoldCount, expectedOperation)\
	 if (ongoing == condition) {															    \
		register uint32_t postInsert;														    \
	 	if (hCount == 2 || singleHoldLock) { 												    	    \
	 		postInsert = ongoing ^ static_cast<uint32_t>(operation); 								   	    \
	 	} else { 																    \
	 		postInsert = ongoing; 														    \
	 	} 																	    \
		postUnlockOperation = generateOperationCount(postInsert, --hCount); 									    \
		continue;																    \
	}

	#define SINGLE_OPERATION_UNLOCK(ongoing, condition, hCount, post) 										\
	if (ongoing == condition) {															\
		hCount--;																\
		post = generateOperationCount(OperationType::NoOperation, 0);										\
		continue;																\
	}

	inline bool unlockElement(OperationType operation) {
		DASSERT(((static_cast<uint32_t>(operationCounter >> 32)) & operation) != 0);
		uint32_t ongoingOperation;
		uint32_t holdCount = 1;
		uint32_t counter = 0;

		uint64_t expectedOperation = generateOperationCount(operation, holdCount);
		static uint64_t newOperation = generateOperationCount(OperationType::NoOperation, 0);
		register uint64_t postUnlockOperation = newOperation;

		while (true) {
//			DASSERT(lockingThreadId == 0L);
			if (operation != OperationType::InsertOperation) {
				lockingThreadId = 0L;
			}

			DASSERT(static_cast<uint32_t>(expectedOperation & 0xffffffff) - static_cast<uint32_t>(postUnlockOperation & 0xffffffff) == 1); 
			bool success = operationCounter.compare_exchange_weak(expectedOperation, postUnlockOperation, std::memory_order_release, std::memory_order_consume);
			if (success) {
//				DEBUG_LOG(INFO) << "Unlocked for operation " << operationTypeAsString(operation) << " and set it to " << postUnlockOperation;
				return success;
			}

			counter++;
			ongoingOperation = getOperationStatus(expectedOperation, &holdCount);

			if (ongoingOperation == OperationType::InsertOperation) {
				DASSERT(operation == ongoingOperation);
				postUnlockOperation = generateOperationCount(ongoingOperation, --holdCount);
				continue;
			}

			SET_VARIABLES_FOR_UNLOCKING(ongoingOperation, (OperationType::InsertBackOperation | OperationType::InsertOperation), holdCount, 
						(operation == OperationType::InsertBackOperation), OperationType::InsertOperation, 
						OperationType::InsertBackOperation, 1, OperationType::InsertOperation);
			/***
			if (ongoingOperation == (OperationType::InsertBackOperation | OperationType::InsertOperation)) {
				DASSERT(holdCount > 1);
				DASSERT(operation == OperationType::InsertOperation || operation == OperationType::InsertBackOperation);
				register uint32_t postInsert;
				if (operation == OperationType::InsertBackOperation || holdCount == 2) {
					postInsert = ongoingOperation  ^ static_cast<uint32_t>(operation);
				} else {
					postInsert = ongoingOperation;
				}
				postUnlockOperation = generateOperationCount(postInsert, --holdCount);
				continue;
			}
			***/

			SET_VARIABLES_FOR_UNLOCKING(ongoingOperation, (OperationType::RemoveBackOperation | OperationType::InsertOperation), holdCount, 
						(operation == OperationType::RemoveBackOperation), OperationType::InsertOperation, OperationType::RemoveBackOperation,
						1, OperationType::InsertOperation);
			/***
			if (ongoingOperation == (OperationType::RemoveBackOperation | OperationType::InsertOperation)) {
				assert(holdCount > 1);
				assert(operation == OperationType::InsertOperation || operation == OperationType::RemoveBackOperation);
				register uint32_t postInsert;
				if (operation == OperationType::RemoveBackOperation || holdCount == 2) {
					postInsert = ongoingOperation  ^ static_cast<uint32_t>(operation);
				} else {
					assert(operation == OperationType::InsertOperation);
					postInsert = ongoingOperation;
				}
				postUnlockOperation = generateOperationCount(postInsert, --holdCount);
			}
			***/

			SET_VARIABLES_FOR_UNLOCKING(ongoingOperation, (OperationType::RemoveFrontOperation | OperationType::InsertBackOperation), holdCount, 
						(true), OperationType::InsertBackOperation, OperationType::RemoveFrontOperation,
						2, true);
			/***
			if (ongoingOperation == (OperationType::RemoveFrontOperation | OperationType::InsertBackOperation)) {
				assert(holdCount == 2);
				assert(operation == OperationType::RemoveFrontOperation || operation == OperationType::InsertBackOperation);
				static uint32_t postUnlock = ongoingOperation  ^ static_cast<uint32_t>(operation);
				postUnlockOperation = generateOperationCount(postUnlock, 1);
			}
			***/

			SET_VARIABLES_FOR_UNLOCKING(ongoingOperation, (OperationType::RemoveFrontOperation | OperationType::RemoveBackOperation), holdCount, 
						(true), OperationType::RemoveBackOperation, OperationType::RemoveFrontOperation,
						2, true);
			/***
			if (ongoingOperation == (OperationType::RemoveFrontOperation | OperationType::RemoveBackOperation)) {
				assert(holdCount == 2);
				assert(operation == OperationType::RemoveFrontOperation || operation == OperationType::RemoveBackOperation);
				static uint32_t postUnlock = ongoingOperation  ^ static_cast<uint32_t>(operation);
				postUnlockOperation = generateOperationCount(postUnlock, 1);
			}
			***/

			SINGLE_OPERATION_UNLOCK(ongoingOperation, OperationType::RemoveBackOperation, holdCount, postUnlockOperation);

			SINGLE_OPERATION_UNLOCK(ongoingOperation, OperationType::RemoveFrontOperation, holdCount, postUnlockOperation);

			SINGLE_OPERATION_UNLOCK(ongoingOperation, OperationType::InsertBackOperation, holdCount, postUnlockOperation);
			/***
			if (ongoingOperation == OperationType::InsertBackOperation) {
				postUnlockOperation == generateOperationCount(OperationType::NoOperation, 0);
			}
			***/
			assert(0);
		}
	}

	/**
	 * Debugging utility to make sure an Element is locked exactly the way the caller thinks it is.
	 */

	inline bool verifyElementLockStatus(OperationType operation) {
		uint32_t ongoingOperation;
		uint32_t holdCount;
		getOperationStatus(ongoingOperation, holdCount);
		if (ongoingOperation & operation) {
			assert((ongoingOperation == OperationType::RemoveOperation && operation == OperationType::RemoveOperation) ||

				(operation == OperationType::RemoveFrontOperation &&
					(ongoingOperation & 
						(OperationType::RemoveBackOperation | OperationType::InsertBackOperation | OperationType::RemoveFrontOperation)
					 	&& (holdCount == 2 || holdCount == 1))) ||

				(operation == OperationType::InsertOperation &&
					(ongoingOperation & 
						(OperationType::InsertBackOperation | OperationType::InsertOperation | OperationType::RemoveBackOperation))) ||

				(operation == OperationType::RemoveBackOperation && (ongoingOperation & OperationType::InsertOperation) ||
					(ongoingOperation & (OperationType::RemoveBackOperation | OperationType::RemoveFrontOperation) && 
						(holdCount == 1 || holdCount == 2))) ||

				(operation == OperationType::InsertBackOperation && (ongoingOperation & OperationType::InsertOperation) ||
					(ongoingOperation & (OperationType::InsertBackOperation | OperationType::RemoveFrontOperation) && 
						(holdCount == 1 || holdCount == 2))) );



			return true;
		}
		if (ongoingOperation == operation) {			// To handle the case when operation == OperationType::NoOperation
			return true;
		}
//		DEBUG_LOG(INFO) << "Expected: " << operation << " Ongoing: " << ongoingOperation << " Payload: " << *payload;
		return false;
	}


	/**
	 * Use CAS to try and set deferredRemove flag to true.
	 *
	 * @return
	 * 	If CAS is successful return true, otherwise false.
	 */
	inline bool setDeferredRemove() {
		DASSERT(payload);
		bool _false = false;
		register bool success = deferredRemove.compare_exchange_weak(_false, true, std::memory_order_release, std::memory_order_consume);
		if (success) {
			readDeferredRemove = deferredRemove.load(std::memory_order_consume);
			doReleaseAcquire();
		}
		return success;
	}


	/**
	 * Checks the operationCounter to see if Element has been locked for insert operation.
	 *
	 *@return
	 *	If the Element has been marked for insert operation, returns true, else false is
	 *	returned.
	 */
	inline bool isLockedForInsert() {
		uint32_t operation;
		getOperationStatus(operation);
//		DEBUG_LOG(INFO) << *(payload) << " is locked for " << operationTypeAsString(operation);
		if (operation & OperationType::InsertOperation) {
			return true;
		}
		return false;
	}


	/**
	 * Sets value of Element::anchored to true.
	 *
	 * @return
	 *	True is the value was successfully changed, else false.
	 */
	inline bool setAnchoredToTrue() {
		register bool _false = false;

		return anchored.compare_exchange_weak(_false, true, std::memory_order_release, std::memory_order_consume);
	}


	/**
	 * Sets value of Element::anchored to false.
	 */
	inline void setAnchoredToFalse() {
		anchored = false;
	}


	/*
	 * Returns value of Element::anchored.
	 */
	inline bool getAnchored() {
		return anchored;
	}
		

	inline uint64_t getOperationCounter() {
		return operationCounter;
	}

	/**
	 * Changes operation counter from OperationType::RemoveOperation with hold count 1 to 
	 * OperationType::RemoveBackOperation with hold count 1.
	 */
	inline void changeRemoveLockToRemoveBack() {
		register uint64_t operation = generateOperationCount(OperationType::RemoveOperation, 1);
		static uint64_t newOperation = generateOperationCount(OperationType::RemoveBackOperation, 1);
		bool success = operationCounter.compare_exchange_weak(operation, newOperation, std::memory_order_release, std::memory_order_consume);
		if (not success) DEBUG_LOG(TRACE) << "Ongoing operation: " << operation;
		assert(success);
		return;
	}


private:

	/**
	 * Sometimes (quite often actually), we have a variable that is read very often but modified
	 * rarely. In such cases, declaring that variable as atomic costs too much when the variable
	 * is read. So, we plan to not declare such a variable as atomic but instead, each time it is
	 * changes, do a release/acquire on some unrelated variable, which guarantees that all changes
	 * to all cache lines made prior to the release/acquire become visible on all cache lines.
	 */
	inline void doReleaseAcquire() {
		std::atomic<int32_t> syncVariable;
		syncVariable.store(42, std::memory_order_release);
		register int32_t temp = syncVariable.load(std::memory_order_acquire);
	}

	/**
	 * Parse the operationCounter and tease apart the holdCount from the operation.
	 * Return the individual operation and holdCount and return them to the caller.
	 * We could have returned a std::tuple<operation, holdCount> but doing so would
	 * necessitate a call to std::make_tuple() which has to create an object, copy
	 * it as we return and then destroy it after it goes out of scope.
	 * However, nothing how getOperationStatus() gets called in critical code paths
	 * of remove/insert operations, returning the tuple would lengthen CPU code path
	 * which is not to our advantage.
	 *
	 * So we choose to take operation and holdCount as arguments taken by reference.
	 *
	 * @param operation
	 *		The value of the OperationType enum that was present at the time
	 *		getOperationStatus() was invoked.
	 *
	 * @param holdCount
	 *		The value of holdCount present at the time getOperationStatus()
	 *		was invoked.
	 */
	inline void getOperationStatus(uint32_t& operation, uint32_t& holdCount) {
		register uint64_t operationValue = operationCounter;
		holdCount = static_cast<uint32_t>(operationValue & 0xffffffff);
		operation = static_cast<uint32_t>(operationValue >> 32);
	}

	/**
	 * Parse operationCounter argument and extract the holdCount from the operation.
	 * Return the individual operation and holdCount - if provided in argument - to the caller.
	 *
	 * @return
	 * 		std::tuple<operation, holdCount> where the operation is a enum
	 * 		value from OperationType and holdCount is the actual hold count
	 * 		at the time it was read by this->function.
	 */
	inline uint32_t getOperationStatus(uint64_t operationArgument, uint32_t* holdCount = nullptr) {
		register uint64_t operationValue = operationArgument;
		register uint32_t operation = static_cast<uint32_t>(operationValue >> 32);

		if (holdCount) (*holdCount) = static_cast<uint32_t>(operationValue & 0xffffffff);

		return operation;
	}

	/**
	 * Used for debugging purposes to make sure the internal state of the Element
	 * is sane. Should be called regularly when making state changes to ensure the
	 * Element is safe to use. Can only be called when the Element is guaranteed
	 * to not being accessed by any other thread.
	 *
	 * @return
	 * 		True if the Element is sane and false otherwise
	 */
	inline bool isElementSane() {
		DASSERT(payload);
		DASSERT((readDeferredRemove && removeGenerationCount) ||
			(not readDeferredRemove && not removeGenerationCount));
		DASSERT(not removeList ||
			(removeList && readDeferredRemove && removeGenerationCount));
		DASSERT((elementPosition != ElementPosition::HeadElement) ||
			(elementPosition == ElementPosition::HeadElement && front != nullptr && back == nullptr));
		DASSERT((elementPosition != ElementPosition::MiddleElement) ||
			(elementPosition == ElementPosition::MiddleElement && front != nullptr && back != nullptr));
		DASSERT((elementPosition != ElementPosition::TailElement) ||
			(elementPosition == ElementPosition::TailElement && front == nullptr && back != nullptr));
		DASSERT((elementPosition != ElementPosition::SoleElement) ||
			(elementPosition == ElementPosition::SoleElement && front == nullptr && back == nullptr));
		return true;
	}

	enum ElementPosition {						//!< Used purely for debugging purposes to tell
		HeadElement = 1,						//!< whether this->Element was at the head, tail
		MiddleElement,							//!< or in-between the DLL at the time the front
		TailElement,							//!< or back pointers were last modified.
		SoleElement
	};

	/**
	 * Get position of this->Element in the DLL
	 */
	inline ElementPosition getElementPositionInDLL() {
		switch (elementPosition) {
			case ElementPosition::HeadElement:
				return ElementPosition::HeadElement;
			case ElementPosition::MiddleElement:
				return ElementPosition::MiddleElement;
			case ElementPosition::TailElement:
				return ElementPosition::TailElement;
			case ElementPosition::SoleElement:
				return ElementPosition::SoleElement;
		}
	}

	/**
	 * Set position of this->Element in the DLL
	 */
	inline void setElementPositionInDLL(ElementPosition position) {
		elementPosition = position;
		return;
	}

	T payload;								//!< Element<T> wraps this->payload variable
	GenericPointer<PointerType, true> front;				//!< Forward pointer of the DLL
	GenericPointer<PointerType, true> back; 				//!< Backward pointer of the DLL. Find() will

	/**
	 * Make sure this->pointer is null before proceesing to enqueue the Element in ListHead::removeList
	 */
	GenericPointer<PointerType, true> removeList;				//!< Forward pointer to be used in the singly
    										//!< linked list of Element objects queued to
    										//!< be removed at ListHead.

	std::atomic<std::uint64_t> operationCounter;				//!< This is a merged value of OperationType
    										//!< << 32 | operation-hold-count as returned
    										//!< by generateOperationCount() method.
	
	std::atomic<std::uint64_t> removeGenerationCount;			//!< Indicates the genetation of the DLL when
    										//!< the element was removed. The memory of the
    										//!< payload can only be freed by a thread from
    										//!< a higher generation.
	std::atomic_bool deferredRemove;					//!< Set to true when the element is on its way
	    									//!< to getting removed. No insert operations
	    									//!< are allowed immediately after any Element
	    									//!< marked for deferred removal.
	bool readDeferredRemove;						//!< When checking if deferredRemove is set, read this variable.

	ElementPosition elementPosition;					//!< Holds the placement position of Element
	std::atomic<std::uint64_t> lockingThreadId;				//!< Id of the thread that locked this Element for an operation
	std::atomic<bool> rangeKill;						//!< Indicates if the element was placed on removeList due to 
										//!< a remove range function call; if so this is set to true.
	std::atomic<bool> rangeStop;						//!< Indicates if this element is the last element in the run
										//!< of Elements getting removed as part of a rangeRemove. A
										//!< racing insert operation uses this flag to decide whether
										//!< it inserted the Element in the midst of a run of Elements
										//!< that are getting rangeRemoved.
	std::atomic<std::uint64_t> uniqueRangeRemoveValue;			//!< A unique value that is present on all Elements that are
										//!< part of the run of Elements getting removed by rangeRemove.
	std::atomic<bool> anchored;						//!< Indicates if the Element is present on ListHead::instermediates.
};
