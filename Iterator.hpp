/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

#include "Element.hpp"
#include "GenericPointer.hpp"
#include "IsSharedPtr.hpp"
#include "ListHead.hpp"
#include "Logging.hpp"
#include "Likely.hpp"

#include <atomic>
#include <functional>
#include <iostream>

template<typename T>
class ListHead;

template<typename T>
class Iterator {
	using PointerType = Element<T>*;
public:
	Iterator(ListHead<T>& listHead, uint64_t genCount, GenericPointer<PointerType, false> pointToMe) : 
			dontDecrementActiveCount(false),
			head(listHead),
			generationCount(genCount),
			pointer(pointToMe) {
		head.incrementActiveCount();
	}

	Iterator(Iterator<T>& pointToMe) : 
			dontDecrementActiveCount(false),
			head(pointToMe.head),
			generationCount(pointToMe.generationCount),
			pointer(pointToMe.pointer) {
		head.incrementActiveCount();
	}

	Iterator(Iterator<T>&& pointToMe) : 
			head(pointToMe.head),
			generationCount(pointToMe.generationCount),
			pointer(std::move(pointToMe.pointer)) {
		pointToMe.dontDecrementActiveCount = true;
	}

	Iterator(const Iterator<T>&& pointToMe) : 
			dontDecrementActiveCount(false),
			head(pointToMe.head),
			generationCount(pointToMe.generationCount),
			pointer(pointToMe.pointer) {
		head.incrementActiveCount();
	}

	Iterator(ListHead<T> &listHead, uint64_t genCount) :
			dontDecrementActiveCount(true),
			head(listHead),
			generationCount(genCount) {
	}

	Iterator(std::shared_ptr<ListHead<T>> listHead) :
			dontDecrementActiveCount(true),
			head(*listHead),
			dllHead(listHead) {}

	~Iterator() {
		if (not dllHead) {
			if (not dontDecrementActiveCount) {
				head.decrementActiveCount(generationCount);	
				return;
			}
		}
	}

	Iterator<T>& operator++() {
		if (pointer.isNullPointer()) {
			return *this;
		}
		pointer = pointer()->getFrontPointer();
		while(not pointer.isNullPointer() && pointer()->getDeferredRemove()) {
			pointer = pointer()->getFrontPointer();
		}
		return *this;
	}
	
	Iterator<T> operator++(int a) {
		Iterator<T> returnMe = *this;
		operator++();
		return returnMe;
	}

	template<typename Q>
	bool operator==(Q&& compareMe) {
		return (pointer == compareMe.pointer);
	}

	template<typename Q>
	bool operator!=(Q&& compareMe) {
		return (not(*this == compareMe));
	}

	const T operator*() {
		return (pointer()->getPayload());
	}

	template<typename Q>
	Iterator<T>& operator=(Q&& other) {
		head = other.head;
		generationCount = other.generationCount;
		pointer = other.pointer;
		head.incrementActiveCount();
		return *this;
	}

private:
	bool dontDecrementActiveCount;
	ListHead<T>& head;
	uint64_t generationCount;
	GenericPointer<PointerType, false> pointer;
	std::shared_ptr<ListHead<T>> dllHead;
};
