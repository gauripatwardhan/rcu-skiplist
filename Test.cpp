/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#include "IsSharedPtr.hpp"

#include <assert.h>
#include <iostream>
#include <string>
#include <string.h>

class TestForLambda {
public:
	
	TestForLambda() : testMe("false") {
		DASSERT(testMe == "false");
	}

	void testLambda() {
		std::string changeMe = "true";

		while (changeMe == "true") {
			changeMe = "false";
			std::cout << "Before calling lambda: changeMe = " << changeMe << " and testMe = " << testMe << std::endl; 
			auto changeVar = [&]() {
				changeMe = "true";
				testMe = "true";
			};
	
			changeVar();
			std::cout << "After calling lambda: changeMe = " << changeMe << " and testMe = " << testMe << std::endl; 
		}
	}

private:
	std::string testMe;
};


template<typename T, typename something=void>
class B;

template<typename T>
class B<T, typename std::enable_if<std::is_pointer<T>::value>::type> {
};

template<typename T>
class B<T, typename std::enable_if<is_shared_ptr<T>::value>::type> {
};

int main() {
	B<int *> ptr;
	B<std::shared_ptr<std::string>> sptr;
	TestForLambda lambda;
	lambda.testLambda();
	return 0;
}
