A doubly linked list (DLL) is required very frequently while storing data whether in-memory or in persistence and there are
several container options available in C++ STL that can be used for this purpose. Imagine a multi-threaded program storing
data in some STL container where find, range-find, insert, remove and range-remove operations are frequently required.
Such a program, when using STL containers would implicitly use mutexes/locks to serialize conflicting operations acting
on the STL container. The fact that the mutex causes serialization of an insert and remove operation that are not working
on the same element in the container or otherwise would, if allowed to run concurrently, run to completion without
interfering with one another, makes using the mutex a sledge hammer approach to solving the multi-threaded consistency
problem.

One can argue that using range-locks to serialize conflicting operations on a DLL can potentially solve the problem by
making find/insert/remove operations on the DLL lock free but, that is not the case. In practice, a simple range lock is not
sufficient here as we need to ensure elements in the DLL do not cross range boundaries and also need to somehow take care
of elements in the DLL that exist at range boundaries but let's skip all that for now. Assuming we have a range-lock that
can serialize conflicting operations within a range, let's analyze how the range-lock gets implemented. At a bare minimum,
the range lock needs some data structure that describes the range (start and end) and the operation type on that range.
Let's assume this data structure is of the form:

struct rangeLockEntry {
	struct rangeLockEntry *next;
	struct rangeLockEntry *prev;
	OperationType operation;
};

Each instance of rangeLockEntry describes an on-going operation on the mail DLL and itself needs to be stored in some container
while the operation on the main DLL is in progress. Once the operation on the main DLL completes, its rangeLockEntry instance
is removed from whatever container it is stored in. It can be noted that storing of the rangeLockEntry instance itself would
need locking and therefore we have only managed to shift the problem from locking the main DLL to whatever container in which
the rangeLockEntry instances are stored. So clearly range-locks do not solve the locking problem at all.


TODO:

1. Implement range-remove
2. Add complete support for iterators


Advantages of using this (RCU) lock-less DLL implementation:

0. Implemented fully as a header file only package.
	There's no need to compile and link with any library for the user program. Just install the header file package, compile it
	into your program and you are good to go.

1. No sleep, guaranteed (almost)

2. find() does not retrieve any element deleted by a prior successfull remove operation

3. find() is guaranteed to retrieve an element inserted by a successful insert() operation provided it was not removed by a prior or on-going remove() operation

4. A remove operation can do a deferred delete where the element to be removed is removed from the DLL synchronously but its memory is freed in a deferred fashion. The payload stored by the element however, is immediately removed

5. Deferred memory free caused by a remove operation requires no additional operations on the DLL from its user


