/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#include "GenericPointer.hpp"
#include "Logging.hpp"

#include <atomic>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

/*****************************************
 * SUMMARY:
 * 	Number of times GenericPointer::consturctor called = 7
 * 	Number of times GenericPointer::operator =() called = 5
 * 	Number of times GenericPointer::operator ==() called = 6
 * 	Number of times GenericPointer::operator ()() called = 3
 * 	Number of times cas called = it is called in a while loop so the exact number of times
 * 					it is called cannot be calculated
 *
 *
 *
 *
******************************************/


using PointerType = int*;

class A {
public:
	A() : value(0) {}
	A(GenericPointer<PointerType, false> arg) : pointer(arg) {}

	friend std::ostream& operator<<(std::ostream& inStream, const A& a) {
		inStream << std::to_string(reinterpret_cast<unsigned long int>(&a)) << " : " << std::to_string(a.value) << std::endl;
		return inStream;
	}

private:
	int value;
	GenericPointer<PointerType, false> pointer;
};

void foo(GenericPointer<PointerType, false> temp) {
	TEST_DEBUG_LOG(TRACE)<< "GenericPointer copy constructed from RValue reference." << std::endl;
}

void casSwap(GenericPointer<PointerType, true>* changeThis, int* expected, int* changeTo, int threadNumber) {
	while (not changeThis->cas(expected, changeTo)) {
	}
}	

int main() {
	int j = 9;
	int m = 2;
	int b = 907;
	PointerType v = &b;

	GenericPointer<PointerType, false> g1(&j);
	TEST_DEBUG_LOG(TRACE)<< "Creating GenericPointer from element address : " << "\t\tSuccess" << std::endl;

	foo(GenericPointer<PointerType, false>());
	TEST_DEBUG_LOG(TRACE)<< "Passing empty GenericPointer<T*> by const reference : " << "\tSuccess" << std::endl;

	GenericPointer<PointerType, false> g2(&m);
	TEST_DEBUG_LOG(TRACE)<< "Creating GenericPointer from element address : " << "\t\tSuccess" << std::endl;
	GenericPointer<PointerType, true> g3(&j);
	TEST_DEBUG_LOG(TRACE)<< "Creating GenericPointer from element address : " << "\t\tSuccess" << std::endl;

	GenericPointer<PointerType, true> g4(g3);
	TEST_DEBUG_LOG(TRACE)<< "Creating atomic GenericPointer from non-atomic : " << "\t\tSuccess" << std::endl;

	GenericPointer<PointerType, false> g5(g4);
	GenericPointer<PointerType, false> g6 = g4;
	TEST_DEBUG_LOG(TRACE)<< "Creating non-atomic GenericPointer from atomic : " << "\t\tSuccess" << std::endl;

	g1 = g2;
	g1 = g3;
	g3 = v;
	g2 = v;
	g3 = g2;

	if(g1 == g1 && g3 == g2 && g2 == g3 && g2() == v) {
		TEST_DEBUG_LOG(TRACE)<< "Testing operator= and operator== : " << "\t\t\t\tSuccess" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "Testing operator= and operator== : " << "\t\t\t\tFail" << std::endl;
	}

	static constexpr int NUMTHREADS = 100;

	int k = 1234;
	GenericPointer<PointerType, true> casBase(&k);
	int array[NUMTHREADS];
	for (int i = 0; i < NUMTHREADS; ++i) {
		array[i] = i;
	}
	std::thread threads[NUMTHREADS];
	for (int i = 0; i < NUMTHREADS-1; ++i) {
		threads[i] = std::thread(casSwap, &casBase, &array[NUMTHREADS-i-2], &array[NUMTHREADS-i-1], i+1);
	}

	casSwap(&casBase, &k, &array[0], 0);

	for (int i = 0; i < NUMTHREADS-1; ++i) {
		threads[i].join();
	}

	if (*(reinterpret_cast<int *>(casBase())) == NUMTHREADS-1) {
		TEST_DEBUG_LOG(TRACE)<< "CAS race between " << NUMTHREADS-1 << " threads : " << "\t\t\t\tSuccess" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "CAS race between " << NUMTHREADS-1 << " threads : " << "\t\t\t\tFailure :" << "Got : " << *(reinterpret_cast<int *>(casBase())) << "Expected : " << NUMTHREADS-1 << std::endl;
	}

	return 0;
}
