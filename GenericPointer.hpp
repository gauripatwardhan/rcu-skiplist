/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

#include "IsGenericPointer.hpp"
#include "IsSharedPtr.hpp"
#include "Logging.hpp"

#include <assert.h>
#include <atomic>
#include <iostream>

constexpr static uint64_t SPIN_YIELD_COUNT = 20;

template<typename T, bool Q>
class DeducePointerType {
public:
	using PointerType = T;
};

template<typename T>
class DeducePointerType<T, true> {
public:
	using PointerType = std::atomic<T>;
};

template<typename Q, bool R>
class GenericPointer<Q, R, typename std::enable_if<std::is_pointer<Q>::value>::type> {
public:
	using Type = typename DeducePointerType<Q, R>::PointerType;
	template<bool W = R, typename std::enable_if<!W>::type* = nullptr>
	inline GenericPointer() {
		pointer = nullptr;
	}

	template<bool W = R, typename std::enable_if<W>::type* = nullptr>
	inline GenericPointer() {
		pointer1 = nullptr;
	}

	// I am non-atomic GenericPointer and pointTo is raw pointer
	template<typename U, bool W = R, typename std::enable_if<std::is_same<typename std::remove_reference<U>::type, Q>::value && !W>::type* = nullptr>
	inline GenericPointer(U&& pointTo) : pointer(pointTo) { }

	// I am non-atomic GenericPointer and pointTo is atomic or non-atomic GenericPointer
	template<typename U, bool W = R, typename std::enable_if<is_generic_pointer<U>::value && !W>::type* = nullptr>
	inline GenericPointer(U&& pointTo) : pointer(pointTo()) {
	}

	// I am atomic GenericPointer and pointTo is atomic or non-atomic GenericPointer
	template<typename U, bool W = R, typename std::enable_if<W && is_generic_pointer<typename std::remove_cv<U>::type>::value>::type* = nullptr>
	inline GenericPointer(U&& pointTo) {
		pointer.store(pointTo(), std::memory_order_release);
		pointer1 = pointer.load(std::memory_order_consume);
	}

	// I am atomic GenericPointer and pointTo is raw pointer
	template<typename U = Q, bool W = R, typename std::enable_if<W && std::is_same<typename std::remove_reference<U>::type, Q>::value, void>::type* = nullptr>
	inline explicit GenericPointer(U&& pointTo) {
		pointer.store(pointTo, std::memory_order_release);
		pointer1 = pointer.load(std::memory_order_consume);
	}

	template<bool W = R, typename std::enable_if<not W, void>::type* = nullptr>
	inline bool isNullPointer() {
		if(pointer == nullptr) {
			return true;
		}
		return false;
	}

	template<bool W = R, typename std::enable_if<W, void>::type* = nullptr>
	inline bool isNullPointer() {
		if(pointer1 == nullptr) {
			return true;
		}
		return false;
	}

        template<bool W = R, typename std::enable_if<W, void>::type* = nullptr>
	inline Q operator() () const {
		return pointer1;
	}

        template<bool W = R, typename std::enable_if<not W, void>::type* = nullptr>
	inline Q operator() () const {
                return pointer;
	}

	//Self using atomic<Q> and U = is_generic_pointer with or without atomic<Q>
	template<typename U, bool W = R, typename S = std::remove_reference<U>, typename A = typename S::type, typename B = typename A::Type,
		 typename C = typename GenericPointerBaseType<B>::type,
		 typename std::enable_if<W && is_generic_pointer<U>::value &&
			                 std::is_same<C, Q>::value, void>::type* = nullptr>
	inline bool operator==(U&& other) {
		Q otherValue = other();
                if (otherValue == pointer.load(std::memory_order_consume)) {
                    return true;
                }
                return false;
	}

	//Self using non-atomic Q and U = is_generic_pointer with or without atomic<Q>
	template<typename U, bool W = R, typename S = std::remove_reference<U>, typename A = typename S::type, typename B = typename A::Type,
		 typename C = typename GenericPointerBaseType<B>::type,
		 typename std::enable_if<not W && is_generic_pointer<U>::value &&
			                 std::is_same<C, Q>::value, void>::type* = nullptr>
	inline bool operator==(U&& other) {
		return pointer == other();
	}

        // Self using atomic or non-atomic. Other is not a GenericPointer
	template<typename U, typename std::enable_if<is_generic_pointer<U>::value && std::is_same<Q, typename GenericPointerBaseType<typename std::remove_reference<U>::type::Type>::type>::value, void>::type* = nullptr>
	inline bool operator!=(U&& other) {
		return (not(*this == other));
	}

	//======operator(=) =================================================================================================================
	
	//Self using non-atomic and U = is_generic_pointer
	template<typename U, bool W = R, typename std::enable_if<not W && is_generic_pointer<U>::value, void>::type* = nullptr>
	inline void operator=(U&& other) {
		pointer = other();
	}

	//Self using non-atomic and U = is_pointer
	template<typename U = Q, bool W = R, typename std::enable_if<not W && std::is_same<typename std::remove_reference<U>::type, Q>::value, void>::type* = nullptr>
	inline void operator=(U&& other) {
		pointer = other;

	}

	//Self using atomic<Q> and U = is_generic_pointer
	template<typename U, bool W = R, typename std::enable_if<W && is_generic_pointer<U>::value, void>::type* = nullptr>
	inline void operator=(U&& other) {
                pointer.store(other(), std::memory_order_release);
		pointer1 = pointer.load(std::memory_order_consume);
	}

	//Self using atomic<Q> and U = is_pointer
	template<typename U = Q, bool W = R, typename std::enable_if<W && std::is_same<typename std::remove_reference<U>::type, Q>::value, void>::type* = nullptr>
	inline void operator=(U&& other) {
                pointer.store(other, std::memory_order_release);
		pointer1 = pointer.load(std::memory_order_consume);
	}

	//======CAS ========================================================================================================================

	// Self using atomic<Q>
	template<bool W = R, bool D = true, bool H = true, typename std::enable_if<W, void>::type* = nullptr>
	inline bool cas(GenericPointer<Q, D>& changeFromThis, GenericPointer<Q, H>& changeTo) {
		register Q otherValue = changeFromThis();
		register bool success = pointer.compare_exchange_weak(otherValue, changeTo(), std::memory_order_release, std::memory_order_consume);
		if (success) {
			pointer1 = pointer.load(std::memory_order_consume);
		}
		return success;
	}

	// Self using atomic<Q>
	template<bool W = R, typename std::enable_if<W, void>::type* = nullptr>
	inline bool cas(Q& changeFromThis, Q& changeTo) {
		register Q from = changeFromThis;
		register bool success = pointer.compare_exchange_weak(from, changeTo, std::memory_order_release, std::memory_order_consume);
		if (success) {
			pointer1 = pointer.load(std::memory_order_consume);
		}
		return success;
	}

	// Self using atomic<Q>
	template<bool W = R, typename std::enable_if<W, void>::type* = nullptr>
	inline bool cas(GenericPointer<Q, R>& changeFromThis, Q& changeTo) {
		register Q from = changeFromThis();
		register bool success = pointer.compare_exchange_weak(from, changeTo, std::memory_order_release, std::memory_order_consume);
		if (success) {
			pointer1 = pointer.load(std::memory_order_consume);
		}
		return success;
	}

	inline void reset() {
		pointer = nullptr;
		pointer1 = nullptr;
	}
	
private:
	Type pointer;
	Q pointer1;	// Always a non-atomic. All CAS operations are on pointer and then pointer1 is initialized from pointer.
			// pointer may not have visibility effect on all L1-caches/cores but that shouldn't matter since in RCU,
			// we never throw away the pointer values from elements marked deleted and elements are never deleted 
			// till non-one is guaranteed to look at them. We further make sure that elements are removed from
			// removeList using release/acquire semantics ensuring all prior pointer1 of all GenericPointers become
			// correctly visible on all cores/L1-caches.
};
