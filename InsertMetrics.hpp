/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

#include "OperationType.hpp"
#include "Likely.hpp"

#include <atomic>
#include <iostream>
#include <string.h>
#include <sstream>
#include <vector>

enum InsertFailures {
	PrevLockFailed = 0,
	NextLockFailed,
	PrevCASFailed,
	FrontCASFailed,
	PrevDeferredRemove,
	NextDeferredRemove,
	BackCASFailed,
	ElementExists
};

std::string& insertFailuresAsString(int reason) {
	static std::string prevLockFailed("Could not lock prev");
	static std::string nextLockFailed("Could not lock next");
	static std::string prevCasFailed("CAS failed on prev->front");
	static std::string frontCasFailed("CAS failed on ListHead->front");
	static std::string prevDeferredRemove("Prev had deferred remove");
	static std::string nextDeferredRemove("Next had deferred remove");
	static std::string backCasFailed("CAS failed on next->back");
	static std::string elementExists("Element already exists");

	switch (reason) {
		case 0:
			return prevLockFailed;
		case 1:
			return nextLockFailed;
		case 2:
			return prevCasFailed;
		case 3:
			return frontCasFailed;
		case 4:
			return prevDeferredRemove;
		case 5:
			return nextDeferredRemove;
		case 6:
			return backCasFailed;
		case 7:
			return elementExists;
		default:
			assert(0);
	}
}

class InsertMetrics {
public:
	inline InsertMetrics(bool disable) : disabled(disable), armed(false), retryCount(0) {
		if (not disable) {
			size = (InsertFailures::ElementExists - InsertFailures::PrevLockFailed) + 1;
			metrics = new std::atomic<size_t>[size];
			for (int i = 0; i < size; ++i) {
				metrics[i] = 0UL;
			}
		}
	}

	/**
	 * Returns true if the object has been enabled to track metrics for failed insert operations
	 */
	inline bool operator ()() {
		return (!disabled);
	}

	/**
	 * Increments a counter that caused insert operation attempt to fail and/or retry.
	 */
	inline void incrementCounter(InsertFailures reason) {
		if (UNLIKELY((*this)())) {
			armed = true;
			metrics[reason]++;
			retryCount++;
		}
	}

	inline void printMetrics() {
		if (LIKELY(not (*this)() || not armed || retryCount < 1000)) {
			return;
		}
		size_t foundConflicts = 0UL;
		std::stringstream ss;
		ss << "Reasons for retrying insert" << "\n";
		for (int i = 0; i < size; ++i) {
			size_t value = metrics[i];
			if (value) {
				ss << "\t" << insertFailuresAsString(i) << " " << value << "\n";
				foundConflicts += value;
			}
		}
		if (foundConflicts > 1) {
			ss << "\n";
			DEBUG_LOG(INFO) << ss;
		}
	}

	inline void reset() {
		if (LIKELY(not (*this)())) {
			return;
		}
		for (int i = 0; i < size; ++i) {
			metrics[i] = 0UL;
		}
		armed = false;
	}

	~InsertMetrics() {
		printMetrics();
		free(metrics);
	}

private:
	bool disabled;
	bool armed;
	int size;
	std::atomic<size_t>* metrics;
	int retryCount;
};
