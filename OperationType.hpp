/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

static constexpr int NUM_LOCK_OPERATIONS = 5;	// TODO: Keep this in sync with OperationType. An element can only be locked for
						// Insert, Remove, RemoveFront, RemoveBack, and InsertBack locks

//TODO : Make the enum a class
enum OperationType {							//!< Required to indicate whether Element has
	NoOperation = 0,						//!< been locked for insert/remove operations.
	InsertOperation = 0x00000001,
	RemoveOperation = 0x00000002,
	RemoveFrontOperation = 0x00000004,
	RemoveBackOperation = 0x00000008,
	InsertBackOperation = 0x00000010,
        InvalidOperation = 0x000000020
};

static constexpr int NUM_CONFLICTING_LOCK_OPERATIONS = 9;	// TODO: Keep this in sync with ConflictingOperationType.

//TODO : Make the enum a class
enum ConflictingOperationType {
	InsertOperationConflict = 0x00000001,
	RemoveOperationConflict = 0x00000002,
	RemoveFrontOperationConflict = 0x00000004,
	RemoveBackOperationConflict = 0x00000008,
	InsertBackOperationConflict = 0x00000010,
	// Compound lock operations
	InsertAndInsertBackConflict = 0x00000011,
	InsertAndRemoveBackConflict = 0x00000009,
	InsertBackAndRemoveFrontConflict = 0x00000014,
	RemoveFrontAndRemoveBackConflict = 0x0000000c
};

inline std::string& operationTypeAsString(OperationType operation) {
	static std::string noOperationString = std::string("OperationType::NoOperation");
	static std::string insertOperationString = std::string("OperationType::InsertOperation");
	static std::string removeOperationString = std::string("OperationType::RemoveOperation");
	static std::string removeFrontOperationString = std::string("OperationType::RemoveFrontOperation");
	static std::string removeBackOperationString = std::string("OperationType::RemoveBackOperation");
	static std::string insertBackOperationString = std::string("OperationType::InsertBackOperation");
	static std::string invalidOperationString = std::string("OperationType::InvalidOperation");

	switch(operation) {
		case OperationType::NoOperation:
			return noOperationString;
		case OperationType::InsertOperation:
			return insertOperationString;
		case OperationType::RemoveOperation:
			return removeOperationString;
		case OperationType::RemoveFrontOperation:
			return removeFrontOperationString;
		case OperationType::RemoveBackOperation:
			return removeBackOperationString;
		case OperationType::InsertBackOperation:
			return insertBackOperationString;
		case OperationType::InvalidOperation:
			return invalidOperationString;
		default:
			assert(0);
	};
}

inline std::string ConflictingOperationIndexAsString(uint32_t operation) {
	static std::string insertOperationString = std::string("InsertOperation");
	static std::string removeOperationString = std::string("RemoveOperation");
	static std::string removeFrontOperationString = std::string("RemoveFrontOperation");
	static std::string removeBackOperationString = std::string("RemoveBackOperation");
	static std::string insertBackOperationString = std::string("InsertBackOperation");

	static std::string insertAndInsertBackOperationString = std::string("InsertAndInsertBackOperation");
	static std::string insertAndRemoveBackOperationString = std::string("InsertAndRemoveBackOperation");
	static std::string insertBackAndRemoveFrontOperationString = std::string("InsertBackAndRemoveFrontOperation");
	static std::string removeFrontAndRemoveBackOperationString = std::string("RemoveFrontAndRemoveBackOperation");

	switch(operation) {
		case 0:
			return insertOperationString;
		case 1:
			return removeOperationString;
		case 2:
			return removeFrontOperationString;
		case 3:
			return removeBackOperationString;
		case 4:
			return insertBackOperationString;

		case 5:
			return insertAndInsertBackOperationString;
		case 6:
			return insertAndRemoveBackOperationString;
		case 7:
			return insertBackAndRemoveFrontOperationString;
		case 8:
			return removeFrontAndRemoveBackOperationString;
		default:
			assert(0);
	};
}

inline int OperationTypeToIndex(OperationType operation) {
	switch (operation) {
		case OperationType::InsertOperation:
			return 0;
		case OperationType::RemoveOperation:
			return 1;
		case OperationType::RemoveFrontOperation:
			return 2;
		case OperationType::RemoveBackOperation:
			return 3;
		case OperationType::InsertBackOperation:
			return 4;
		default:
			assert(0);
	}
}

inline int ConflictingOperationTypeToIndex(uint32_t operation) {
	switch (operation) {
		case ConflictingOperationType::InsertOperationConflict:
			return 0;
		case ConflictingOperationType::RemoveOperationConflict:
			return 1;
		case ConflictingOperationType::RemoveFrontOperationConflict:
			return 2;
		case ConflictingOperationType::RemoveBackOperationConflict:
			return 3;
		case ConflictingOperationType::InsertBackOperationConflict:
			return 4;

		case ConflictingOperationType::InsertAndInsertBackConflict:
			return 5;
		case ConflictingOperationType::InsertAndRemoveBackConflict:
			return 6;
		case ConflictingOperationType::InsertBackAndRemoveFrontConflict:
			return 7;
		case ConflictingOperationType::RemoveFrontAndRemoveBackConflict:
			return 8;
		default:
			assert(0);
	}
}

inline std::string operationTypeAsString(uint64_t operation) {
	return operationTypeAsString(static_cast<OperationType>((operation & 0xffffffff00000000) >> 32)).append(" with holdCount ").append(std::to_string(static_cast<uint32_t>(operation & 0xffffffff)));
}

