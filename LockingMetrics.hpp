/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

#include "OperationType.hpp"
#include "Likely.hpp"

#include <iostream>
#include <string.h>
#include <sstream>
#include <vector>

enum NameType {
	InvalidName = 0,
	PrevName,
	NextName,
	CurName,
	CurEndName,
	FrontReferenceName,
	BackReferenceName
};

std::string& elementNameTypeAsString(NameType name) {
	static std::string prevName("prev");
	static std::string nextName("next");
	static std::string curName("cur");
	static std::string curEndName("curEnd");
	static std::string frontReferenceName("frontReference");
	static std::string backReferenceName("backReference");

	switch (name) {
		case PrevName:
			return prevName;
		case NextName:
			return nextName;
		case CurName:
			return curName;
		case CurEndName:
			return curEndName;
		case FrontReferenceName:
			return frontReferenceName;
		case BackReferenceName:
			return backReferenceName;
		default:
			assert(0);
	}
}

class LockingMetrics {
public:
	inline LockingMetrics(bool enable, NameType name) : enabled(enable), name(name), armed(false) {
		currentOperation = OperationType::NoOperation;
		if (enable) {
			size = NUM_LOCK_OPERATIONS;
			conflictingSize = NUM_CONFLICTING_LOCK_OPERATIONS;
			metrics.reserve(size);
			for (int i = 0; i < size; ++i) {
				std::vector<size_t>* payload = new std::vector<size_t>;
				metrics.emplace_back(std::move(*payload));
				
				for (int j = 0; j < conflictingSize; ++j) {
					size_t value = 0UL;
					metrics.at(i).emplace_back(value);
				}
			}
		}
	}

	/**
	 * Returns true if the object has been enabled to track metrics for conflicting lock operations
	 */
	inline bool operator ()() {
		return (enabled);
	}

	/**
	 * Increments a counter that prevented the ongoingOperation from completion due to the conflictingOperation
	 */
	inline void incrementCounter(OperationType ongoingOperation, uint64_t conflictingOperation) {
		assert(currentOperation == OperationType::NoOperation || ongoingOperation == currentOperation);
		currentOperation = ongoingOperation;
		assert(currentOperation != OperationType::NoOperation);
		if (UNLIKELY((*this)())) {
			uint32_t conflictingOp = ((conflictingOperation  & 0xffffffff00000000) >> 32);
			if (conflictingOp == 0) {
				return;
			}
			armed = true;
			int opIndex = OperationTypeToIndex(ongoingOperation);
			int conflictIndex = ConflictingOperationTypeToIndex(conflictingOp);
			assert(opIndex <= NUM_LOCK_OPERATIONS-1);
			assert(conflictIndex <= NUM_CONFLICTING_LOCK_OPERATIONS-1);
			metrics.at(opIndex).at(conflictIndex)++;
		}
		assert(currentOperation != OperationType::NoOperation);
	}

	inline void printMetrics() {
		if (LIKELY(not (*this)() || not armed)) {
			return;
		}
		assert(currentOperation != OperationType::NoOperation);

		size_t foundConflicts = 0UL;

		std::stringstream ss;
		ss << "Locking element " << elementNameTypeAsString(name) << " for " << operationTypeAsString(currentOperation) << " conflicted with : " << "\n";
		for (int i = 0; i < conflictingSize; ++i) {
			size_t value = metrics.at(OperationTypeToIndex(currentOperation)).at(i);
			if (value) {
				ss << "\t" << ConflictingOperationIndexAsString(i) << " " << value << " times" << "\n";
				foundConflicts += value;
			}
		}
		if (foundConflicts > 1) {
			ss << "\n";
			DEBUG_LOG(TRACE) << ss;
		}
	}

	inline void reset() {
		if (LIKELY(not (*this)())) {
			return;
		}
		for (int i = 0; i < size; ++i) {
			for (int j = 0; j < size; ++j) {
				metrics.at(i).at(j) = 0UL;
			}
		}
		currentOperation = OperationType::NoOperation;
		name = NameType::InvalidName;
		armed = false;
	}

	~LockingMetrics() {
		if (UNLIKELY((*this)() || armed)) {
			printMetrics();
		}
	}

private:
	bool enabled;
	bool armed;
	int size;
	int conflictingSize;
	std::vector<std::vector<size_t>> metrics;
	NameType name;
	OperationType currentOperation;
};
