/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#include <algorithm>
#include <atomic>
#include <chrono>
#include <cxxabi.h>
#include <execinfo.h>
#include <iostream>
#include <list>
#include <mutex>
#include <pthread.h>
#include <signal.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/syscall.h>
#include <thread>
#include <time.h>
#include <unistd.h>
#include <vector>

int main(int argc, char **argv) {
	if (argc != 2) {
		std::cout << "Invoke as : " << argv[0] << " atomic [0]/non-atomic [1]" << std::endl;
		return 0;
	}
	size_t useAtomic = strtoll(argv[1], nullptr, 0);

	register std::chrono::high_resolution_clock::time_point StartTime = std::chrono::high_resolution_clock::now();
	if (useAtomic) {
		std::atomic<uint64_t> value;
		for (size_t i = 0; i < 1024*1024; ++i) {
			atomic_store(&value, i);
		//	register uint64_t j = value;
		}
	} else {
		uint64_t value = 0UL;
		for (size_t i = 0; i < 1024*1024; ++i) {
			value = i;
		//	register uint64_t j = value;
		}
	}
	register std::chrono::high_resolution_clock::time_point EndTime = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(EndTime - StartTime).count();

	std::cout << "Time taken by ";
	if (useAtomic) std::cout << " atomic : " ; else std::cout << " non-atomic : " ;
	std::cout << std::to_string(duration) << std::endl;;

	return 0;
}
