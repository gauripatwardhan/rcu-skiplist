/***********************************************************
itatic constexpr MAX_LOCK_SPIN = 100;
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#include "Element.hpp"
#include "GenericPointer.hpp"
#include "InsertMetrics.hpp"
#include "IsSharedPtr.hpp"
#include "ListHead.hpp"
#include "Logging.hpp"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <cxxabi.h>
#include <execinfo.h>
#include <iostream>
#include <list>
#include <mutex>
#include <pthread.h>
#include <signal.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/syscall.h>
#include <sys/types.h>
#include <thread>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <numeric>
#include <chrono>

static constexpr size_t NUMELEMENTS = 100;
static constexpr size_t MAXSTACKDEPTH = 4096;
static int CONCURRENCY = std::thread::hardware_concurrency();
static int REPEAT = std::min(CONCURRENCY, 30); //std::thread::hardware_concurrency();
static constexpr size_t LOOPELEMENTS = 4096;

static constexpr size_t START_FIND = 9;
static constexpr size_t END_FIND = 50;

static constexpr size_t MAX_SPIN = 10;
static constexpr size_t MIN_REMOVE_RANGES = 5000000;
static constexpr size_t MIN_REMOVES = 5000000;

void stack_trace() {

  // storage array for stack trace address data
    void* addrlist[MAXSTACKDEPTH+1];

    // retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    // resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);

    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 2096;
    char* funcname = (char*)malloc(funcnamesize);

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++) {
		char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

		// find parentheses and +address offset surrounding the mangled name:
		// ./module(function+0x15c) [0x8048a6d]
		for (char *p = symbollist[i]; *p; ++p) {
			if (*p == '(') {
				begin_name = p;
			} else if (*p == '+') {
				begin_offset = p;
			} else if (*p == ')' && begin_offset) {
				end_offset = p;
				break;
			}
		}
	
		if (begin_name && begin_offset && end_offset && begin_name < begin_offset) {
			*begin_name++ = '\0';
			*begin_offset++ = '\0';
			*end_offset = '\0';
	
		    // mangled name is now in [begin_name, begin_offset) and caller
		    // offset in [begin_offset, end_offset). now apply
		    // __cxa_demangle():
	
		    int status;
		    char* ret = abi::__cxa_demangle(begin_name, funcname, &funcnamesize, &status);
		    if (status == 0) {
				funcname = ret; // use possibly realloc()-ed string
				printf("  %s : %s+%s\n", symbollist[i], funcname, begin_offset);
		    } else {
				// demangling failed. Output function name as a C function with
				// no arguments.
				printf("  %s : %s()+%s\n", symbollist[i], begin_name, begin_offset);
		    }
		} else {
			// couldn't parse the line? print the whole line.
			printf("  %s\n", symbollist[i]);
		}
	}

	free(funcname);
   	free(symbollist);
}

void dumpException(int signalNumber) {
	TEST_DEBUG_LOG(CORRECTNESS) << "\n\nStack Trace  :  Reason (" << strsignal(signalNumber) << ") ========================================\n\n";
	stack_trace();
	TEST_DEBUG_LOG(CORRECTNESS) << "\n\n================================================================================\n\n";
	abort();
}

using PointerType = Element<std::shared_ptr<int>>*;

class payloadFunctorList {
public:
	payloadFunctorList() {}
	payloadFunctorList(int findMe) : findMe(findMe) {}

	bool operator()(std::shared_ptr<int> compareMe) {
		if (findMe == *compareMe) {
			return true;
		}
		return false;
	}

private:
	int findMe;
};

class payloadFunctorListInsert {
public:
	bool operator()(std::shared_ptr<int> findMe, std::shared_ptr<int> compareMe) {
		if (*findMe < *compareMe) {
			return true;
		}
		return false;
	}
};

class payloadFunctor {
public:
	payloadFunctor() {}
	payloadFunctor(int findMe) : findMe(findMe) {}

	/**
	 * Sets greaterThan if compareMe is greater than payload in the object, aka, payload is
	 * smaller than value from Element (compareMe).
	 */
	inline void operator() (std::shared_ptr<int>& compare, bool& match, bool& greaterThan) {
		match = false;
		greaterThan = false;
		register bool lessThan = false;
		register int compareMe = *(compare);

		if (compareMe > findMe) {
			greaterThan = true;
		} else if (compareMe < findMe) {
			lessThan = true;
		} else {
			match = true;
		}
		return;
	}

	std::string describe() {
		return std::to_string(findMe);
	}

private:
	int findMe;
};

class payloadRangeFunctor {
public:
	payloadRangeFunctor() {}
	payloadRangeFunctor(int start, int end) :
				start(start),
				end(end) {}

	inline bool operator() (std::shared_ptr<int> compareMe) {
		if (*compareMe >= start && *compareMe <= end) {
			return true;
		}

		return false;
	}

	std::string describe() {
		std::string describe = "Range between " + std::to_string(start) + " and " + std::to_string(end) + ".";
		return describe;
	}

private:
	int start;
	int end;
};

class payloadRangeFunctorRemove {
public:
	payloadRangeFunctorRemove() {}
	payloadRangeFunctorRemove(int64_t start, int64_t end) :
				start(start),
				end(end) {}


	inline void operator() (int& compareMe, bool& lessThan, bool& greaterThan, bool shortenRange = false) {
		greaterThan = false;
		lessThan = false;

		if (end < compareMe) {
			lessThan = true;
		} else if (start > compareMe) {
			greaterThan = true;
		}
		if (shortenRange && not lessThan && not greaterThan) {
			start = compareMe;
		}
		
	}

	std::string describe() {
		std::string describe = "Range between " + std::to_string(start) + " and " + std::to_string(end) + ".";
		return describe;
	}

	template<typename T>
	bool operator<(T&& val) {
		return false;
	}
	template<typename T>
	bool operator>(T&& val) {
		return false;
	}
	template<typename T>
	bool operator==(T&& val) {
		return false;
	}
	
private:
	int64_t start;
	int64_t end;
};

void insert(ListHead<std::shared_ptr<int>>* head, std::shared_ptr<int>* insertMe, int threadNum, size_t numElements, int numThreads) {
	for (size_t i = 0; i < numElements; ++i) {
		head->insert(insertMe[i*numThreads+threadNum]);
	}
}

void listInsert(std::list<std::shared_ptr<int>>* list, std::shared_ptr<int>* insertMe, std::mutex* myMutex, int threadNum, size_t numElements, int numThreads) {
	for (size_t i = 0; i < numElements; ++i) {
		std::lock_guard<std::mutex> guardMutex(*myMutex);
		std::shared_ptr<int>& insertValue = insertMe[i*numThreads+threadNum];
		register auto it = std::lower_bound(list->begin(), list->end(), insertValue);
		list->insert(it, insertValue);
	}
}

void insertRemoveLoop(ListHead<std::shared_ptr<int>>* head, uint64_t count, std::shared_ptr<int>* payloads, int threadNum) {
	//DEBUG_LOG(TRACE) << "Loop insert with elements : " << count << " ThreadId : " << threadNum;
	for (int i = 0; i < MAX_SPIN; i++) {
		for (int i = 0; i < count; ++i) {
			head->insert(payloads[i]);
		}
		for (int i = 0; i < count; ++i) {
			head->remove<false>(*payloads[i]);
		}
	}
}

void insertRemoveListLoop(std::list<std::shared_ptr<int>>*list, uint64_t count, std::shared_ptr<int>* payloads, std::mutex* myMutex) {
	for (int i = 0; i < MAX_SPIN; i++) {
		for (int i = 0; i < count; ++i) {
			std::lock_guard<std::mutex> guardMutex(*myMutex);
			payloadFunctorListInsert func;
			list->insert(std::lower_bound(list->begin(), list->end(), payloads[i], func), payloads[i]);
		}
		for (int i = 0; i < count; ++i) {
			std::lock_guard<std::mutex> guardMutex(*myMutex);
			payloadFunctorList func(*(payloads[i]));
			list->remove_if(func);
		}
	}
}

void insertWithRemoveRange(ListHead<std::shared_ptr<int>>* head, uint64_t maxIndex, uint64_t threadNum, std::shared_ptr<int>* payloads) {
	int insertMe = rand() % maxIndex;
	InsertMetrics insertFailures(false);
	bool inserted = head->insert(payloads[insertMe], &insertFailures);
	//bool inserted = head->insert(payloads[insertMe], &insertFailures);
}

void removeElement(ListHead<std::shared_ptr<int>>* head, std::shared_ptr<int>*array, int numElements, int threadNum) {
	for (size_t i = numElements; i != 0; --i) {
		head->remove<false>(*array[(i-1)+numElements*threadNum]);
	}
}

void listRemove(std::list<std::shared_ptr<int>>* list, std::shared_ptr<int>* removeMe, std::mutex* myMutex, int numElements, int threadNum) {
	for (size_t i = numElements; i != 0; --i) {
		std::lock_guard<std::mutex> guardMutex(*myMutex);
		std::shared_ptr<int>& removeValue = removeMe[(i-1)+numElements*threadNum];
		register auto it = std::lower_bound(list->begin(), list->end(), removeValue);
		while (*it > removeValue) {
			--it;
		}
		while (*it < removeValue) {
			++it;
		}
		list->erase(it);
	}
}


void removeWithRemoveRange(ListHead<std::shared_ptr<int>>* head, uint64_t maxIndex, uint64_t threadNum, 
				std::atomic<uint64_t>* elementsRemoved, std::shared_ptr<int>* payloads) {
	bool removed= false;
	uint64_t numKilled = 1;
	int removeMe = rand() % maxIndex;
	removed = head->remove<false>(*(payloads[removeMe]), &numKilled);

	elementsRemoved->fetch_add(numKilled, std::memory_order_relaxed);
}


void removeRange(ListHead<std::shared_ptr<int>>* head, uint64_t maxIndex, int threadNum, std::atomic<uint64_t>* removed, 
			std::shared_ptr<int>* payloads) {
	int64_t start = 0;
	int64_t end = 0;
	uint64_t numKilled = 1;

	start = rand() % (maxIndex);
	end = rand() % (maxIndex);
	if (end < start) {
		end = end ^ start;
		start = end ^ start;
		end = end ^ start;
	}
	payloadRangeFunctorRemove func(*(payloads[start]), *(payloads[end]));
	head->remove<true>(func, &numKilled);

	removed->fetch_add(numKilled, std::memory_order_relaxed);
}

std::string convertDurationToString(std::chrono::duration<double, std::micro> microSeconds) {
        double timeDuration = microSeconds.count();

	if (timeDuration > 10000 && timeDuration < 10000000) {
		return std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(microSeconds).count()) + " milliSeconds";
	}
        if (timeDuration > 10000000 && timeDuration < 10000000*60) {
		return std::to_string(std::chrono::duration_cast<std::chrono::seconds>(microSeconds).count()) + " seconds";
	}
        if (timeDuration > 10000000*60) {
		return std::to_string(std::chrono::duration_cast<std::chrono::minutes>(microSeconds).count()) + " minutes";
	}
	return std::to_string(std::chrono::duration_cast<std::chrono::microseconds>(microSeconds).count()) + " microSeconds";

}


enum class ThreadOperation {
	Insert = 0,
	Remove,
	RemoveRange,
	NoOperation
};


struct ThreadProgress {
	ThreadProgress(): removeCounter(0), removeRangeCounter(0), insertCounter(0), done(false), operation(ThreadOperation::NoOperation), threadId(0UL) {}

	bool operator==(ThreadProgress& other) {
		if (done) return false;
		if (removeCounter != other.removeCounter) return false;
		if (removeRangeCounter != other.removeRangeCounter) return false;
		if (insertCounter != other.insertCounter) return false;

		return true;
	}

	std::string getOperation() {
		if (operation == ThreadOperation::NoOperation) {
			return "NoOperation";
		} else if (operation == ThreadOperation::Insert) {
			return "Insert";
		} else if (operation == ThreadOperation::Remove) {
			return "Remove";
		} else if (operation == ThreadOperation::RemoveRange) {
			return "RemoveRange";
		}
	}


	int removeCounter;
	int removeRangeCounter;
	int insertCounter;
	ThreadOperation operation;
	bool done;
	uint64_t threadId;		
};


void supervisor(int insertPercent, int removePercent, int removeRangePercent, int threadNum, ListHead<std::shared_ptr<int>>* head,
		std::shared_ptr<int>* array, uint64_t maxIndex, std::atomic<uint64_t>* numElementsRemoved, ThreadProgress* counter) {
	int operation = 0;
	counter->threadId = syscall(SYS_gettid); 
	counter->operation = ThreadOperation::NoOperation;
	
	while ((removePercent > 0 && counter->removeCounter < MIN_REMOVES) || (removeRangePercent > 0 && counter->removeRangeCounter < MIN_REMOVE_RANGES)) {
		operation = rand() % 100;
		
		if (operation <= insertPercent) {
			counter->operation = ThreadOperation::Insert;
			insertWithRemoveRange(head, maxIndex, threadNum, array);
			counter->insertCounter++;
		} else if (operation > (100-removeRangePercent)) {
			counter->operation = ThreadOperation::RemoveRange;
			removeRange(head, maxIndex, threadNum, numElementsRemoved, array);
			counter->removeRangeCounter++;
		} else {
			counter->operation = ThreadOperation::Remove;
			assert(operation > insertPercent && operation <= (100-removeRangePercent));
			removeWithRemoveRange(head, maxIndex, threadNum, numElementsRemoved, array);
			counter->removeCounter++;
		} 
		counter->operation = ThreadOperation::NoOperation;
	}
	counter->done = true;
}

int main(int argc, char **argv) {
	size_t terminateOption = 0;
	if (argc > 1) {
		terminateOption = strtoll(argv[1], nullptr, 0);
	}
	signal(SIGSEGV, dumpException);
	int numberOfElements = 0;

	
	std::shared_ptr<int>* array = new std::shared_ptr<int>[REPEAT*NUMELEMENTS];
	for (size_t k = 0; k < REPEAT*NUMELEMENTS; ++k) {
		array[k] = std::make_shared<int>(k);
	}

	// Test simple insert operation
	std::shared_ptr<int> startElement = std::make_shared<int>(REPEAT*NUMELEMENTS+1); //never kept in array[REPEAT*NUMELEMENTS]
	std::thread threads[REPEAT];

	std::chrono::high_resolution_clock::time_point startDLLTime;
	std::chrono::high_resolution_clock::time_point endDLLTime;
	ListHead<std::shared_ptr<int>> head;

	bool inserted = head.insert(startElement);
	if (inserted) {
		TEST_DEBUG_LOG(TRACE) << "Test basic insert operation:" << "\t\t\t\tSUCCESS" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE) << "Test basic insert operation:" << "\t\t\t\tFAILURE" << std::endl;
	}

	if (terminateOption == 1 || terminateOption == 6 || terminateOption == 5) {
		// Measure time taken to insert elements in RCU-DLL with several concurrent threads.
		startDLLTime = std::chrono::high_resolution_clock::now();

		for(int i = 0; i < REPEAT; ++i) {
			threads[i] = std::thread(insert, &head, array, i, NUMELEMENTS, REPEAT);
		}

		for(int i = 0; i < REPEAT; ++i) {
			threads[i].join();
		}
		endDLLTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::micro> DLLDuration = std::chrono::duration_cast<std::chrono::duration<double, std::micro>>(endDLLTime - startDLLTime);

		assert((REPEAT*NUMELEMENTS)+1 == head.getNumberOfElements());
		head.verifyAscending(true);


		TEST_DEBUG_LOG(TRACE) << "Multiple concurrent insert test:\t\t\tSUCCESS" << std::endl;
		TEST_DEBUG_LOG(TRACE) << "Time taken by RCU-DLL to insert elements in ascending order: " <<
					convertDurationToString(DLLDuration) << std::endl;;

	}

	if (terminateOption == 1) return 0;

	// Measure time taken to insert elements in std::list with several concurrent threads. Each threads needs to
	// take a mutex for correct list operation.
	std::chrono::high_resolution_clock::time_point startListTime;
	std::chrono::high_resolution_clock::time_point endListTime;
	std::mutex listMutex;
	std::list<std::shared_ptr<int>> list;

	if (terminateOption == 2 || terminateOption == 7) {
		std::chrono::high_resolution_clock::time_point startListTime = std::chrono::high_resolution_clock::now();
		for(int i = 0; i < REPEAT; ++i) {
			//!< std::list does not insert in ascending order so an external function must be used to 
			//!< provide std::list::insert with the correct index value.
			threads[i] = std::thread(listInsert, &list, array, &listMutex, i, NUMELEMENTS, REPEAT);
		}

		for(int i = 0; i < REPEAT; ++i) {
			threads[i].join();
		}

		std::chrono::high_resolution_clock::time_point endListTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::micro> ListDuration = std::chrono::duration_cast<std::chrono::duration<double, std::micro>>(endListTime - startListTime);

		TEST_DEBUG_LOG(TRACE) << "Time taken by std::list to insert elements in ascending order: " <<
						convertDurationToString(ListDuration) << std::endl;;
	}
	if (terminateOption == 2) return 0;

	// Exercise various RCU-DLL iterator constructor/destructor operations.
	if (terminateOption == 3) {
		{
			auto iterator1(head.begin());
		}
	
		for (auto& iterator : head) {}
		DASSERT(head.getActiveCount() == 0);
		TEST_DEBUG_LOG(TRACE) << "Test iterators: " << "\t\t\t\t\tSUCCESS" << std::endl;
	}
	if (terminateOption == 3) return 0;


	// Searching the RCU-DLL for list of elements that fit the specified range (findRange).
	if (terminateOption == 4) {
		payloadRangeFunctor rangeFunctor(START_FIND, END_FIND);
		auto rcuDll = head.findRange(rangeFunctor);
	
		DASSERT(rcuDll->getNumberOfElements() == (END_FIND-START_FIND+1));

		for (const auto& element : *rcuDll) {}
		TEST_DEBUG_LOG(TRACE) << "Test range find: " << "\t\t\t\t\tSUCCESS" << std::endl;


		// Looking for a exact payload in the RCU-DLL.
		if ((*(head.find(*array[NUMELEMENTS-1]))) == (*array[NUMELEMENTS-1])) {
			TEST_DEBUG_LOG(TRACE) << "Test find() operation: " << "\t\t\t\tSUCCESS" << std::endl;
		} else {
			TEST_DEBUG_LOG(TRACE) << "Test find() operation: " << "\t\t\t\tFAILURE" << std::endl;
		}
	}
	if (terminateOption == 4) return 0;


	// Removing a range of elements from the RCU-DLL by calling range remove.
	if (terminateOption == 5) {
		assert(START_FIND >= 0 && END_FIND >= START_FIND);
		payloadRangeFunctorRemove rangeFunctorRemove(START_FIND, END_FIND);

		bool removed = head.remove<true>(rangeFunctorRemove);

		numberOfElements = head.getNumberOfElements();

		DASSERT(numberOfElements = NUMELEMENTS-(END_FIND - START_FIND-1));

		TEST_DEBUG_LOG(TRACE) << "num of elements = " << numberOfElements << std::endl;
		TEST_DEBUG_LOG(TRACE) << "Test range remove: " << "\t\t\t\t\tSUCCESS" << std::endl;

		// Test concurrent remove range operation with insert and remove operations.
		ThreadProgress counters[REPEAT];
		
		int insertPercent = 70;
		int removePercent = 20;
		int removeRangePercent;

		if (insertPercent + removePercent > 100) {
			TEST_DEBUG_LOG(TRACE) << "Bad test environment. Insert and remove percentage cannot exceed 100%." << std::endl;
		}
		removeRangePercent = 100 - (insertPercent+removePercent);
		assert(removeRangePercent == 10);

		std::atomic<uint64_t> elementsRemoved;
		uint64_t maxIndex = REPEAT*NUMELEMENTS;

		std::thread threads[REPEAT];
		
		for(int i = 0; i < REPEAT; i++) {
			threads[i] = std::thread(supervisor, insertPercent, removePercent, removeRangePercent, i, &head, array, maxIndex, &elementsRemoved,
							&counters[i]);
		}
		
		int success = 0;
		int seconds = 1;
		int increment = 1;
		int targetRemoveRanges = increment;
		int targetRemoves = increment;
		int counter = 1;
		size_t removeRangeCounter = 0;
		size_t removeCounter = 0;
		while (success < REPEAT) {
			success = 0;
			ThreadProgress temp[REPEAT] = counters;
			usleep(1000000 * seconds);
			removeRangeCounter = 0;
			for(int i = 0; i < REPEAT; i++) {
				removeRangeCounter += counters[i].removeRangeCounter;
				removeCounter += counters[i].removeCounter;
				if (counters[i].done) {
					success++;
					continue;
				}
				if (counters[i] == temp[i]) {
					head.printDLL();
					DEBUG_LOG(TRACE) << counter <<". Thread: " << i << " (" << counters[i].threadId << ") may be hung in: " << counters[i].getOperation();
					usleep(1000000 * 2);
				} 
			}
			counter++;
			if (removeRangeCounter >= (MIN_REMOVE_RANGES * REPEAT * targetRemoveRanges)/100) {
				targetRemoveRanges += increment;
				DEBUG_LOG(TRACE) << "Done with " << targetRemoveRanges << "% of remove ranges.";
			}
			if (removeCounter >= (MIN_REMOVES * REPEAT * targetRemoves)/100) {
				targetRemoves += increment;
				DEBUG_LOG(TRACE) << "Done with " << targetRemoves << "% of removes.";
			}
		}
		assert ((removeRangePercent > 0 && removeRangeCounter >= REPEAT*MIN_REMOVE_RANGES) || 
			(removePercent > 0 && removeCounter >= REPEAT*MIN_REMOVES));
		for(int i = 0; i < REPEAT; i++) {
			threads[i].join();
		}

		head.printDLL();
		head.verifyAscending(true);
		
		TEST_DEBUG_LOG(TRACE) << "Test range remove with multiple remove and insert operations (elements removed: " 
					<< elementsRemoved.load() << ") : " << "\tSUCCESS" << std::endl;
	}

	if (terminateOption == 5) return 0;

	// Measuring the time taken to remove multiple elements from RCU-DLL using various threads.
	std::thread removeThreads[REPEAT];

	if (terminateOption == 6) {
		startDLLTime = std::chrono::high_resolution_clock::now();
	
		for(int i = 0; i < REPEAT; ++i) {
			removeThreads[i] = std::thread(removeElement, &head, &array[0], NUMELEMENTS, i);
		}
	
		for(int i = 0; i < REPEAT; ++i) {
			removeThreads[i].join();
		}
	
		endDLLTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::micro> DLLDuration = std::chrono::duration_cast<std::chrono::duration<double, std::micro>>(endDLLTime - startDLLTime);
		head.remove<false>(*startElement);
	
		numberOfElements = head.getNumberOfElements();
		TEST_DEBUG_LOG(TRACE) << "After remove: Num elements: " << numberOfElements;

		assert(numberOfElements == 0);

		head.verifyAscending(true);

		TEST_DEBUG_LOG(TRACE) << "Test concurrent remove operations: " << "\t\t\tSUCCESS" << std::endl;
		TEST_DEBUG_LOG(TRACE) << "Time taken by RCU-DLL to remove elements [" << std::to_string(NUMELEMENTS*REPEAT) << "] in ascending order: " << 
					convertDurationToString(DLLDuration) << std::endl;
	}
	if (terminateOption == 6) return 0;


	// Measuring the time taken to remove multiple elements from std::list using various threads.
	if (terminateOption == 7) {
		startListTime = std::chrono::high_resolution_clock::now();
		for(int i = 0; i < REPEAT; ++i) {
			removeThreads[i] = std::thread(listRemove, &list, array, &listMutex, NUMELEMENTS, i);
		}
	
		for(int i = 0; i < REPEAT; ++i) {
			removeThreads[i].join();
		}

		endListTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::micro> ListDuration = std::chrono::duration_cast<std::chrono::duration<double, std::micro>>(endListTime - startListTime);

		TEST_DEBUG_LOG(TRACE) << "Time taken by std::list to remove elements [" << std::to_string(NUMELEMENTS*REPEAT) << "] in ascending order: " <<
					convertDurationToString(ListDuration) << std::endl;
	}

	if (terminateOption == 7) return 0;


	// Final concurrency test
	int concurrency = REPEAT;
	std::thread insertAndRemove[concurrency];
	std::shared_ptr<int>* testPayload = new std::shared_ptr<int>[concurrency*LOOPELEMENTS];
	for (int j = 0; j < concurrency; ++j) {
		for (int i = 0; i < LOOPELEMENTS; ++i) {
			testPayload[i+(j*LOOPELEMENTS)] = std::make_shared<int>(i+(j*LOOPELEMENTS));
		}
	}

	if (terminateOption == 8) {
		// Final test for RCU-DLL with multiple concurrent insert and remove operations using various threads.
		startDLLTime = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < concurrency; ++i) {
			insertAndRemove[i] = std::thread(insertRemoveLoop, &head, LOOPELEMENTS, &testPayload[LOOPELEMENTS*i], i);
		}
		for (int i = 0; i < concurrency; ++i) {
			insertAndRemove[i].join();
		}
		endDLLTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::micro> InsertAndRemoveDLLDuration = std::chrono::duration_cast<std::chrono::duration<double, std::micro>>
							(endDLLTime - startDLLTime);
	
		numberOfElements = head.getNumberOfElements();

		assert(numberOfElements == 0);
		TEST_DEBUG_LOG(TRACE) << "Test concurrent [" << concurrency << "] insert and remove operations: " << "\tSUCCESS" << std::endl;
		TEST_DEBUG_LOG(TRACE) << "Time taken by RCU-DLL to concurrently insert and remove elements: " <<
				convertDurationToString(InsertAndRemoveDLLDuration) << std::endl;
	}
	if (terminateOption == 8) return 0;


	// Final test for std::list with multiple concurrent insert and remove operations using various threads.
	if (terminateOption == 9) {
		startListTime = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < concurrency; ++i) {
			insertAndRemove[i] = std::thread(insertRemoveListLoop, &list, LOOPELEMENTS, &testPayload[LOOPELEMENTS*i],
								&listMutex);
		}
		for (int i = 0; i < concurrency; ++i) {
			insertAndRemove[i].join();
		}
		endListTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::micro> InsertAndRemoveList = std::chrono::duration_cast<std::chrono::duration<double, std::micro>>
							(endListTime - startListTime);
	
		TEST_DEBUG_LOG(TRACE) << "Time taken by std::list to concurrently insert and remove elements: " <<
					convertDurationToString(InsertAndRemoveList) << std::endl;
	}
	return 0;
}
