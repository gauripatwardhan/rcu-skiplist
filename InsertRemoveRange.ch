int numInsertThreads = REPEAT*0.70;
int numRemoveThreads = REPEAT*0.20;
int numRemoveRnageThreads = REPEAT*0.10;

std::thread insertThreads[numInsertThreads];
std::thread removeRangeThreads[numRemoveRangeThreads];
std::thread removeThreads[numRemoveThreads];

// Create remove, insert, and removeRange threads
for (int i = 0; i < numInsertThreads; i++) {
	insertThreads[i] = std::thread(insertWithRemoveRange, &head, NUMELEMENTS, maxElements, i);
}

for (int i = 0; i < numRemoveThreads; i++) {
	removeThreads[i] = std::thread(removeWithRemoveRange, &head, NUMELEMENTS, maxElements, i);
}

for (int i = 0; i < numRemoveThreads; i++) {
	removeThreads[i] = std::thread(removeWithRemoveRange, &head, maxElements, i);
}

void removeWithRemoveRange(ListHead<std::shared_ptr<int>>* head, uint64_t count, uint64_t maxElements, uint64_t threadNum) {
	bool removed= false;
	for (int i = 0; i < count; ++i) {
		int * removeMe = rand() % maxElements;
		removed = head->remove<false>(removeMe);

		if (not removed) {
			count++;
		}
		/******************************
		if (count%10 == 0 && count) {
			timespec* request = new timespec;
			request->tv_sec = 0;
			request->tv_nsec = 1000000;
			nanosleep(request, nullptr);
			count = 0;
		}
		******************************/
	}
}

