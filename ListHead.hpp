/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

#include "Element.hpp"
#include "GenericPointer.hpp"
#include "InsertMetrics.hpp"
#include "IsSharedPtr.hpp"
#include "Iterator.hpp"
#include "Likely.hpp"
#include "LockElement.hpp"
#include "LockingMetrics.hpp"
#include "Logging.hpp"

#include <atomic>
#include <functional>
#include <iostream>
#include <typeinfo>
#include <type_traits>
#include <utility>

static constexpr bool CODE_DISABLED = true;
static constexpr int MAX_RETRY = 15;
static constexpr int YIELD_COUNT = 5;
static constexpr int MIN_NUMELEMENTS = 10;
static constexpr uint64_t NUM_INTERMEDIATES = 3;


enum DLL_OP_ERRORCODE {
	DLL_SUCCESS = 1,
	DLL_ERETRY = 3,
	DLL_EINVAL = 5
};

template<typename T>
class Iterator;

template<typename T>
class ListHead {

public:
	ListHead(const uint64_t intermediatePointers = NUM_INTERMEDIATES, bool yield = false) : 
		activeCount(0),
		generationCount(0),
		removeOperationInProgress(false),
		front(GenericPointer<PointerType, false>()),
		removeList(GenericPointer<PointerType, false>()),
		numElements(0UL),
		uniqueValue64(0UL),
		numIntermediates(0UL),
		insertRemoveCounter(0UL),
		operationInProgress (false),
		yield(yield),
		numElementsInRemoveList(0UL) {
			DASSERT(removeList.isNullPointer());
			DASSERT(front.isNullPointer());	
			DASSERT(not removeOperationInProgress);
			DASSERT(activeCount == 0);
			DASSERT(generationCount == 0);
			numIntermediates = (1 << intermediatePointers) - 1;
			intermediates = new PointerType[numIntermediates];
		}

	~ListHead() {
		// Never delete elements from intermediates in the destructor.
		delete(intermediates);
	}

	using PointerType = Element<T>*;


	//========================================================================================
	// Insert() functions
	//========================================================================================


	/**
	 * Inserts insertPayload by calling insert_impl. insertPayload needs to support the >
	 * (greater than) annd < (less than) operators taking T as its only argument.
	 *
	 * @param insertPayload 
	 * 	The payload which has to be inserted in the DLL.
	 *
	 * @return
	 * 	Returns true if insertPayload was inserted, false otherwise.
	 */

	template<typename Q, 
		 typename std::enable_if<std::is_same<Q, T&>::value || std::is_same<Q, T&&>::value || std::is_same<Q, T>::value, void>::type* = nullptr>
	bool insert(Q&& insertPayload, InsertMetrics* insertFailures = nullptr, volatile bool* insertedAndRemoved = nullptr) {
		incrementActiveCount();
		uint64_t myGenCount = generationCount;
		register DLL_OP_ERRORCODE retval = DLL_OP_ERRORCODE::DLL_SUCCESS;
		PointerType cur = nullptr;
		uint64_t attempts = 0UL;

		do {
			if (yield && attempts && attempts % 50 == 0) {
				std::this_thread::yield();
			}

			retval = insert_impl(insertPayload, cur, insertFailures);
			attempts++;
		} while (retval == DLL_OP_ERRORCODE::DLL_ERETRY);


		if (insertFailures) insertFailures->printMetrics();

		if (LIKELY(retval == DLL_OP_ERRORCODE::DLL_SUCCESS)) {
			incrementElementCount();
			incrementOperationCounter();
			decrementActiveCount(myGenCount);
			return true;
		}

		decrementActiveCount(myGenCount);
		return false;
	}


	/**
	 * Inserts payload in the DLL using payload operator methods < (less than) and > (greater than). Wraps payload in an
	 * Element and inserts that Element in DLL.
	 *
	 * Added support to check if the insert raced ahead of an on-going range remove operation and inserted an element in a
	 * range that is actively getting removed. If so, we check if the inserted element is visible and if not, we remove all
	 * Elements that don't have rangeKill set and which occur prior to the last element getting killed by rangeRemove (such
	 * an Element has the rangeStop flag set with matching unique value).
	 *
	 * @param insertPayload
	 * 	The payload which is to be inserted in the DLL.
	 * 	NOTE: Payload, is user-defined, must have operator methods < (less than) and > (greater than) which take the sole
	 * 	argument of another object of same type as payload. The methods must return true is they succed or else false.
	 *
	 *@return
	 * 	Returns a value from DLL_OP_ERRORCODE.
	 */
	#define LOCK_ELEMENT(element, rootInsert, operation, tryHard, dRemove, lock) 						\
		lock = false;													\
		lock = element->lockElementForOperation(operation, nullptr, tryHard);						\
		dRemove = element->getDeferredRemove();										\
		if(not lock) {													\
			if (rootInsert) rootInsert->unlockElement(OperationType::InsertOperation);				\
			return DLL_OP_ERRORCODE::DLL_ERETRY;									\
		}														\
		if(dRemove) {													\
			element->unlockElement(operation);									\
			if (rootInsert) rootInsert->unlockElement(OperationType::InsertOperation);				\
			return DLL_OP_ERRORCODE::DLL_ERETRY;									\
		}

	DLL_OP_ERRORCODE insert_impl(T& insertPayload, PointerType& curElement, InsertMetrics* insertFailures) {
		register PointerType elementPrev = nullptr;
		register PointerType elementNext = nullptr;
		register PointerType insertReferenceFront = nullptr;
		register PointerType insertReferenceBack = nullptr;
		
		DASSERT(insertPayload != nullptr);
		DASSERT(prev.isNullPointer());
		DASSERT(next.isNullPointer());
		DASSERT(insertReferenceFront.isNullPointer());
		DASSERT(insertReferenceBack.isNullPointer());

		bool exists = walkListInsertBinarySearch(insertPayload, elementPrev, elementNext, insertReferenceFront, insertReferenceBack);

		if (UNLIKELY(exists)) {
			if (curElement) {
				Element<T>::deleteElement(std::move(curElement));
			}
			if (insertFailures) insertFailures->incrementCounter(InsertFailures::ElementExists);
			return DLL_OP_ERRORCODE::DLL_EINVAL;					//!< insertPayload already exists in DLL
		}

		DASSERT(not exists);
		assert(not elementPrev || *insertPayload > *(elementPrev->getPayload()));
		assert(not elementNext || *insertPayload < *(elementNext->getPayload()));
		assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));


		if (LIKELY(not curElement)) {
			curElement = Element<T>::newElement(insertPayload, elementPrev, elementNext);	//!< Create Element wrapping insertPayload
		} else {
			curElement->setFrontPointer(elementNext);
			curElement->setBackPointer(elementPrev);
		}

		register bool prevIsNullptr = (elementPrev == nullptr) ? true : false;
		register bool nextIsNullptr = (elementNext == nullptr) ? true : false;

		register LockElement<T> prevElement(NameType::PrevName, false);
		register LockElement<T> nextElement(NameType::NextName, false);

		register bool deferredRemove = false;
		register bool locked = false;

		if (LIKELY(not prevIsNullptr)) {
			prevElement.lockingElement(elementPrev, OperationType::InsertOperation, false, deferredRemove, locked);
			if (UNLIKELY(not locked || deferredRemove)) {	//!< prev could not be locked or had deferred remove; retry all over
				if (insertFailures) {
					if (not locked) { insertFailures->incrementCounter(InsertFailures::PrevLockFailed); }
					if (deferredRemove) { insertFailures->incrementCounter(InsertFailures::PrevDeferredRemove); }
				}
				return DLL_OP_ERRORCODE::DLL_ERETRY;
			}
			assert (locked && elementPrev->verifyElementLockStatus(OperationType::InsertOperation));
		}

		if (LIKELY(not nextIsNullptr)) {
			nextElement.lockingElement(elementNext, OperationType::InsertBackOperation, false, deferredRemove, locked);
			if (UNLIKELY(not locked)) {	//!< next could not be locked; retry all over
				if (insertFailures) {
					if (not locked) { insertFailures->incrementCounter(InsertFailures::NextLockFailed); }
				}
				return DLL_OP_ERRORCODE::DLL_ERETRY;
			}
			assert (locked && elementNext->verifyElementLockStatus(OperationType::InsertBackOperation));
		}

		assert(not elementPrev || *insertPayload > *(elementPrev->getPayload()));
		assert(not elementNext || *insertPayload < *(elementNext->getPayload()));
		assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));

		if (not nextIsNullptr && elementNext->getBackElementPointer() != insertReferenceBack) {
			if (insertFailures) insertFailures->incrementCounter(InsertFailures::BackCASFailed);
			return DLL_OP_ERRORCODE::DLL_ERETRY;
		}

		register bool frontChanged = false;

		if (LIKELY(not prevIsNullptr)) {
			assert (elementPrev->verifyElementLockStatus(OperationType::InsertOperation));
			assert (not elementNext || elementNext->verifyElementLockStatus(OperationType::InsertBackOperation));
			frontChanged = elementPrev->setFrontPointer(insertReferenceFront, curElement);//!< Change front pointer of prev iff insertReference
			if (not frontChanged && insertFailures) insertFailures->incrementCounter(InsertFailures::PrevCASFailed);
		} else {
			frontChanged = front.cas(insertReferenceFront, curElement);		//!< Change front pointer of prev iff insertReference
			if (not frontChanged && insertFailures) insertFailures->incrementCounter(InsertFailures::FrontCASFailed);
		}

		if (UNLIKELY(not frontChanged)) {
			return DLL_OP_ERRORCODE::DLL_ERETRY;
		}			

		PointerType temp = nullptr;
		if (elementPrev)curElement->getFrontElementPointer();
		while (temp && temp != elementNext) {
			if(not temp->getDeferredRemove()) printDLL();
			assert(temp->getDeferredRemove());
			temp = temp->getFrontElementPointer();
		}
	

		if (LIKELY(not nextIsNullptr)) {
			DASSERT(insertReferenceBack == prev);
			assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::InsertOperation));
			assert (elementNext->verifyElementLockStatus(OperationType::InsertBackOperation));
			elementNext->setBackPointer(insertReferenceBack, curElement);
		}

		assert (not elementNext || elementNext->verifyElementLockStatus(OperationType::InsertBackOperation));
		assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::InsertOperation));

		prevElement.unlock();
		nextElement.unlock();

		return DLL_OP_ERRORCODE::DLL_SUCCESS;
	}


	/**
	 * Walks the DLL untill it finds a pair of Elements in the DLL such that the first Element's Payload is less 
	 * than insertPayload and that of the other Element is greater. Note that there might exist Elements between
	 * the two pair of Elements referred to above but have been marked for deferred remove. The funcion fails if
	 * it detects an Element in the DLL with a matching payload.
	 * 
	 * @param payloadValue
	 * 	The payload which is to be inserted in the DLL. Compare insertPayload to payloads of other
	 * 	elements to find the Element pair (details above). Should have operators > (greater than) and < (less
	 * 	than) defined.
	 *
	 * @param prev
	 * 	The Element after which insertPayload has to be inserted. This Element was not marked for deferred
	 * 	remove at the time of walking the DLL.
	 *
	 * @param next
	 * 	The Element before which insertPayload has to be inserted. Again, this Element did not have deferred
	 * 	remove set when walking the DLL.
	 * 	
	 * @param insertRefernceFront
	 * 	It is used as CAS reference for prev, when inserting the new payload after prev.
	 *
	 * @param insertRefernceBack
	 * 	It is used as CAS reference for next, when inserting the new payload before next.
	 *
	 * @return
	 * 	If an Element with matching payload is found, true is returned, false otherwise.
	 */

	template<typename V>
	inline bool walkListInsert(V& payloadValue, PointerType& prev, PointerType& next, 
				PointerType& insertReferenceFront, 
				PointerType& insertReferenceBack) {
	
		prev = next;
		next = nullptr;
		insertReferenceFront = nullptr;
		insertReferenceBack = nullptr;

		register PointerType elementPrev = prev;
		register PointerType elementNext = next;
		register PointerType elementFront = insertReferenceFront;
		register PointerType elementBack = insertReferenceBack;

		if (not front()) {
			return false;
		}

		if (not elementPrev) {
			 elementPrev = front();
		}
		// Front may have become nullptr when it was read last and if so next will be nullptr
		if (not elementPrev) {	
			prev = nullptr;
			return false;
		}

		/*
		 * We have a next but there's no guarantee that next is the smallest element that is
		 * greater then payloadValue. Consequently, we need to use "next" as a hint and look
		 * for the correct value of next in the near neighbourhood of what we received as the
		 * approximate value.
		 */
		register bool lessThan = false;
		register bool greaterThan = false;
		register bool backward = false;

		register bool skipElement = false;
		elementBack = elementPrev;								// elementBack is used as a temp. variable
		/* payload lessThan/greaterThan elementNext */
		elementPrev->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);
		while (skipElement && elementPrev) {
			assert(not skipElement);
			skipElement = false;
			elementPrev = elementPrev->getFrontElementPointer();
			if (not elementPrev) {
				elementPrev = elementBack;
				lessThan = true;
				break;
			}

			elementPrev->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);
			
		}

		if (lessThan) backward = true; 									/* iterate backwards */
		if (greaterThan) backward = false; 								/* iterate forward */
		if (not lessThan && not greaterThan && not elementPrev->getDeferredRemove()) return true;	/* We have an exact match already in the DLL */

		register PointerType tempPtr = nullptr;
		// (1) Find the exact value of prev that is the smallest element greater than payloadValue
		while (elementPrev && not backward) {								// Go forwards through the DLL to find next marker
			assert(not skipElement);
			tempPtr = elementPrev;
			elementPrev = elementPrev->getFrontElementPointer();
			if (not elementPrev) break;

			/* lessThan = true if payloadValue < elementPrev */
			elementPrev->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);

			if (skipElement) continue;

			if (UNLIKELY(lessThan)) break;

			if (UNLIKELY(not greaterThan && not lessThan && not elementPrev->getDeferredRemove())) {//!< payloadValue is a duplicate
				return true;
			}
		}


		if (elementPrev == nullptr) {
			elementPrev = tempPtr; //The biggest payload value in the DLL
		}

		// (2) Found a prev greater than payloadValue or the element to be inserted is the bigger than any in the DLL
		bool hasDeferredRemove = elementPrev->getDeferredRemove();

		while (elementPrev && (hasDeferredRemove || lessThan)) {
			if (not hasDeferredRemove) {
				/* lessThan = true if payloadValue > elementNext */
				elementPrev->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);

				if (UNLIKELY(not skipElement && not greaterThan && not lessThan && not elementPrev->getDeferredRemove())) {//!< payloadValue is a duplicate
					return true;
				}
			}
			elementPrev = elementPrev->getBackElementPointer();
			if (elementPrev) {
				hasDeferredRemove = elementPrev->getDeferredRemove();
			}
		}

		if (not elementPrev) {
			elementNext = front();
		} else {
			elementNext = elementPrev->getFrontElementPointer();
		}
		// (3) Prev is guranteed to be smaller than payloadValue and did not have deferredRemove when we saw it.
		elementFront = elementNext; 									// May not the same as prev->front
		elementBack = elementPrev;
		if (not elementNext) {
			prev = elementPrev;
			next = elementNext;
			insertReferenceFront = elementFront;
			insertReferenceBack = elementBack;
			return false;
		}
		
		assert(not elementPrev || not elementPrev->payloadGreaterThan(payloadValue));
		/* lessThan = true if payloadValue < elementNext */
		elementNext->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);

		// Advance next to skip over "dead" elements - elements on ListHead::removeList
		while (elementNext && skipElement) {
			elementNext = elementNext->getFrontElementPointer();

			if (not elementNext) break;

			skipElement = false;
			elementNext->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);
		}
		if (elementNext) hasDeferredRemove = elementNext->getDeferredRemove();

		if (UNLIKELY(not skipElement && not greaterThan && not lessThan && not hasDeferredRemove)) {	//!< payloadValue is a duplicate
			return true;
		}

		skipElement = false;
		while (elementNext && (skipElement || hasDeferredRemove || greaterThan)) {
			assert(not skipElement);
			if (not hasDeferredRemove && not skipElement) {						//!< elementNext is smaller than payloadValue
				assert(not elementPrev || not elementPrev->payloadGreaterThan(payloadValue));
				elementPrev = elementNext;
				elementBack = elementPrev; 
				elementNext = elementNext->getFrontElementPointer();
				elementFront = elementNext;
			} else {
				elementBack = elementNext; 
				elementNext = elementNext->getFrontElementPointer();
			}

			if (elementNext) {
				assert(not skipElement);
				skipElement = false;
				hasDeferredRemove = elementNext->getDeferredRemove();
				elementNext->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);

				if (skipElement) continue;

			}
			if (UNLIKELY(not greaterThan && not lessThan && not hasDeferredRemove)) {		//!< payloadValue is a duplicate
				return true;
			}
		}

		assert(not elementNext || lessThan);
		if (elementPrev && elementPrev->payloadGreaterThan(payloadValue)) {
			DEBUG_LOG(TRACE) << "Prev : " << *(elementPrev->getPayload()) << " To be inserted : " << payloadValue;
			assert(0);
		}
		assert(not elementPrev || not elementPrev->payloadGreaterThan(payloadValue));

		prev = elementPrev;
		next = elementNext;
		insertReferenceFront = elementFront;
		insertReferenceBack = elementBack;

		return false;
	}


	/**
	 * Walks the DLL untill it finds a pair of Elements in the DLL such that the first Element's Payload is less 
	 * than insertPayload and that of the other Element is greater. Note that there might exist Elements between
	 * the two pair of Elements referred to above but have been marked for deferred remove. The funcion fails if
	 * it detects an Element in the DLL with a matching payload.
	 * 
	 * @param insertPayload
	 * 	The payload which is to be inserted in the DLL. Compare insertPayload to payloads of other
	 * 	elements to find the Element pair (details above). Should have operators > (greater than) and < (less
	 * 	than) defined.
	 *
	 * @param prev
	 * 	The Element after which insertPayload has to be inserted. This Element was not marked for deferred
	 * 	remove at the time of walking the DLL.
	 *
	 * @param next
	 * 	The Element before which insertPayload has to be inserted. Again, this Element did not have deferred
	 * 	remove set when walking the DLL.
	 * 	
	 * @param insertRefernceFront
	 * 	It is used as CAS reference for prev, when inserting the new payload after prev.
	 *
	 * @param insertRefernceBack
	 * 	It is used as CAS reference for next, when inserting the new payload before next.
	 *
	 * @return
	 * 	If an Element with matching payload is found, true is returned, false otherwise.
	 */

	inline bool walkListInsertBinarySearch(T& insertPayload, PointerType& prev, PointerType& next, 
				PointerType& insertReferenceFront, PointerType& insertReferenceBack) {

		if(UNLIKELY(front.isNullPointer())) {
			return false;
		}

		next = front(); 										// Can be a nullptr by the time this is read
		insertReferenceFront = next;
		prev = nullptr;

		register uint64_t elementNumber =  numElements;
		register uint64_t moveBy = elementNumber;

		register uint32_t startIndex = 0;
		register uint32_t endIndex = numIntermediates;
		register uint32_t midIndex = 0;

		register bool reachedEnd = false;
		register bool reachedFront = false;

		register bool backward = false;
		register bool lessThan = false;
		register bool greaterThan = false;
		register bool skipElement = false;

		register auto& payloadValue = *insertPayload;

		if (not next) reachedFront = true;

		while(moveBy > 1 && not reachedEnd && not reachedFront) {
			/* payload lessThan/greaterThan next */
			insertReferenceBack = next; 						// Used as temp. variable if next reaches end of DLL
			next->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);
			while (skipElement && next) {
				assert(not skipElement);
				next = next->getFrontElementPointer();

				if (not next) {
					lessThan = true;
					next = insertReferenceBack;
					insertReferenceBack = nullptr;
				}

				next->template comparePayloadValue<true>(payloadValue, lessThan, greaterThan, skipElement);
				
			}

			if (UNLIKELY(not greaterThan && not lessThan && not next->getDeferredRemove())) {	//!< insertPayload is a duplicate
				return true;
			}

			if (lessThan) {
				backward = true; /* iterate backwards */
				endIndex = midIndex;
			} else {
				backward = false; /* iterate forward */
				startIndex = midIndex;
			}

			moveBy = moveBy/2;

			if (startIndex != endIndex) {
				midIndex = ((endIndex-startIndex)/2) + startIndex;
				PointerType temp = intermediates[midIndex];
				if (not temp) {
					getMid(next, moveBy, reachedEnd, reachedFront, backward, midIndex);
				} else {
					next = temp;
				}
			} else {
				assert(midIndex == startIndex);
				getMid(next, moveBy, reachedEnd, reachedFront, backward);
			}
		}

		if (reachedFront) next = front();

		return walkListInsert(payloadValue, prev, next, insertReferenceFront, insertReferenceBack);
	}




	//========================================================================================
	// Remove() functions
	//========================================================================================
	

	/**
	 * Traverses the removeList until it reaches element (cur) whose removeGenerationCount is lower
	 * than curGeneration. Then it frees up the memory for all element in the list following 
	 * cur.
	 *
	 * @param currentGeneration
	 * 	The generation count which was present when caller of ListHead::deleteFromRemoveList 
	 * 	enterned the DLL.
	 */

	void deleteFromRemoveList(int currentGeneration) {
		if (removeList.isNullPointer()) return;

		bool _false = false;
		if (not removeOperationInProgress.compare_exchange_weak(_false, true, std::memory_order_release, std::memory_order_consume)) {
			return;
		}
		
		while (true) {
			PointerType cur = removeList();
			PointerType prev = nullptr;
			if (not cur) {
				removeOperationInProgress = false;
				return;
			}

			while(cur && currentGeneration <= cur->getRemoveGenerationCount()) {
				prev = cur;
				cur = (cur->getRemoveList())();
			}

			assert(not cur || cur->getRemoveGenerationCount() < currentGeneration);

			PointerType next = nullptr;

			while (cur) {
				PointerType curRemoveList = (cur->getRemoveList())();
				if (prev) {
					DASSERT((prev->getRemoveList())() == cur);
					prev->setInRemoveList(curRemoveList);
				} else {
					if (not cas(removeList, cur, curRemoveList)) {
						break;
					}
				}
				numElementsInRemoveList.fetch_sub(1, std::memory_order_relaxed);
				
				next = (cur->getRemoveList())();
				Element<T>::deleteElement(cur);
				cur = next;
			}
			
			if (not cur) {
				break;
			}
		}
		removeOperationInProgress = false;
		return;
	}


	/**
	 * Removes a payload declared as a match by the supplied functor: func. The functor needs to support the >
	 * (greater than) annd < (less than) operators taking T as its only argument.
	 *
	 * @param func
	 * 	The functor used to comapre relative positions of various elements in the DLL.
	 *
	 * @return
	 * 	Returns true if the element matched by functor: func() was found, false otherwise.
	 */

	inline std::string errorAsString(DLL_OP_ERRORCODE error) {
		switch (error) {
			case DLL_OP_ERRORCODE::DLL_SUCCESS:
				return "SUCCESS";
			case DLL_OP_ERRORCODE::DLL_ERETRY:
				return "ERETRY";
			case DLL_OP_ERRORCODE::DLL_EINVAL:
				return "EINVAL";
		};
	}

	template<bool range = false, typename U>
	bool remove(U&& func, uint64_t* elementsRemoved = nullptr) {
		incrementActiveCount();
		uint64_t myGenCount = generationCount;
		DLL_OP_ERRORCODE retval = DLL_OP_ERRORCODE::DLL_SUCCESS;
		PointerType remove_element = nullptr;
		size_t attempts = 0;
		int reason = 0;
		uint64_t uniqueNum = 0UL;
		bool makeDLLGreatAgain = true;

		/**
		 * reason 1: Could not lock prev.
		 * reason 2: Could not lock next.
		 * reason 3: Could not lock self.
		 * reason 4: Prev set for deferred remove.
		 * reason 5: Next set for deferred remove.
		 * reason 6: CAS of prev's front failed.
		 * reason 7: CAS of ListHead's front failed.
		 * reason 8: RemoveReferenceBack missmatch.
		 * reason 9: CAS on ListHead::front failed.
		 * reason 10: Could not lock curEnd
		 */
		const int MAX_VALID_REASON = 10;
		const int REASONS = 12;
		int reasons[REASONS];
		for (int i = 0; i < REASONS; ++i) {
			reasons[i] = 0;
		}
		uint64_t removed = 0UL;
		PointerType lastElement = nullptr;
		PointerType firstElement = nullptr;
		uint64_t uniqueValue = 0UL; 

		do {
			assert (not range || makeDLLGreatAgain);
			if (not CODE_DISABLED && attempts && attempts%1000 == 0) {
				DEBUG_LOG(TRACE) << "Total retries: " << attempts;
				DEBUG_LOG(TRACE) << "Remove completed. Reasons seen: ";
				if (reasons[1]) DEBUG_LOG(TRACE) << "Could not lock prev" << reasons[1];
				if (reasons[10]) DEBUG_LOG(TRACE) << "Could not lock curEnd" << reasons[10];
				if (reasons[2]) DEBUG_LOG(TRACE) << "Could not lock next." << reasons[2];
				if (reasons[3]) DEBUG_LOG(TRACE) << "Could not lock self." << reasons[3];
				if (reasons[4]) DEBUG_LOG(TRACE) << "Prev set for deferred remove." << reasons[4];
				if (reasons[5]) DEBUG_LOG(TRACE) << "Next set for deferred remove." << reasons[5];
				if (reasons[6]) DEBUG_LOG(TRACE) << "CAS of prev's front failed." << reasons[6];
				if (reasons[7]) DEBUG_LOG(TRACE) << "CAS of ListHead's front failed." << reasons[7];
				if (reasons[8]) DEBUG_LOG(TRACE) << "RemoveReferenceBack missmatch." << reasons[8];
				if (reasons[9]) DEBUG_LOG(TRACE) << "Detailed reason" << reasons[9];
				attempts = 0;
			}

			if (yield && attempts && attempts % 50 == 0) {
				std::this_thread::yield();
			}

			if (not range) retval = remove_impl<range>(func, remove_element, reason, removed);

			if (range) {
				if (retval != DLL_OP_ERRORCODE::DLL_ERETRY) uniqueValue = getUniqueValue64();
				verifyAscending(__LINE__, false);
				retval = rangeRemove_impl<range>(func, reason, makeDLLGreatAgain, removed, lastElement, firstElement, uniqueValue);
			}

			if (retval == DLL_OP_ERRORCODE::DLL_SUCCESS) {
				assert(not range || not makeDLLGreatAgain);
				DASSERT(reason == 0);
			}
			++attempts;
			reasons[reason]++;
		} while (retval == DLL_OP_ERRORCODE::DLL_ERETRY);

		// Ennable the debug statements only when debugging code. They do add to overhead.
		if (not CODE_DISABLED && attempts > 1000) {
			DEBUG_LOG(TRACE) << "Total retries: " << attempts;

			DEBUG_LOG(TRACE) << "Remove completed. Reasons seen: ";
			DEBUG_LOG(TRACE) << "Count not lock prev" << reasons[1];
			DEBUG_LOG(TRACE) << "Could not lock next." << reasons[2];
			DEBUG_LOG(TRACE) << "Could not lock self." << reasons[3];
			DEBUG_LOG(TRACE) << "Prev set for deferred remove." << reasons[4];
			DEBUG_LOG(TRACE) << "Next set for deferred remove." << reasons[5];
			DEBUG_LOG(TRACE) << "CAS of prev's front failed." << reasons[6];
			DEBUG_LOG(TRACE) << "CAS of ListHead's front failed." << reasons[7];
			DEBUG_LOG(TRACE) << "RemoveReferenceBack missmatch." << reasons[8];
			DEBUG_LOG(TRACE) << "Detailed reason" << reasons[9];
		}

		if (range) {
			verifyAscending(__LINE__, false, &lastElement, &firstElement, nullptr, nullptr, &uniqueValue);
		}
		if (elementsRemoved) *elementsRemoved = removed;

		if (LIKELY(retval == DLL_OP_ERRORCODE::DLL_SUCCESS)) {
			verifyAscending(__LINE__, false);
			incrementOperationCounter();
			decrementActiveCount(myGenCount);
			return true;
		}

		DASSERT(retval == DLL_OP_ERRORCODE::DLL_EINVAL);
		verifyAscending(__LINE__, false);
		decrementActiveCount(myGenCount);
		return false;
	}



	/**
	 * Actual implementation of DLL remove node logic.
	 *
	 * Remove payload in the DLL using func operator methods < and >. remove function removes the Element wrapping
	 * the payload from the DLL. The payload being a shared_ptr, gets destroyed only when the last reference to it
	 * get dropped, which could exist in form of users still referring to the payload from earlier calls to find().
	 *
	 * The function attempts to lock the Element to be removed along with Elements before (prev) and after (next)
	 * the Element to be removed. This ensures that no intertwining operation can affect the remove. The guarantee
	 * given by a successful remove operation is that the payload would not be visible to any subsequent find, insert
	 * or remove operations. It is possible to give that guarantee by marking the Element to be removed for deferred
	 * removal. remove() at its discretion decides the best option to effect the remove by either marking the Element
	 * for deferred removal or actually removing it from the DLL.
	 *
	 * In spite of the size of this function, the compiler would inline a lot of metods called internally reducing the
	 * number of instructions required to remove an Element to best case: 5 CAS instructions or 5 load and 5 stores.
	 * Which is to say, there are only 10 instructions which are critical and can conflict with another operation on
	 * the involved elements.
	 *
	 * @param func
	 * 	The functor that represents the payload to be removed in the DLL.
	 * 	NOTE: The user-defined functor must have operator methods < (less than) and > (greater than) which
	 * 	take the sole argument of payload. The methods must return true if the condition is met, false otherwise.
	 *
	 *@return
	 *	Returns DLL_OP_ERRORCODE::DLL_SUCCESS if the payload was found and deleted, or DLL_OP_ERRORCODE::(!DLL_SUCCESS) along
	 *	with Element to be removed as a std::tuple otherwise. There exists a guarantee that if the payload was found,
	 *	it would be logically or physical deleted by this function before returning. The only time this function can
	 *	fail and return failure is if the payload did not exist in the DLL.
	 */

	#define EVALUATE_LOCK_SUCCESS_REMOVE_IMPL(prevE, nextE, lockSuccess, dRemove, rCount, retryReason, remove, find)	\
	if (UNLIKELY(not lockSuccess || dRemove)) {										\
		if (UNLIKELY(rCount >= MAX_RETRY)) {										\
			assert(not remove || find == remove);									\
			remove = find;												\
			reason = retryReason;											\
			return DLL_OP_ERRORCODE::DLL_ERETRY;									\
		}														\
		retryOperation = true;												\
		prevE.unlock();													\
		nextE.unlock();													\
		continue;													\
	}

	template<bool range = false, typename U, typename std::enable_if<range>::type* = nullptr>
	DLL_OP_ERRORCODE remove_impl(U&& func, PointerType& removeMe, int& reason, uint64_t& numKilled);

	template<bool range = false, typename U, typename std::enable_if<not range>::type* = nullptr>
	DLL_OP_ERRORCODE remove_impl(U&& func, PointerType& removeMe, int& reason, uint64_t& numKilled) {

		PointerType elementPrev = nullptr;
		PointerType elementNext = nullptr;
		PointerType elementRemoveReferenceFront = nullptr;
		PointerType elementRemoveReferenceBack = nullptr;
		PointerType elementCur = nullptr;
		PointerType elementFindMe = nullptr;
		bool nullPrev = true;
		bool nullNext = true;

		DASSERT(func);
		DASSERT(prev.isNullPointer() == true);
		DASSERT(next.isNullPointer() == true);
		DASSERT(removeReferenceFront.isNullPointer() == true);
		DASSERT(removeReferenceBack.isNullPointer() == true);
		DASSERT(cur.isNullPointer() == true);
		
		bool retryOperation = true;
		size_t retryCount = 0L;

		LockElement<T> prevElement(NameType::PrevName, false);
		LockElement<T> nextElement(NameType::NextName, false);
		LockElement<T> curElement(NameType::CurName, false);

		while (retryOperation == true) {
			++retryCount;
			// remove Element before next and after prev
			elementPrev = nullptr;
			elementNext = nullptr;
			elementRemoveReferenceFront = nullptr;
			elementRemoveReferenceBack = nullptr;
			elementCur = nullptr;

			bool found = this->template walkListRemoveBinarySearch<true>(func, elementNext, elementPrev, elementRemoveReferenceFront, 
						elementRemoveReferenceBack, elementCur, removeMe, numKilled);

			if (UNLIKELY(not found)) {
				reason = 0;
				return DLL_OP_ERRORCODE::DLL_EINVAL;
			}
			
			assert(not elementPrev || func > *(elementPrev->getPayload()));
			assert(not elementNext || func < *(elementNext->getPayload()));
			assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));
		
			if(not elementPrev) nullPrev = true;
			if(not elementNext) nullNext = true;
			if(elementPrev) nullPrev = false;
			if(elementNext) nullNext = false;

			elementFindMe = elementCur;

			bool deferredRemove = false;
			bool success = false;

			/* 
			 * Critical section of remove_impl() begins. We need to mkae sure the section takes minimal amount of time 
			 * required to finish. Preload everything possible in order to decrease the time taken.
			 */
			if (LIKELY(not nullPrev)) {
				prevElement.lockingElement(elementPrev, OperationType::RemoveFrontOperation, false, deferredRemove, success);
				EVALUATE_LOCK_SUCCESS_REMOVE_IMPL(prevElement, nextElement, success, deferredRemove, retryCount,
									1, removeMe, elementFindMe);
				elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation);
			}

			if (LIKELY(not nullNext)) {
				nextElement.lockingElement(elementNext, OperationType::RemoveBackOperation, false, deferredRemove, success);
				EVALUATE_LOCK_SUCCESS_REMOVE_IMPL(prevElement, nextElement, success, deferredRemove, retryCount, 
									2, removeMe, elementFindMe);
				elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation);
			}

			if (not removeMe) {
				success = elementCur->lockElementForOperation(OperationType::RemoveOperation, nullptr, false);
				deferredRemove = elementCur->getDeferredRemove();
				EVALUATE_LOCK_SUCCESS_REMOVE_IMPL(prevElement, nextElement, success, false, retryCount,
									3, removeMe, elementFindMe);
			}

			elementCur->verifyElementLockStatus(OperationType::RemoveOperation);

			register bool setForRemoval = elementCur->setDeferredRemove();
			if (UNLIKELY(setForRemoval && not removeMe)) {
				elementCur->setRemoveGenerationCount(generationCount);
				elementCur->setInRemoveList(removeList());						//!< Placing cur in removeList
				while(not cas(removeList, elementCur->getRemoveList(), elementCur)) {
					elementCur->setInRemoveList(removeList());
				}
				numElementsInRemoveList.fetch_add(1, std::memory_order_relaxed);
				decrementElementCount();

				if (elementCur->getAnchored()) removeAnchor(elementCur);
			}	

			removeMe = elementCur;

			break;
		}

		/**
		 * Adjust front of prev and back of next iff our understanding of the relative neighbours of cur as identified 
		 * by walkListRemove is still correct.
		 */

		assert(not elementPrev || func > *(elementPrev->getPayload()));
		assert(not elementNext || func < *(elementNext->getPayload()));
		assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));

		if (UNLIKELY(not nullNext && elementNext->getBackElementPointer() != elementRemoveReferenceBack)) {
			reason = 8;
			return DLL_OP_ERRORCODE::DLL_ERETRY;
		}

		bool frontChanged;
		if (LIKELY(not nullPrev)) {
			elementCur->verifyElementLockStatus(OperationType::RemoveOperation);
			if (elementNext) elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation);
			elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation);

			frontChanged = elementPrev->setFrontPointer(elementRemoveReferenceFront, elementNext);//!< Change front pointer of prev to next
			if (UNLIKELY(not frontChanged)) {
				reason = 6;
				return DLL_OP_ERRORCODE::DLL_ERETRY;
			}
		} else {
			/**
			 * If ListHead::front() is a nullptr, its implicit that we won't be able to find the payload we
			 * want to remove when we walk the DLL. So just return here itself. We must have already queued
			 * "cur" Element on removeList and someone will take care of removing it as part of exit processing.
			 */
			if (UNLIKELY(front.isNullPointer())) {
				reason = 0;
				return DLL_OP_ERRORCODE::DLL_EINVAL;
			}

			frontChanged = cas(front, elementRemoveReferenceFront, elementNext);
			if (UNLIKELY(not frontChanged)) {
				reason = 9;
				return DLL_OP_ERRORCODE::DLL_ERETRY;
			}
		}

		PointerType temp = nullptr;
		if (elementPrev) temp = elementPrev->getFrontElementPointer();
		if (not elementPrev) temp = front();
		while (temp && temp != elementNext) {
			if(not temp->getDeferredRemove()) printDLL();
			assert(temp->getDeferredRemove());
			temp = temp->getFrontElementPointer();
		}


		// We cannot fail the remove now !!!

		if (LIKELY(not nullNext)) {
			elementCur->verifyElementLockStatus(OperationType::RemoveOperation);
			if (elementPrev) elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation);
			elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation);

			elementNext->setBackPointer(elementPrev);	 				//!< Change back pointer of cur->next 
		}

		DASSERT(genCount == generationCount);
		elementCur->verifyElementLockStatus(OperationType::RemoveOperation);
		if (elementPrev) elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation);
		if (elementNext) elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation);

		reason = 0;
		return DLL_OP_ERRORCODE::DLL_SUCCESS;
	}


	/*
	 * Modified to add support to paint all elements getting removed as part of rangeRemove with a unique number
	 * and add a rangeStop flag on the last Element getting removed as part of a rangeRemove. Also if a regular
	 * remove operation clears the rangeKill, rangeStop and uniqueRangeRemoveValue flags after ensuring that the
	 * remove cannot fail.
	 */

	#define EVALUATE_LOCK_SUCCESS_REMOVE_RANGE_IMPL(prevE, nextE, curE, curEndE, lockSuccess, dRemove, rCount, retryReason)		\
	if (UNLIKELY(not lockSuccess || dRemove)) {											\
		if (UNLIKELY(rCount >= MAX_RETRY)) {											\
			reason = retryReason;												\
			return DLL_OP_ERRORCODE::DLL_ERETRY;										\
		}															\
		retryOperation = true;													\
		prevE.unlock();														\
		nextE.unlock();														\
		curE.unlock();														\
		curEndE.unlock();													\
		continue;														\
	}

	template<bool range = false, typename U, typename std::enable_if<not range>::type* = nullptr>
	DLL_OP_ERRORCODE rangeRemove_impl(U&& func, int& reason, bool& makeDLLGreatAgain, uint64_t& numKilled, 
						PointerType& lastElement, PointerType& firstElement, uint64_t uniqueValue);

	template<bool range = false, typename U, typename std::enable_if<range>::type* = nullptr>
	DLL_OP_ERRORCODE rangeRemove_impl(U&& func, int& reason, bool& makeDLLGreatAgain, uint64_t& numKilled, 
						PointerType& lastElement, PointerType& firstElement, uint64_t uniqueValue) {

		DASSERT(func);
		
		register PointerType elementPrev = nullptr;
		register PointerType elementNext = nullptr;
		register PointerType removeReferenceFront = nullptr;
		register PointerType removeReferenceBack = nullptr;
		register bool nullPrev = false;
		register bool nullNext = false;

		register bool retryOperation = true;
		register size_t retryCount = 0L;

		makeDLLGreatAgain = false;

		register LockElement<T> prevElement(NameType::PrevName, false);
		register LockElement<T> nextElement(NameType::NextName, false);

//		LockingMetrics prevMetrics(false, NameType::PrevName);
//		LockingMetrics nextMetrics(false, NameType::NextName);
//		LockingMetrics curMetrics(false, NameType::CurName);
//		LockingMetrics curEndMetrics(false, NameType::CurEndName);

		/*** CONSTRUCTION ZONE ***/
		register PointerType elementCur = nullptr;
		elementPrev = nullptr;
		elementNext = nullptr;
		register PointerType removeMe = nullptr;

		// remove Element before next and after prev
		register bool found = walkListRemoveBinarySearch<false>(func, elementNext, elementPrev, removeReferenceFront, 
									removeReferenceBack, elementCur /* nullptr */, 
									removeMe /* nullptr */, numKilled, &prevElement,
									&nextElement, &makeDLLGreatAgain, &lastElement, &firstElement,
									&uniqueValue);

		DEBUG_LOG(INFO) << "Finished walkListRemoveRange" << std::endl;
		if (found == false) {
			reason = 0;
			return DLL_OP_ERRORCODE::DLL_EINVAL;
		}

		assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
		assert (not elementNext || elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation));
		assert (not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));

		/*** No need to lock anything any more. walkListRemoveRange, called by walkListRemoveBinarySeach, takes care of that. ***/
		/*************************************************************
		bool deferredRemove = false;
		bool success = false;

		// 
		 / Critical section of remove_impl() begins. We need to mkae sure the section takes minimal amount of time 
		 / required to finish. Preload everything possible in order to decrease the time taken.
		 //

		if(UNLIKELY(elementPrev && elementPrev->getDeferredRemove())) {
			EVALUATE_LOCK_SUCCESS_REMOVE_RANGE_IMPL(prevElement, nextElement, curElement, curEndElement, true, elementPrev->getDeferredRemove(), retryCount, 2);
		}

		if (LIKELY(elementNext)) {
			nextElement.lockingElement(elementNext, OperationType::RemoveBackOperation, false, deferredRemove, success);
			EVALUATE_LOCK_SUCCESS_REMOVE_RANGE_IMPL(prevElement, nextElement, curElement, curEndElement, success, deferredRemove, retryCount, 2);
//			DASSERT(elementNext->getElementOperationLockStatus() == OperationType::RemoveBackOperation);
			elementNext->setRangeStop();
		}


		curElement.lockingElement(elementCur, OperationType::RemoveOperation, false, deferredRemove, success);
		EVALUATE_LOCK_SUCCESS_REMOVE_RANGE_IMPL(prevElement, nextElement, curElement, curEndElement, success, false, retryCount, 3);
//		DASSERT(elementCur->getElementOperationLockStatus() == OperationType::RemoveOperation);

		
		if (LIKELY(elementCurEnd)) {
			curEndElement.lockingElement(elementCurEnd, OperationType::RemoveOperation, false, deferredRemove, success);
			EVALUATE_LOCK_SUCCESS_REMOVE_RANGE_IMPL(prevElement, nextElement, curElement, curEndElement, success, false, retryCount, 10);
//			DASSERT(elementNext->getElementOperationLockStatus() == OperationType::RemoveBackOperation);
		}
		*************************************************************/

		/**
		 * Adjust front of prev and back of next iff our understanding of the relative neighbours of cur as identified 
		 * by walkListRemove is still correct.
		 */

		if (UNLIKELY(elementNext && elementNext->getBackElementPointer() != removeReferenceBack)) {
			DEBUG_LOG(TRACE) << "Cannot change next's back pointer" << std::endl;
			DEBUG_LOG(TRACE) << "Next : " << *(elementNext->getPayload());
			DEBUG_LOG(TRACE) << "removeReferenceBack : " << *(removeReferenceBack->getPayload());
			DEBUG_LOG(TRACE) << "Back pointer of next : " << *((elementNext->getBackElementPointer())->getPayload());
			assert(0);
			return DLL_OP_ERRORCODE::DLL_ERETRY;
		}

		bool frontChanged;
		if (LIKELY(elementPrev)) {
			if (UNLIKELY(elementPrev->getDeferredRemove())) {
				makeDLLGreatAgain = true;
				reason = 1;
				return DLL_OP_ERRORCODE::DLL_ERETRY;
			}
			assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
			assert (not elementNext || elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation));
			assert (not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));
			frontChanged = elementPrev->setFrontPointer(removeReferenceFront, elementNext);//!< Change front pointer of prev to next
			if (UNLIKELY(not frontChanged)) {
				assert(0);
				return DLL_OP_ERRORCODE::DLL_ERETRY;
			}
		} else {
			/**
			 * If ListHead::front() is a nullptr, its implicit that we won't be able to find the payload we
			 * want to remove when we walk the DLL. So just return here itself. We must have already queued
			 * "cur" Element on removeList and someone will take care of removing it as part of exit processing.
			 */
			if (UNLIKELY(front.isNullPointer())) {
				reason = 0;
				return DLL_OP_ERRORCODE::DLL_EINVAL;
			}

			frontChanged = cas(front, removeReferenceFront, elementNext);
			if (UNLIKELY(not frontChanged)) {
				reason = 9;
				makeDLLGreatAgain = true;
				return DLL_OP_ERRORCODE::DLL_ERETRY;
			}
		}
		DEBUG_LOG(INFO) << "Switched pointers; running verifyAscending()" << std::endl;

		PointerType temp = nullptr;
		if (firstElement) temp = firstElement->getFrontElementPointer();
		while (temp && temp != elementNext) {
			if(not temp->getDeferredRemove()) printDLL();
			assert(temp->getDeferredRemove());
			temp = temp->getFrontElementPointer();
		}


		register bool shrinkRange = true;
		register bool lessThan = false;
		register bool greaterThan = false;
		register bool exactMatch = false;
		register bool skipElement = false;
		if (lastElement) lastElement->template comparePayloadValue<false>(func, lessThan, greaterThan, skipElement, &exactMatch, &shrinkRange);

		if (LIKELY(elementNext)) {
			assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
			assert (not elementNext || elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation));
			assert (not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));

			elementNext->setBackPointer(elementPrev);
		}

		DASSERT(genCount == generationCount);

		reason = 0;
		DEBUG_LOG(INFO) << "Done" << std::endl;

		assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
		assert (not elementNext || elementNext->verifyElementLockStatus(OperationType::RemoveBackOperation));
		assert (not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));

		if (makeDLLGreatAgain) {
			verifyAscending(__LINE__, false, &lastElement, &firstElement, elementPrev, elementNext, &uniqueValue);
			return DLL_OP_ERRORCODE::DLL_ERETRY;
		} else {
			verifyAscending(__LINE__, false, &lastElement, &firstElement, elementPrev, elementNext, &uniqueValue);
			return DLL_OP_ERRORCODE::DLL_SUCCESS;
		}
	}

	
	/**
	 * Walks the DLL untill it reaches an Element whose payload is equal to removePayload.
	 * 
	 * @param func
	 * 	The functor that represents the payload to be removed from the DLL. Compares payloads of
	 * 	other elements to find an Element with the same payload as that represented by func.
	 *
	 * @param prev
	 * 	The Element (not marked for deferred remove) just preceeding the Element to be removed.
	 *
	 * @param next
	 * 	The Element (not) marked for deferred remove) immediately following the Element to be removed.
	 * 	
	 * @param removeReferenceFront
	 * 	The Element following prev which could be some element marked for deferred remove or can also
	 * 	be cur, whichever comes first in the DLL.
	 *
	 * @param removeReferenceBack
	 * 	The Element following cur which could be some element marked for deferred remove or can also
	 * 	be next, whichever comes first in the DLL.
	 *
	 * @param cur
	 * 	The Element matching the payload to be removed. It is guaranteed to be between prev and
	 * 	next.
	 *
	 * @return
	 * 	Returns true if Element to be removed is found in the DLL, false otherwise.
	 */
	
	template<bool nonRangeType = true, typename U>
	bool walkListRemove(U&& func, PointerType& prev, PointerType& next, 
				PointerType& removeReferenceFront, PointerType& removeReferenceBack,
				PointerType& cur, PointerType& findMe, uint64_t& numKilled) {
	
		DASSERT(func);
		assert(nonRangeType);
		numKilled = 0;

		register PointerType elementNext = next;	
		register PointerType elementCur = cur;	
		register PointerType elementPrev = prev;	
		register PointerType elementBack = removeReferenceBack;	
		register PointerType elementFront = removeReferenceFront;	
	
		if (not front.isNullPointer() && not elementPrev) {
			elementPrev = front();
		} else if (front.isNullPointer()) { 				// DLL is empty  
			return false;
		}
		if (not elementPrev) {						// DLL is empty
			return false;
		}
	
		register bool exactMatch = false;
		register bool greaterThan = false;
		register bool lessThan = false;
		register bool backward = false;

		register bool skipElement = false;

		elementBack = elementPrev;								// elementBack is used as a temp. variable
		elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement); /* func lessThan/greaterThan */
		while (skipElement && elementPrev) {
			assert(0);
			skipElement = false;
			elementPrev = elementPrev->getFrontElementPointer();
			if (not elementPrev) {
				elementPrev = elementBack;
				lessThan = true;	
				break;
			}

			elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement);
			
		}
		exactMatch = (not greaterThan && not lessThan) ? true : false;

		if (lessThan || exactMatch) backward = true;

		bool hasDeferredRemove = false;
		//(1) Advance prev till prev is equal to *func or findMe or till payload not found (end of DLL)
		while (not backward && (greaterThan)) {
			elementBack = elementPrev; 			// Temporary refernece holder
			elementPrev = elementPrev->getFrontElementPointer();

			if (not elementPrev) break;

			if (findMe) {
				exactMatch = (elementPrev == findMe);
				if (exactMatch) {
					assert (not elementCur);
					elementCur = elementPrev;
					break;
				}
			} else {
				elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement);

				if (skipElement) continue;

				exactMatch = (not greaterThan && not lessThan) ? true : false;
			}

 			hasDeferredRemove = elementPrev->getDeferredRemove();

			if (exactMatch && not findMe && not hasDeferredRemove) {
				assert (not elementCur);
				elementCur = elementPrev;
				DEBUG_LOG(INFO) << "Found cur";
				break;
			}
		}
		assert(not elementPrev || func < *(elementPrev->getPayload()) || func == *(elementPrev->getPayload()));
		assert(not elementNext);

		if (elementPrev) {
			elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement); /* func lessThan/greaterThan */
//			DEBUG_LOG(INFO) << "Prev: " << *(elementPrev->getPayload()) << " GreaterThan: " << greaterThan << " LessThan: " << lessThan << " w/ functor " << func;
			assert(skipElement || not greaterThan);
			assert(skipElement || lessThan || (not lessThan && not greaterThan));
		}

		if (not backward && not elementCur) {
//			DEBUG_LOG(TRACE) << "Returning not found. Came looking for: " << func;
			//printDLL();
			//assert(0);
			return false;
		}
		if (not elementPrev) {
			elementPrev = elementBack;
		}

		greaterThan = true;

		//(2) Now rewind prev till prev is smaller than  *func or findMe and doesn't have deferredRemove set.
		while (elementPrev && (lessThan || hasDeferredRemove || exactMatch)) {
			elementPrev = elementPrev->getBackElementPointer();

			if (not elementPrev) break;

			elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement); /* func lessThan/greaterThan */
			
			if (skipElement) continue;

			exactMatch = (not greaterThan && not lessThan) ? true : false;
			hasDeferredRemove = elementPrev->getDeferredRemove();
		}

		if (not elementPrev) { 						// Reached start DLL
			elementNext = front();
			elementFront = elementNext;
			elementBack = elementPrev;
		} else {
			elementNext = elementPrev->getFrontElementPointer();
			elementFront = elementNext;
			elementBack = elementPrev;
		}
		if (not elementNext) { 						// DLL is empty OR reached end of DLL
//			DEBUG_LOG(TRACE) << "Returning not found. Came looking for: " << func;
			return false;
		}

		if (elementPrev) {
			elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement); /* func lessThan/greaterThan */
//			DEBUG_LOG(INFO) << "Prev: " << *(elementPrev->getPayload()) << " GreaterThan: " << greaterThan << " LessThan: " << lessThan << " w/ functor " << func;
			assert(skipElement || greaterThan);
			assert(skipElement || not lessThan);
			assert(skipElement || not (not lessThan && not greaterThan));
		}

		if (not findMe) {
			// greaterThan = true iff func > elementNext
			elementNext->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement); /* func lessThan/greaterThan */
			exactMatch = (not greaterThan && not lessThan) ? true : false;
			hasDeferredRemove = elementNext->getDeferredRemove();
		} else {
			exactMatch = (findMe == elementNext);
			greaterThan = false;
		}

		elementCur = nullptr;

		if (elementPrev) {
			DEBUG_LOG(INFO) << "\t Next: " << elementNext << " Prev: " << *(elementPrev->getPayload()) << " Front: " << elementFront << " Back: " << elementBack;
		}

		assert (elementPrev != elementNext || (elementPrev == nullptr && elementNext == nullptr && elementFront));
		assert (elementFront || elementBack);
		assert(not elementPrev || func > *(elementPrev->getPayload()));
		assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));

		// (3) Now advance next (with elementBack following it) till next is greater than *func (or findMe) and doesn't have deferredRemove set
		// Switch elementPrev between values smaller than *func (or findMe) and don't have deferredRemove with elementFront an element ahead of it.
		// elementNext can equal *fund (or findMe) at the beginning of this loop.
		PointerType copyElementNext = elementNext;
		PointerType copyElementPrev = elementPrev;
		bool copyGreaterThan = greaterThan;
		bool copyLessThan = lessThan;
		bool copyExactMatch = exactMatch;

		while (elementNext && (greaterThan || exactMatch || hasDeferredRemove || skipElement)) {
			assert(not skipElement);
			if (exactMatch && not hasDeferredRemove && not findMe && not skipElement) {
				elementCur = elementNext;
			}
			if (findMe && (findMe == elementNext)) {
				exactMatch = true;
				elementCur = elementNext;
			}

			elementBack = elementNext;

			// not greaterThan implies elementNext is smaller than *func
			if (greaterThan && not hasDeferredRemove && not skipElement) {	// Next is smaller than element to be deleted.
				elementPrev = elementNext;				// Next is the new prev and next should be advanced.

				bool localGreaterThan = false;
				bool localLessThan = true;
				bool localExactMatch = true;
				bool localSkipElement = true;
				elementPrev->template comparePayloadValue<nonRangeType>(func, localLessThan, localGreaterThan, localSkipElement, &localExactMatch);
				assert(localSkipElement || not localLessThan);
				assert(localSkipElement || localGreaterThan);
				assert(localSkipElement || not localExactMatch);

				DEBUG_LOG(INFO) << "New prev: " << *(elementPrev->getPayload());

				elementNext = elementNext->getFrontElementPointer();
				elementFront = elementNext;
				assert(not elementPrev || func > *(elementPrev->getPayload()));
				assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));
			} else {
				elementNext = elementNext->getFrontElementPointer();
			}

			if (not elementNext) break;				// Reached end of DLL

			// greaterThan = true iff *func greater than elementNext
			elementNext->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement);

			if (skipElement) continue;

			exactMatch = (not greaterThan && not lessThan) ? true : false;
			hasDeferredRemove = elementNext->getDeferredRemove();
			if (lessThan && not elementCur) {
//				DEBUG_LOG(INFO) << "Returning not found. Came looking for: " << func;
				return false;
			}
			assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));
		}
		if (not elementNext) {						// Reached end of DLL
			DEBUG_LOG(INFO) << "reached end of DLL";
			elementBack = nullptr;
		}
		if (not elementCur) {						// Element to be removed was not found in the DLL
//			DEBUG_LOG(INFO) << "Returning not found. Came looking for: " << func;
			DEBUG_LOG(INFO) << "Running verifyAscending";
			verifyAscending(__LINE__, false);
			//printDLL();
			//assert(0);
			return false;
		}

		assert(not elementPrev || func > *(elementPrev->getPayload()));
		assert(not elementNext || func < *(elementNext->getPayload()));
		assert(not elementPrev || not elementNext || *(elementPrev->getPayload()) < *(elementNext->getPayload()));

		next = elementNext;
		cur = elementCur;
		prev = elementPrev;
		removeReferenceBack = elementBack;
		removeReferenceFront = elementFront;

		DEBUG_LOG(INFO) << "Next: " << elementNext << " Prev: " << elementPrev << " Front: " << elementFront << " Back: " << elementBack;
		assert ((not nonRangeType || cur) && prev != next || (prev == nullptr && next == nullptr && removeReferenceFront));
		assert (removeReferenceFront || removeReferenceBack);

		assert(not prev || func > *(prev->getPayload()));
		assert(not next || func < *(next->getPayload()));
		assert(not prev || not next || *(prev->getPayload()) < *(next->getPayload()));

		numKilled = 1;
		return true;





		/******************************************
		while (not exactMatch) {
			if (backward) {
				elementCur = elementCur->getBackElementPointer();
			} else {
				elementCur = elementCur->getFrontElementPointer();
			}

			if (not elementCur) {
				return false;
			}
			comparePayload(elementCur, func, exactMatch, greaterThan);
			if (findMe) {
				exactMatch = (findMe == elementCur);
			}

			// We changed direction; clearly the element to remove in not in the list
			if (greaterThan && backward == true) { return false; }
			if (not greaterThan && backward == false) { return false; }
		}

		assert(elementCur != nullptr && exactMatch);

		// Now set the prev & next pointers along with front and back reference pointers

		elementNext = elementCur->getFrontElementPointer();
		elementPrev = elementCur->getBackElementPointer();
		elementBack = elementCur;
		elementFront = elementCur;

		while (elementNext && elementNext->getDeferredRemove()) {
			elementBack = elementNext;
			elementNext = elementNext->getFrontElementPointer();
		}

		while (elementPrev && elementPrev->getDeferredRemove()) {
			elementFront = elementPrev;
			elementPrev = elementPrev->getBackElementPointer();
		}
		next = elementNext;
		cur = elementCur;
		prev = elementPrev;
		removeReferenceBack = elementBack;
		removeReferenceBack = elementFront;
		assert (prev != next && cur);

		return true;
		******************************************/
	}


	/**
	 * Walks the DLL untill it finds a pair of Elements in the DLL such that the first Element's Payload is less 
	 * than insertPayload and that of the other Element is greater. Note that there might exist Elements between
	 * the two pair of Elements referred to above but have been marked for deferred remove. The funcion fails if
	 * it detects an Element in the DLL with a matching payload.
	 * 
	 * @param insertPayload
	 * 	The payload which is to be inserted in the DLL. Compare insertPayload to payloads of other
	 * 	elements to find the Element pair (details above). Should have operators > (greater than) and < (less
	 * 	than) defined.
	 *
	 * @param prev
	 * 	The Element after which insertPayload has to be inserted. This Element was not marked for deferred
	 * 	remove at the time of walking the DLL.
	 *
	 * @param next
	 * 	The Element before which insertPayload has to be inserted. Again, this Element did not have deferred
	 * 	remove set when walking the DLL.
	 * 	
	 * @param insertRefernceFront
	 * 	It is used as CAS reference for prev, when inserting the new payload after prev.
	 *
	 * @param insertRefernceBack
	 * 	It is used as CAS reference for next, when inserting the new payload before next.
	 *
	 * @return
	 * 	If an Element with matching payload is found, true is returned, false otherwise.
	 */

	template<bool nonRangeType = false, typename U>
	inline bool walkListRemoveBinarySearch(U&& func, PointerType& next, PointerType& prev, 
				PointerType& removeReferenceFront, PointerType& removeReferenceBack,
				PointerType& cur, PointerType& findMe, uint64_t& numKilled, 
				LockElement<T>* lockElementPrev = nullptr, LockElement<T>* lockElementNext = nullptr,
				bool* makeDLLGreatAgain = nullptr, PointerType* lastElement = nullptr, 
				PointerType* firstElement = nullptr, uint64_t* uniqueValue = nullptr) {

		if(UNLIKELY(front.isNullPointer())) {
			return false;
		}
		if (findMe != nullptr) {
			prev = findMe;
			register bool found = walkListRemove<nonRangeType>(func, prev, next, removeReferenceFront, removeReferenceBack, cur, findMe,
								numKilled);
			if (found) {
				assert(not prev || func > *(prev->getPayload()));
				assert(not next || func < *(next->getPayload()));
				assert(not prev || not next || *(prev->getPayload()) < *(next->getPayload()));
			}
			return found;
		}

		prev = front(); 							// Can be a nullptr by the time this is read
		removeReferenceFront = nullptr;
		removeReferenceBack = nullptr;
		next = nullptr;
		cur = nullptr;
		
		register uint64_t elementNumber =  numElements;
		register uint64_t moveBy = elementNumber/2;
		register bool reachedEnd = false;
		register bool reachedFront = false;
		register bool backward = false;
		register bool exactMatch = false;
		register bool lessThan = false;
		register bool greaterThan = false;
		register bool skipElement = false;

		register uint32_t startIndex = 0;
		register uint32_t endIndex = numIntermediates;
		register uint32_t midIndex = 0;


		register bool settMid = false;
		register bool usedRegular = false;
		register bool usedAnchors = false;

		if (not prev) return false;

		// cur is approximately set, there is no gurantee that it the element to be removed.
		while(moveBy > 1 && not reachedEnd && not reachedFront && prev) {
			/* func lessThan/greaterThan */
			removeReferenceBack = prev;					// Used as temp varible is prev reaches the end
			prev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement, &exactMatch);
			while (skipElement && prev) {
				assert(not skipElement);
				prev = prev->getFrontElementPointer();

				if (not prev) {
					prev = removeReferenceBack;
					lessThan = true;
					removeReferenceBack = nullptr;
				}

				prev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement);
			}
				
			if (exactMatch) {
				break;
			} else if (greaterThan) {
				backward = false; /* iterate forward */
				startIndex = midIndex;
			} else {
				backward = true; /* iterate backward */
				endIndex = midIndex;
			}

			moveBy = moveBy/2;
			if (endIndex != startIndex) {
				midIndex = (endIndex-startIndex)/2 + startIndex;
				PointerType temp = intermediates[midIndex];
				if (temp) {
					usedAnchors = true;
					prev = temp;
				} else {
					settMid = true;
					getMid(prev, moveBy, reachedEnd, reachedFront, backward, midIndex);
				}
			} else {
				usedRegular = true;
				getMid(prev, moveBy, reachedEnd, reachedFront, backward);
			}
		}
		DEBUG_LOG(INFO) << "Set mid: " << settMid;
		DEBUG_LOG(INFO) << "Used anchors: " << usedAnchors;
		DEBUG_LOG(INFO) << "Used reg. getMid: " << usedRegular;

		if (prev) {
			prev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement, &exactMatch);
			//DEBUG_LOG(INFO) << "Prev: " << *(prev->getPayload()) << " GreaterThan: " << greaterThan << " LessThan: " << lessThan << " w/ functor " << func;
		}

		assert((reachedFront && prev == nullptr) || (not reachedFront && prev != nullptr));

		if (reachedFront) prev = front();

		// The only guarantee we offer is that "cur" is somewhere near the element we want to remove.
		// All other pointers are set to nullptr.
		register bool found = false;

		if (nonRangeType) {
			found = walkListRemove<nonRangeType>(func, prev, next, removeReferenceFront, removeReferenceBack, cur, findMe, 
								numKilled);
		} else {
			found = walkListRemoveRange<nonRangeType>(func, prev, next, removeReferenceFront, removeReferenceBack, 
									lockElementPrev, lockElementNext, *makeDLLGreatAgain,
									numKilled, *lastElement, *firstElement, *uniqueValue);
		}

		if (LIKELY(found)) {
			assert ((not nonRangeType || cur) && prev != next || (prev == nullptr && next == nullptr && removeReferenceFront));
			assert (removeReferenceFront || removeReferenceBack);
			if (prev && not nonRangeType) assert(prev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
			return true;
		}
		return false;
	}

	
	/**
	 * Walks the DLL identifying Elements matching the payload range to be removed.
	 * 
	 * @param func
	 *	The functor that represents the payload range to be removed from the DLL. Compares
	 *	payloads of elements in the DLL, to find Element within payload range represented
	 *	by the range functor.
	 *
	 * @param prev
	 * 	The Element (not marked for deferred remove) just preceeding the Element to be removed.
	 *
	 * @param next
	 * 	The Element (not marked for deferred remove) immediately following the Element to be removed.
	 * 	
	 * @param removeReferenceFront
	 * 	The Element following prev which could be some element marked for deferred remove or can also
	 * 	be cur, whichever comes first in the DLL.
	 *
	 * @param removeReferenceBack
	 * 	The Element following curEnd which could be some element marked for deferred remove or curEnd
	 * 	itself, whichever is the last Element to be removed from the DLL.
	 *
	 * @param cur
	 * 	The Element matching the payload to be removed. It is guaranteed to be between prev and
	 * 	next.
	 *
	 * @param curEnd
	 * 	The last Element matching the payload to be removed. It is guaranteed to be between prev
	 * 	and next and does not have deferred remove set (when walkListRemove saw it).
	 *
	 * @param uniqueValue
	 *	The value if non-zero, is a request to find Elements with matching uniqueValue terminated
	 *	by a Element with payload outside the range to find.
	 *
	 *
	 * @return
	 * 	Retuns DLL_OP_ERRORCODE if Element range to be removed (possibly from the range) is 
	 * 	found in the DLL. If Element range to be removed is not found in the DLL, returns 
	 * 	DLL_OP_ERRORCODE::DLL_EINVAL. If while looking for curEnd an Element locked for insert 
	 * 	operation is found, DLL_OP_ERRORCODE::DLL_ERETRY is returned. If DLL_OP_ERRORCODE::DLL_SUCCESS
	 * 	is returned, prev is guaranteed to be locked for removeFrontOperation. 
	 */


	template<bool nonRangeType, typename U>
	bool walkListRemoveRange(U&& func, PointerType& prev, PointerType& next, 
				PointerType& removeReferenceFront, PointerType& removeReferenceBack,
				LockElement<T>* prevElement, LockElement<T>* nextElement, 
				bool& makeDLLGreatAgain, uint64_t& killed, PointerType& lastElement, 
				PointerType& firstElement, uint64_t uniqueValue) {
	
		DASSERT(func);
		makeDLLGreatAgain = false;
		uint64_t genCount = generationCount;

		while (true) { // Outer retry if any element could not be locked.
			register PointerType elementNext = next;
			register PointerType elementPrev = prev;
			register PointerType elementBack = removeReferenceBack;
			register PointerType elementFront = removeReferenceFront;
	
			if (not front.isNullPointer() && not elementPrev) {
				elementPrev = front();
			} else if (front.isNullPointer()) { 				// DLL is empty  
				return false;
			}
			if (not elementPrev) {						// DLL is empty
				return false;
			}

			register bool exactMatch = false;
			register bool greaterThan = false;
			register bool lessThan = false;
			register bool backward = false;
			register bool nextLockFailed = false;
			register bool removeLockFailed = false;
			register bool prevLockFailed = false;
			register PointerType elementCurEnd = nullptr;

			register bool skipElement = false;
			elementBack = elementPrev; 							// elementBack is used as a temp. variable
	
			elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement, &exactMatch);
			while (skipElement && elementPrev) {
				assert(not skipElement);
				skipElement = false;
				elementPrev = elementPrev->getFrontElementPointer();
				if (not elementPrev) {
					elementPrev = elementBack;
					lessThan = true;	
					break;
				}
				elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement);
			}

			exactMatch = (not greaterThan && not lessThan) ? true : false;
			if (lessThan || exactMatch) backward = true;
	
			bool hasDeferredRemove = false;
			//(1) Advance prev till prev is equal to *func or till payload not found (end of DLL)
			while (not backward && (not lessThan || skipElement)) {
				assert(not skipElement);
				elementBack = elementPrev; 			// Temporary refernece holder
				elementPrev = elementPrev->getFrontElementPointer();
	
				if (not elementPrev) break;

				elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement, &exactMatch);

				if (skipElement) continue;

				if (exactMatch) {
					assert (not elementCurEnd);
					elementCurEnd = elementPrev;
					break;
				}
			}

			if (not backward && not elementCurEnd) {
				verifyAscending(__LINE__, false, &lastElement, &firstElement, nullptr, nullptr, &uniqueValue);
				return false;
			}

			if (not elementPrev) { // We reach end of DLL so latch on to the last element in the DLL.
				elementPrev = elementBack;
			}
			//DEBUG_LOG(INFO) << "Backward: " << backward << " Next: " << elementNext << " Prev: " << elementPrev << " Front: " << elementFront << " Back: " << elementBack;
	
			greaterThan = true;
	
			//(2) Now rewind prev till prev is smaller than  *func and doesn't have deferredRemove set.
			while (elementPrev && (skipElement || lessThan || hasDeferredRemove || exactMatch)) {
				assert(not skipElement);
				elementPrev = elementPrev->getBackElementPointer();
	
				if (not elementPrev) break;
	
				elementPrev->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement, &exactMatch);
	
				if (skipElement) continue;
	
				hasDeferredRemove = elementPrev->getDeferredRemove();
			}
	
			bool deferredRemove = false;
			bool locked = false;
			if (elementPrev) {
				prevElement->lockingElement(elementPrev, OperationType::RemoveFrontOperation, true, deferredRemove, locked);		
				if (not locked) continue; // Restart the outer while loop.
				DEBUG_LOG(INFO) << "Checked prev lock status: " << *(elementPrev->getPayload());
				assert(elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
			}
			//DEBUG_LOG(INFO) << "Next: " << elementNext << " Prev: " << elementPrev << " Front: " << elementFront << " Back: " << elementBack;

			if (not elementPrev) { 						// Reached start DLL
				elementNext = front();
				elementFront = elementNext;
				elementBack = elementPrev;
			} else {
				elementNext = elementPrev->getFrontElementPointer();
				elementFront = elementNext;
				elementBack = elementPrev;
			}
			if (not elementNext) { 						// DLL is empty OR reached end of DLL
				prevElement->unlock();
				verifyAscending(__LINE__, false, &lastElement, &firstElement, nullptr, nullptr, &uniqueValue);
				return false;
			}
			DEBUG_LOG(INFO) << "Next: " << elementNext << " Prev: " << elementPrev << " Front: " << elementFront << " Back: " << elementBack;
	
			// lessThan = true iff elementNext greater than *func
			elementNext->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement, &exactMatch);
			hasDeferredRemove = elementNext->getDeferredRemove();
	
			int numberOfElementsLocked = 0;					// Keeps track of number of elements locked.
			PointerType firstLocked = nullptr;
			PointerType lastLocked = nullptr;
			bool nextNotFound = false; 					// Set to true if walkListRangeRemove needs to be
											// retried.
	
			elementCurEnd = nullptr;
	
			// (3) Now advance next (with elementBack following it) till next is greater than *func and doesn't have deferredRemove set
			// Switch elementPrev between values smaller than *func and doesn't have deferredRemove with elementFront an element ahead of it.
			// elementNext can equal *func at the beginning of this loop.
			while (elementNext && (greaterThan || exactMatch || hasDeferredRemove || skipElement)) {
				assert(not skipElement);
				if (exactMatch) {
					elementCurEnd = elementNext;
				}

				if (exactMatch && not hasDeferredRemove) {

					++killed;
					DEBUG_LOG(INFO) << "Killed: " << killed;

					elementCurEnd = elementNext;
					locked = elementCurEnd->lockElementForOperation(OperationType::RemoveOperation, nullptr, true);
					if (not locked) {
						nextNotFound = true;
						removeLockFailed = true;
						break;
					}

					if (not firstLocked) {
						firstLocked = elementNext;
					} else {
						lastLocked = elementNext;
					}
	
					register bool setDeferred = elementCurEnd->setDeferredRemove();
					if (setDeferred) {
						elementCurEnd->setRangeRemoveUnique(uniqueValue, false);
						elementCurEnd->setRemoveGenerationCount(genCount);
						elementCurEnd->setInRemoveList(removeList());
						while(not cas(removeList, elementCurEnd->getRemoveList(), elementCurEnd)) {
							elementCurEnd->setInRemoveList(removeList());
						}
						numElementsInRemoveList.fetch_add(1, std::memory_order_relaxed);
						decrementElementCount();

						if (elementCurEnd->getAnchored()) removeAnchor(elementCurEnd);
					}
				}
	
				elementBack = elementNext;
	
				// not greaterThan implies elementNext is smaller than *func
				if (greaterThan && not hasDeferredRemove && not skipElement) {// Next is smaller than element to be deleted.
					assert(not skipElement);
					prevElement->unlock();
					elementPrev = elementNext;			// Next is the new prev and next should be advanced.
					prevElement->lockingElement(elementPrev, OperationType::RemoveFrontOperation, true, deferredRemove, locked);		
					if (not locked) {
						prevLockFailed = true;
						nextNotFound = true;
						break;
					}
					elementNext = elementNext->getFrontElementPointer();
					elementFront = elementNext;
					if (elementPrev) DEBUG_LOG(INFO) << "Checked prev lock status: " << *(elementPrev->getPayload());
					assert (elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
				} else {
					elementNext = elementNext->getFrontElementPointer();
				}
	
				if (not elementNext) break;				// Reached end of DLL
	
				// greaterThan = true iff elementNext greater than *func
				elementNext->template comparePayloadValue<nonRangeType>(func, lessThan, greaterThan, skipElement, &exactMatch);

				if (skipElement) continue;

				hasDeferredRemove = elementNext->getDeferredRemove();

				if (lessThan && not elementCurEnd) {
					assert (not lastLocked && not firstLocked);
					verifyAscending(__LINE__, false, &lastElement, &firstElement);
					return false;
				}

				assert (elementNext != elementBack);
				assert (elementPrev != elementFront);
				assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));

			}
			//DEBUG_LOG(INFO) << "Next: " << elementNext << " Prev: " << elementPrev << " Front: " << elementFront << " Back: " << elementBack;
			bool nextLocked = false;						//!< Ensures that next is not locked twice
			if (nextNotFound) {
				if (firstLocked) {
					prevElement->unlock();
					firstLocked->unlockElement(OperationType::RemoveOperation);
					killed--;
					continue;
				} else if (lastLocked) {
					assert (not prevLockFailed);
					assert (lastLocked != elementNext);
					elementNext = lastLocked;
					nextElement->changeLockStatus(__LINE__, lastLocked);
					elementBack = elementNext->getBackElementPointer();
					lastLocked = elementBack;
					nextLocked = true;

					assert (elementNext != elementBack);
					assert (elementPrev != elementFront);
					if (elementPrev) DEBUG_LOG(INFO) << "Checked prev lock status: " << *(elementPrev->getPayload());
					assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));

				} else {
					prevElement->unlock();
					continue;
				}
			}
			if (not elementCurEnd) {						// Element to be removed was not found in the DLL
				assert (not lastLocked && not firstLocked);
				verifyAscending(__LINE__, false, &lastElement, &firstElement);
				return false;
			}

			assert (elementNext != elementBack);
			assert (elementPrev != elementFront);

			if (not elementNext) {							// Reached end of DLL
				elementBack = nullptr;		
			} else if (not nextLocked) {
				// Try to lock next. It unsuccessful either retry or shorten range
				nextElement->lockingElement(elementNext, OperationType::RemoveBackOperation, false, deferredRemove, locked);
				if (not locked) {
					nextLockFailed = true;
					if (firstLocked) {
						prevElement->unlock();
						firstLocked->unlockElement(OperationType::RemoveOperation);
						killed--;
						continue;
					} else if (lastLocked) {
						assert (lastLocked != elementNext);
						elementNext = lastLocked;
						nextElement->changeLockStatus(__LINE__, lastLocked);
						lastLocked = lastLocked->getBackElementPointer();

						if (elementPrev) DEBUG_LOG(INFO) << "Checked prev lock status: " << *(elementPrev->getPayload());
						assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));

					} else {
						prevElement->unlock();
						continue;
					}
				}
				elementBack = elementNext->getBackElementPointer();

				assert (elementNext != elementBack);
				assert (elementPrev != elementFront);
				nextLocked = true;
			}

			if (nextLockFailed || removeLockFailed) makeDLLGreatAgain = true;
			assert (not prevLockFailed);
	
			next = elementNext;
			prev = elementPrev;
			removeReferenceBack = elementBack;
			removeReferenceFront = elementFront;
			lastElement = lastLocked;
			firstElement = firstLocked;

			assert (elementNext != elementBack);
			assert (elementPrev != elementFront);
			assert (not elementPrev || elementPrev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
			assert (not prev || prev->verifyElementLockStatus(OperationType::RemoveFrontOperation));
			assert (elementCurEnd || lastLocked || firstLocked);

//			DEBUG_LOG(INFO) << "Next: " << elementNext << " Prev: " << elementPrev << " Front: " << elementFront << " Back: " << elementBack;

			assert (not nonRangeType && prev != next || (prev == nullptr && next == nullptr && removeReferenceFront));
			assert (removeReferenceFront || removeReferenceBack);
	
			return true;
		}
	}




	//===============================================================================================
	// FIND SECTION BEGINS
	//===============================================================================================
	

	/**
	 * find() walks the DLL searching for a payload that is considered as a match by the payloadMatch
	 * functor.
	 *
	 * The payloadMatch functor returns a std::tuple<bool match, bool greaterThan> such that match is
	 * set to true if the payload matches the criteria of payloadMatch. payloadMatch sets greaterThan
	 * to true if the payload passed to it is greater than its match criteria.
	 *
	 * @return
	 * 		Returns the payload if a match is detected and nullptr otherwise.
	 */
	
	template<typename U>
	const T find(U&& func) {
		incrementActiveCount();
		register uint64_t myGenCount = generationCount;
		register PointerType cur = front();
		if (not cur) {
			decrementActiveCount(myGenCount);
			return nullptr;
		}
		register bool match = false;
		register bool greaterThan = false;
		register bool lessThan = false;
		register bool skipElement = false;

		while (cur) {
			if (cur->getDeferredRemove()) {
				cur = cur->getFrontElementPointer();
				continue;
			}

			cur->template comparePayloadValue<true>(func, lessThan, greaterThan, skipElement);
	
			if (skipElement) {
				assert(not skipElement);
				cur = cur->getFrontElementPointer();
				continue;
			}	
	
			match = (not greaterThan && not lessThan) ? true : false;

			if (match) {
				decrementActiveCount(myGenCount);
				return cur->getPayload();
			}
			if (lessThan) {
				decrementActiveCount(myGenCount);
				return nullptr;
			}
			cur = cur->getFrontElementPointer();
		}

		decrementActiveCount(myGenCount);
		return nullptr;
	}


	/**
	 * findRange() walks the DLL in search of all payloads that fit within the range represented by
	 * the functor.
	 * 
	 * @param func
	 * 	Supplied by the user, func is a functor that returns true if a payload falls within the
	 * 	range the func represents. Else false is returned. The functor cannot be a filter, since
	 *	findRange() terminates finding new elements immediately after the functor returns false 
	 * 	after returning true.
	 * 
	 * @return
	 * 	Returns a ListHead of a newly created DLL that the user can use to traverse the DLL.
	 */

	std::shared_ptr<ListHead<T>> findRange(std::function<bool(T)> func) {
		std::shared_ptr<ListHead<T>> dllList = std::make_shared<ListHead<T>>();
		findRange(*dllList, func);
		return dllList;
	}

	void findRange(ListHead<T>& dllList, std::function<bool(T)> func) {
		incrementActiveCount();
		uint64_t myGenCount = generationCount;
		GenericPointer<PointerType, false> cur = front;
		if (cur.isNullPointer()) {
			decrementActiveCount(myGenCount);
			return;
		}

		while (not cur.isNullPointer() && (cur()->getDeferredRemove() || (func(cur()->getPayload()) == false))) {
			cur = cur()->getFrontPointer();
		}
 
		while (not cur.isNullPointer() && func(cur()->getPayload()) == true) {
			if (cur()->getDeferredRemove()) {
				continue;
			}
			dllList.insert(cur()->getPayload());
			cur = cur()->getFrontPointer();
		}
		
		decrementActiveCount(myGenCount);
		return;
	}
		

	//========================================================================================
	// Generally useful (or useless) functions
	//========================================================================================


	/*
	 * A helper function that returns a unique value per DLL each time the routine is called.
	 */
	inline uint64_t getUniqueValue64() {
		return uniqueValue64.fetch_add(1, std::memory_order_relaxed)+1;
	}


	/**
	 * Compare the payload passed with that represented by func functor and returns a tuple
	 * of two boolean values (exactMatch and greaterThan).
	 *
	 * @param func
	 * 	The functor that represents the value to be compared with that wrapped by next.
	 * 	Func should define operators > (greater than) and < (less than).
	 *
	 * @param wrapper
	 * 	The GenericPointer (or PointerType) type from DLL that wraps some payload.
	 *
	 * @return
	 * 	Returns std::tuple<bool exactMatch, bool greaterThan> where exactMatch is set
	 * 	to true if payload is an exact match to that wrapped by next. greaterThan is
	 * 	set to true if the value wrapped by wrapper is greater than payload in functor.
	 */

	template<typename U>
	inline void comparePayload(GenericPointer<PointerType, false>& wrapper, U&& func, bool& exactMatch, bool& greaterThan) {
		DASSERT(func != nullptr);
		DASSERT(not wrapper.isNullPointer());
		func(wrapper()->getPayload(), exactMatch, greaterThan);
	}

	template<typename U>
	inline void comparePayload(PointerType& wrapper, U&& func, bool& exactMatch, bool& greaterThan) {
		DASSERT(func != nullptr);
		DASSERT(wrapper);
		func(wrapper->getPayload(), exactMatch, greaterThan);
	}


	template<bool W = true, bool F = true>
	inline bool cas(GenericPointer<PointerType, true>& changeMe, GenericPointer<PointerType, W>& expected,
			GenericPointer<PointerType, F>& pointToMe) {
		return changeMe.cas(expected, pointToMe);
	}
	
	template<bool W = true>
	inline bool cas(GenericPointer<PointerType, true>& changeMe, GenericPointer<PointerType, W>& expected,
			PointerType& pointToMe) {
		return changeMe.cas(expected, pointToMe);
	}
	
	inline bool cas(GenericPointer<PointerType, true>& changeMe, PointerType& expected,
			PointerType& pointToMe) {
		return changeMe.cas(expected, pointToMe);
	}

	/**
	 * Traverse all elements in the DLL making sure they are in an ascending order.
	 */
	/*******************************************************************
	bool verifyAscending(bool verifyElementUnlocked = true) {
		GenericPointer<PointerType, false> cur = front;
		T payload;
		if (not cur.isNullPointer()) {
			if (verifyElementUnlocked && not cur()->verifyElementLockStatus(OperationType::NoOperation)) {
				DEBUG_LOG(TRACE) << "Element locked" << *(cur()->getPayload());
				assert(0);
			}
			payload = cur()->getPayload();
		}
		while (not cur.isNullPointer()) {
			PointerType prev = cur();
			cur = cur()->getFrontPointer();
			if (cur.isNullPointer()) {
				return true;
			}
			if (verifyElementUnlocked && not cur()->verifyElementLockStatus(OperationType::NoOperation)) {
				DEBUG_LOG(TRACE) << "Element locked" << *(cur()->getPayload());
				assert(0);
			}
			
			T nextPayload = cur()->getPayload();
			PointerType temp = cur();
			auto nextValue = *nextPayload;
			auto currentValue = *payload;
			if (nextValue <= currentValue) {
				DEBUG_LOG(TRACE) << "Assert failure: nextPayload " << nextValue << " currentValue " << currentValue << " deferredRemove on next" << temp->getDeferredRemove() << " removeList of next " << (temp->getRemoveList())() << " next address: " << temp << " current address: " << prev << " DR on cur " << prev->getDeferredRemove() << " removeList of current " << (prev->getRemoveList())();

				printDLL();
				assert(nextValue > currentValue);
			}
//			payload = nextPayload;
		}
		return true;
	}
	*******************************************************************/

	void verifyAscending(int line, bool verifyElementUnlocked = true, PointerType* last = nullptr, PointerType* first = nullptr, 
				PointerType removePrev = nullptr, PointerType removeNext = nullptr, uint64_t* uniqueValue = nullptr) {
		register PointerType firstElement = nullptr;
		register PointerType lastElement = nullptr;

		std::stringstream ss;		

		if (first && last) {
			firstElement = *first;
			lastElement = *last;
		}

		PointerType cur = front();
		T prevPayload = nullptr;
		if (cur) {
			ss << "Line: " << line << std::endl;
			if (removeNext && removePrev) {
				ss << " removePrev @ " << removePrev << " Payload: " << *(removePrev->getPayload()) << " w/ Front: " 
					<< removePrev->getFrontElementPointer() << " Back: " << removePrev->getBackElementPointer() 
					<< " hasDeferredRemove " << removePrev->getDeferredRemove() << " RemoveList: " << (removePrev->getRemoveList())()<< std::endl;
				ss << " removeNext: " << removeNext << " Payload: " << *(removeNext->getPayload()) << " w/ Front: " 
					<< removeNext->getFrontElementPointer() << " Back: " << removeNext->getBackElementPointer() 
					<< " hasDeferredRemove " << removeNext->getDeferredRemove() << " RemoveList: " << (removeNext->getRemoveList())()<< std::endl;
			}
			ss << " Payload: " << *(cur->getPayload()) << " @ " << cur << " w/ Front: " << cur->getFrontElementPointer() << " Back: " << cur->getBackElementPointer() << " hasDeferredRemove " << cur->getDeferredRemove() << " uniqueValue " << cur->getUniqueValue() << std::endl;
			prevPayload = cur->getPayload();
			if (verifyElementUnlocked && not cur->verifyElementLockStatus(OperationType::NoOperation)) {
				DEBUG_LOG(TRACE) << "Element locked" << *(cur->getPayload());
				assert(0);
			}
			if (uniqueValue && *uniqueValue == cur->getUniqueValue()) {
				DEBUG_LOG(TRACE) << ss;
				assert(0);
			}
		} else { return; }	
		
		cur = cur->getFrontElementPointer();
		T currentPayload = prevPayload;
		PointerType prev = cur;
		while (cur) {
			prevPayload = currentPayload;
			prev = cur;
			cur = cur->getFrontElementPointer();

			if (not cur) break;

			ss << "Payload: " << *(cur->getPayload()) << " @ " << cur << " w/ Front: " << cur->getFrontElementPointer() << " Back: " << cur->getBackElementPointer() << " hasDeferredRemove " << cur->getDeferredRemove() << " remove genenration count " << cur->getRemoveGenerationCount() << " and head->genCount  " << generationCount << std::endl;

			currentPayload = cur->getPayload();
			auto currentPayloadValue = *currentPayload; 
			auto prevPayloadValue = *prevPayload;

			if (cur == firstElement || cur == lastElement) {
				ss << " firstElement: " << firstElement << " lastElement: " << lastElement;
				DEBUG_LOG(TRACE) << ss;
				assert (cur != firstElement);
				assert (cur != lastElement);
			}

			if (uniqueValue && *uniqueValue == cur->getUniqueValue()) {
				DEBUG_LOG(TRACE) << ss;
				assert(0);
			}

			if (verifyElementUnlocked && not cur->verifyElementLockStatus(OperationType::NoOperation)) {
				DEBUG_LOG(TRACE) << "Element locked" << *(cur->getPayload());
				assert(0);
			}

			if (currentPayloadValue <= prevPayloadValue) {// && not cur->getDeferredRemove()) {
				if (firstElement) DEBUG_LOG(TRACE) << "First element: " << firstElement << " Last element: " << lastElement;
				if (lastElement && cur == lastElement->getFrontElementPointer()) DEBUG_LOG(TRACE) << "Next found";

				DEBUG_LOG(TRACE) << " currentPayloadValue " << currentPayloadValue << " @ " << cur << " DR on cur" << cur->getDeferredRemove() << " cur removeList" << (cur->getRemoveList())() << " w/ generation count " << (cur->getRemoveGenerationCount());
				DEBUG_LOG(TRACE) << " prevPayloadValue " << prevPayloadValue  << " @ " << prev << " DR on prev " << prev->getDeferredRemove() << " prev removeList " << (prev->getRemoveList())();

				DEBUG_LOG(TRACE) << ss;
				assert (prevPayloadValue < currentPayloadValue);
			}
		}
	}


	bool printDLL() {
		PointerType cur = front();
		int elementIndex = 0;
		if (not cur) {
			DEBUG_LOG(TRACE) << "The DLL is empty";
		}
		while (cur) {
			++elementIndex;
			DEBUG_LOG(TRACE) << "Payload: " << *(cur->getPayload()) << " @ " << cur << " w/ Front: " << cur->getFrontElementPointer() << " Back: " << cur->getBackElementPointer() << " hasDeferredRemove " << cur->getDeferredRemove() << std::endl;
			cur = cur->getFrontElementPointer();
		}
		return true;
	}


	inline uint64_t getNumberOfElements() {
		return numElements.load(std::memory_order_consume);
	}

	inline uint64_t getActiveCount() {
		return activeCount.load(std::memory_order_consume);
	}


	friend class Iterator<T>;

	Iterator<T> begin() {
		return Iterator<T>(*this, generationCount, front);
	}

	Iterator<T> end() {
		return Iterator<T>(*this, generationCount);
	}



private:

	inline void printRemoveList() {	
		PointerType cur = removeList();
		int elementIndex = 0;
		if (not cur) {
			DEBUG_LOG(TRACE) << "Remove list is empty";
		}
		while (cur) {
			++elementIndex;
			DEBUG_LOG(TRACE) << "Payload: " << *(cur->getPayload()) << " @ " << cur << " w/ removeList: " << (cur->getRemoveList())() << 
				" hasDeferredRemove " << cur->getDeferredRemove() << " remove generation count: " << cur->getRemoveGenerationCount() << std::endl;
			cur = (cur->getRemoveList())();
		}
	}


	/**
	 * Advances startElement (and reference) using either front or back pointer based on value of direction and times specified by 
	 * iterateNumber.
	 *
	 * @param startElement
	 * 	The element which is to be advanced.
	 *
	 * @param iterateNumber
	 * 	Specifies the number of times startElement is to be advanced
	 *
	 * @param reachedEnd
	 * 	If while advancing startElement, the last element in the DLL is reached, this is assigned to true.
	 *
	 * @param reachedFront
	 * 	If while advancing startElement, the first element in the DLL is reached, this is assigned to true.
	 *
	 * @param backward
	 * 	If true, the DLL is traversed backward (using back pointers), else the DLL is traversed forward (using front pointers).
	 */

	inline void getMid(PointerType& startElement, size_t iterateNumber, bool& reachedEnd, bool& reachedFront, bool backward, int index = -1) {
		reachedEnd = false;
		reachedFront = false;
		register PointerType start = startElement;
		register PointerType preStart = nullptr;
		if (start && not backward /* false = forward iteration */) { 				// go forward
			for (size_t i = 0; i < iterateNumber; ++i) {
				preStart = start;
				start = start->getFrontElementPointer();
				if (not start) {
					start = preStart;
					reachedEnd = true;
					break;
				}
			}
		} else if (start && backward) { 							// go backward
			for (size_t i = 0; i < iterateNumber; ++i) {
				start = start->getBackElementPointer();
				if (not start) {
					reachedFront = true; // reference is now our basis for next pointer.
					break;
				}
			}
		}
		startElement = start;
		if (index != -1) {
			if (start && not start->getDeferredRemove()) {
				if (start->setAnchoredToTrue()) {
					intermediates[index] = start;
					if (start->getDeferredRemove()) {
						intermediates[index] = nullptr;
						start->setAnchoredToFalse();	
					}
				}
			}
		}
		return;
	}

	inline void incrementActiveCount() {
		if (numElementsInRemoveList >= 1000) {
			int currentGenerationCount = generationCount;
			while (currentGenerationCount == generationCount) {
				std::this_thread::yield();
			}
		}
		activeCount.fetch_add(1, std::memory_order_relaxed);
	}

	inline void decrementActiveCount(uint64_t myGenCount) {
		uint64_t newActiveCount = activeCount.fetch_sub(1, std::memory_order_relaxed);
		if (activeCount > 1000000) {
			// This kinda doesn't happen unless something went seriously wrong.
			assert(0);
		}
		// Just so you know, fetch_sub() returns the value prior to decrement. So we check with previous value being 1.
		if (newActiveCount == 1) {
			if (generationCount.compare_exchange_weak(myGenCount, myGenCount+1, std::memory_order_release, std::memory_order_consume)) {
				deleteFromRemoveList(myGenCount+1);
			} else {
				deleteFromRemoveList(myGenCount);
			}
		}
		return;
	}

	inline void incrementElementCount(uint64_t number = 1) {
		DASSERT(number > 0);
		DASSERT(numElements >= 0);
		numElements.fetch_add(number, std::memory_order_relaxed);
	}

	inline void decrementElementCount(uint64_t number = 1) {
		DASSERT(number > 0);
		numElements.fetch_sub(number, std::memory_order_relaxed);
		DASSERT(numElements >= 0);
	}

	inline void incrementOperationCounter() {
		register uint64_t operationCounter = insertRemoveCounter.fetch_add(1, std::memory_order_relaxed);
		register uint64_t numberOfElements = numElements;
		if (numberOfElements > MIN_NUMELEMENTS && operationCounter * 10 >= numberOfElements) {
			insertRemoveCounter.fetch_sub(insertRemoveCounter, std::memory_order_relaxed);
			register PointerType temp = nullptr;
			for (int i = 0; i < numIntermediates; ++i) {
				temp = intermediates[i];
				intermediates[i] = nullptr;

				if (temp) temp->setAnchoredToFalse();
			}
		}
	}

	/**
	 * Remove element from ListHead::intermediates
	 *
	 * @param element
	 *	The element to be remove from the array
	 */
	inline void removeAnchor(PointerType& element) {
		register PointerType elementAnchor = element;
		for(int i = 0; i < numIntermediates; i++) {
			if (elementAnchor == intermediates[i]) {
				intermediates[i] = nullptr;
				elementAnchor->setAnchoredToFalse();
				return;
			}
		}
	}
		
	/**
	 * Walks the DLL to check visibility of the element that is an exact match to the argument.
	 * Used by insert implementation to make sure a range remove did not race past itself and
	 * implicitly remove the said insert operation.
	 *
	 * @param findMe
	 * 	The PointerType whose visibility in the DLL is to be ascertained.
	 *
	 * @return
	 *	Returns true if the element is visible, false otherwise.
	 */
	inline bool checkVisibilityInDLL(PointerType findMe) {
		PointerType cur = nullptr;
		
		if (not front.isNullPointer()) {
			cur = front;
		} else {
			return false;
		}
	
		register bool exactMatch = false;
		register bool greaterThan = false;

		while (not exactMatch && not greaterThan && not cur == nullptr) {
//			uint64_t currentGenerationCount = cur()->getRemoveGenerationCount();
//			DASSERT(currentGenerationCount == localGenerationCount || currentGenerationCount == 0UL);
			exactMatch = (findMe == cur);
			if (UNLIKELY(exactMatch)) {
				return true;
			}
			greaterThan = *(findMe->getPayload()) < *(cur()->getPayload());
			if (UNLIKELY(greaterThan)) {
				return false;
			}

			cur = (cur()->getFrontPointer())();
		}
		return false;
	}


	std::atomic<uint64_t> activeCount;						//!< How many threads are active on the DLL
	std::atomic<uint64_t> generationCount;						//!< How many times activeCount went to zero 
	std::atomic_bool removeOperationInProgress;					//!< Indicates if any thread is processing 
											//!< removeList

	GenericPointer<PointerType, true> front;					//!< Points to first element in DLL 
	GenericPointer<PointerType, true> removeList;					//!< Points to elements set for removal          
	std::atomic<uint64_t> numElements;						//!< Number of elements in the DLL
	std::atomic<uint64_t> uniqueValue64;						//!< Generates uniqueVal for rangeRemove threads
	uint64_t numIntermediates;							//!< Single level skip list intermediate elements
	PointerType* intermediates;							//!< Contains the actual intermediate pointers
	std::atomic<size_t> insertRemoveCounter;					//!< Counts the number of inserts/removes in between
											//!<  calculating intermediate values
	std::atomic<bool> operationInProgress;
	bool yield;
	std::atomic<size_t> numElementsInRemoveList;
};
