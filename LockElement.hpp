/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#pragma once

#include "GenericPointer.hpp"
#include "Element.hpp"
#include "LockingMetrics.hpp"
#include "OperationType.hpp"

#include <iostream>

template<typename T>
class LockElement {
	using PointerType = Element<T>*;
public:
	inline LockElement(NameType elementName, bool enableMetrics)
	    : element(nullptr),
	      metricsEnabled(enableMetrics),
	      lockedByMe(false),
	      ongoingOperation(OperationType::NoOperation),
	      metricsPointer(nullptr),
	      operationCounter(0UL) {
		if (enableMetrics) {
			metricsPointer = new LockingMetrics(enableMetrics, elementName);
		}
		return;
	}

	
	
	/**
	 * Locks the given element for operation named by argument operation.
	 * If element was set for deferredRemove changes the deferredRemove argument 
	 * to true. If it was unable to lock the element for the specified operation
	 * return false (by argument locked).
	 */
	inline LockElement(PointerType element, OperationType operation,
			bool tryHard, NameType elementName, bool enableMetrics, 
			bool& deferredRemove, bool& locked)
	    : element(element),
	      metricsEnabled(enableMetrics),
	      lockedByMe(false),
	      ongoingOperation(OperationType::NoOperation),
	      metricsPointer(nullptr) {

		locked = false;
		deferredRemove = false;
		if (enableMetrics) {
			metricsPointer = new LockingMetrics(enableMetrics, elementName);
		}
		locked = element->lockElementForOperation(operation, metricsPointer, tryHard);
		if (locked) {
			deferredRemove = element->getDeferredRemove();
			element->verfyElementLockStatus(operation);
			ongoingOperation = operation;
			operationCounter = element->getOperationCounter();
		}
		lockedByMe = locked;
	}


	inline void lockingElement(PointerType myElement, OperationType operation, bool tryHard, bool& deferredRemove, bool& locked) {
		DASSERT(not lockedByMe);
		element = myElement;
		locked = false;
		deferredRemove = false;
		locked = element->lockElementForOperation(operation, metricsPointer, tryHard);
		if (locked) {
			operationCounter = myElement->getOperationCounter();
			deferredRemove = element->getDeferredRemove();
			ongoingOperation = operation;
			if (not ongoingOperation & (operationCounter >> 32)) DEBUG_LOG(TRACE) << "OperationCounter: " << operationCounter;

			assert(ongoingOperation & (operationCounter >> 32));
			assert(operationCounter & 0xffffffff);
			lockedByMe = true;
			return;
		}
		//DEBUG_LOG(TRACE) << "Lock failed: operation: " << operationTypeAsString(operation);
		lockedByMe = false;
		return;
	}

	inline void unlock() {
		if (lockedByMe) {
			operationCounter = element->getOperationCounter();
			if (not ongoingOperation & (operationCounter >> 32)) DEBUG_LOG(TRACE) << "OperationCounter: " << operationCounter;

			assert(ongoingOperation & (operationCounter >> 32));
			assert(operationCounter & 0xffffffff);

			bool success = element->unlockElement(ongoingOperation);
			DASSERT(success);
			lockedByMe = false;
			element = nullptr;
		}
	}

	void changeLockStatus(int LINE, PointerType myElement) {
		if (ongoingOperation != OperationType::NoOperation) {
			DEBUG_LOG(TRACE) << "Called from: " << LINE << " OngoingOperation: " << ongoingOperation;
		}
		assert(ongoingOperation == OperationType::NoOperation);
		element = myElement;		
		ongoingOperation = OperationType::RemoveBackOperation;
		element->changeRemoveLockToRemoveBack();
	}

	/**
	 * Unlocks the element for the operation he had locked it for.
	 */
	inline ~LockElement() {
		if (lockedByMe) {
			operationCounter = element->getOperationCounter();
			DASSERT (ongoingOperation & (operationCounter >> 32));
			DASSERT (operationCounter & 0xffffffff);

			element->unlockElement(ongoingOperation);
			lockedByMe = false;
			element = nullptr;
		}
		if (metricsPointer) delete metricsPointer;
	}

private:
	PointerType element;
	bool metricsEnabled;
	bool lockedByMe = false;
	OperationType ongoingOperation;
	LockingMetrics* metricsPointer;
	uint64_t operationCounter;
};
