/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

/********************************************************************************************************************
#include "GenericPointer.hpp"
#include "Logging.hpp"

#include <atomic>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>

using PointerType = std::shared_ptr<int>;

class A {
public:
	A() : value(0) {}
	A(GenericPointer<PointerType> arg) : pointer(arg) {}

	friend std::ostream& operator<<(std::ostream& inStream, const A& a) {
		inStream << std::to_string(reinterpret_cast<unsigned long int>(&a)) << " : " << std::to_string(a.value) << std::endl;
		return inStream;
	}

private:
	int value;
	GenericPointer<PointerType> pointer;
};

void foo(GenericPointer<PointerType> temp) {
	TEST_DEBUG_LOG(TRACE)<< "GenericPointer copy constructed from RValue reference." << std::endl;
}

void casSwap(GenericPointer<PointerType>* changeThis, PointerType* expected, PointerType* changeTo, int threadNumber) {
	while (not changeThis->cas(*expected, *changeTo)) {
	}
	TEST_DEBUG_LOG(TRACE)<< "Exiting thread: " << threadNumber << std::endl;
}

static constexpr int NUMTHREADS = 100;

int main() {
	int j = 9;
	int m = 2;

	GenericPointer<PointerType> g1 = std::make_shared<int>(j);
	TEST_DEBUG_LOG(TRACE)<< "Creating GenericPointer from element address : " << "\t\tSuccess" << std::endl;

	foo(GenericPointer<PointerType>());
	TEST_DEBUG_LOG(TRACE)<< "Passing empty GenericPointer<T*> by const reference : " << "\tSuccess" << std::endl;

	GenericPointer<PointerType> g2 = std::make_shared<int>(m);
	g1 = g2;

	if(g1 == g2) {
		TEST_DEBUG_LOG(TRACE)<< "Testing operator= and operator== : " << "\t\t\tSuccess" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "Testing operator= and operator== : " << "\t\t\tFail" << std::endl;
	}

	GenericPointer<PointerType> casBase = std::make_shared<int>(1234);
	std::shared_ptr<int> k = casBase();
	std::shared_ptr<int> array[NUMTHREADS];

	for (int i = 0; i < NUMTHREADS; ++i) {
		array[i] = std::make_shared<int>(i);
	}
	std::thread threads[NUMTHREADS];
	for (int i = 0; i < NUMTHREADS-1; ++i) {
		threads[i] = std::thread(casSwap, &casBase, &array[NUMTHREADS-i-2], &array[NUMTHREADS-i-1], i+1);
	}

	casSwap(&casBase, &k, &array[0], 0);

	for (int i = 0; i < NUMTHREADS-1; ++i) {
		threads[i].join();
	}

	if (*(casBase()) == NUMTHREADS-1) {
		TEST_DEBUG_LOG(TRACE)<< "CAS race between " << NUMTHREADS-1 << " threads : " << "\t\t\t\tSuccess"	<< std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "CAS race between " << NUMTHREADS-1 << " threads : " << "\t\t\t\tFailure"	<< std::endl;
	}

	return 0;
}
********************************************************************************************************************/
