/***********************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
***********************************************************/

#include "Element.hpp"
#include "GenericPointer.hpp"
#include "IsSharedPtr.hpp"
#include "OperationType.hpp"

#include <iostream>
#include <execinfo.h>
#include <thread>


/*****************************
 * SUMMARY OF TEST:
 *	Number of Element<T> created = 6 + NUMELEMENTS
 *	Number of GenericPointer created = 6*2 + NUMELEMENTS*2
 *	Number of times GenericPointer = operator called = 1
 *	Number of times cas called on back pointer of an Element = NUMELEMENTS*2 + 2
 *	Number of times cas called on front pointer of an Elment = called in a while loop so 
 *								the exact number cannot be calculated
 *	Number of times setRemoveGenerationCount called = 1
 *	Number of times getRemoveGenerationCount called = 1
 *	Number of times lock operation called = 4
 *	Number of times unlock operation called = 3
 *	Number of times getFrontPointer called = 4 + NUMELEMENTS*2
 *	Number of times getBackPointer called = 5 + NUMELEMENTS*2
 *	Number of times getRemoveList called = 2
 *	Number of times getPayload called = 2
 *	Number of times verifyElementLockStatus called = 2
******************************/

const static int NUMELEMENTS = 200;
const static int MAXSTACKDEPTH = 4096;

void stack_trace() {
    void *array[MAXSTACKDEPTH];
    size_t size;
    char **strings;
    
    size = backtrace(array, MAXSTACKDEPTH);
    strings = backtrace_symbols(array, size);

    printf("\n\nStack Trace =============================================\n");
    for (size_t i = 0; i < size; ++i) {
        printf("%s\n", strings[i]);
    }
    printf("\n=========================================================\n\n");
    free(strings);
}

void insertElement(Element<std::shared_ptr<int>>* source, Element<std::shared_ptr<int>>* reference, Element<std::shared_ptr<int>>* changeTo, GenericPointer<Element<std::shared_ptr<int>>*, true>* sourceReference, int threadNum) {
	register uint64_t counter = 0;
	while (source->setFrontPointer(reference, changeTo) == false) {
		counter++;
                if (counter == SPIN_YIELD_COUNT) {
			std::this_thread::yield();
			counter = 0;
		}
	}
	reference->setBackPointer((*sourceReference), changeTo); 
}

int main() {

	std::shared_ptr<int> number = std::make_shared<int> (1234);
	Element<std::shared_ptr<int>> b (number);
	if (*(b.getPayload()) == *number){
		TEST_DEBUG_LOG(TRACE)<< "Test getPayload() function and Element<T>constructor:\t\t\t\tSuccess"<< std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "Test getPayload() function and Element<T> constructor:\t\t\t\tFailure"<< std::endl;
	}

	/** Test for testing getFrontPointer and getBackPointer */
	DASSERT((b.getFrontPointer()).isNullPointer() == true);
	DASSERT((b.getBackPointer()).isNullPointer() == true);
	DASSERT((b.getRemoveList()).isNullPointer() == true);

	b.setRemoveGenerationCount(1);
	if (b.getRemoveGenerationCount() == 1 && b.getDeferredRemove()) {
		TEST_DEBUG_LOG(TRACE)<< "Test setRemoveGenrationCount and getRemoveGenarationCount:\t\t\tSuccess" << std::endl;
		DASSERT(b.getDeferredRemove() == true);
		TEST_DEBUG_LOG(TRACE)<< "Test setDeferredRemove and getDeferredRemove:\t\t\t\t\tSuccess" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "Test setRemoveGenrationCount and getRemoveGenarationCount:\t\t\tFailure" << std::endl;
		TEST_DEBUG_LOG(TRACE)<< "Test setDeferredRemove and getDeferredRemove:\t\t\t\tFailure" << std::endl;
	}

	if (b.lockElementForOperation(OperationType::InsertOperation, nullptr) == true) {
		TEST_DEBUG_LOG(TRACE)<< "Test lockElementForOperation, getOperationStatus, and generateOperationCount:\tSuccess" << std::endl;
	}
	b.lockElementForOperation(OperationType::InsertBackOperation, nullptr);

	b.verifyElementLockStatus(OperationType::InsertOperation);
	b.verifyElementLockStatus(OperationType::InsertBackOperation);

	if(b.unlockElement(OperationType::InsertOperation) == true && b.unlockElement(OperationType::InsertBackOperation) == true) {
		TEST_DEBUG_LOG(TRACE)<< "Test unlockElement:\t\t\t\t\t\t\t\tSuccess" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "Test unlockElement:\t\t\t\t\t\t\t\tFailure" << std::endl;
	}

	// Test locking for conflicting types, remove and insert operation
	b.lockElementForOperation(OperationType::RemoveOperation, nullptr);
	b.lockElementForOperation(OperationType::InsertOperation, nullptr);		// This should fail
	b.verifyElementLockStatus(OperationType::RemoveOperation);			// If lock for insert succeded, this will blow an assert
	b.unlockElement(OperationType::RemoveOperation);


	// Test remove back with insert operation compatibility as well as remove back with remove front compatibility
	b.lockElementForOperation(OperationType::RemoveBackOperation, nullptr);
	b.lockElementForOperation(OperationType::InsertOperation, nullptr);
	b.verifyElementLockStatus(OperationType::RemoveBackOperation);
	b.verifyElementLockStatus(OperationType::InsertOperation);
	b.unlockElement(OperationType::InsertOperation);

	b.lockElementForOperation(OperationType::RemoveFrontOperation, nullptr);
	b.verifyElementLockStatus(OperationType::RemoveBackOperation);
	b.verifyElementLockStatus(OperationType::RemoveFrontOperation);
	b.unlockElement(OperationType::RemoveFrontOperation);
	b.unlockElement(OperationType::RemoveBackOperation);

	TEST_DEBUG_LOG(TRACE) << "Test multiple lock compatibility: \t\t\t\t\t\tSuccess" << std::endl;

	if(b.getElementOperationLockStatus() == OperationType::NoOperation) {
		TEST_DEBUG_LOG(TRACE)<< "Test unlockElement correctness:\t\t\t\t\t\tSuccess" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "Test unlockElement correctness:\t\t\t\t\t\tFailure : got : " << operationTypeAsString(b.getElementOperationLockStatus())<< std::endl;
	}

	std::shared_ptr<int> numberFront = std::make_shared<int> (2345);
	Element<std::shared_ptr<int>> c(numberFront);
	Element<std::shared_ptr<int>> c1(std::make_shared<int>(2345));
	b.setFrontPointer(b.getFrontPointer(), &c);

	std::shared_ptr<int> numberBack = std::make_shared<int> (3456);
	Element<std::shared_ptr<int>> d(numberBack);
	b.setBackPointer(b.getBackPointer(), &d);

	std::shared_ptr<int> removeList = std::make_shared<int> (4567);
	Element<std::shared_ptr<int>> e(removeList);
	b.setInRemoveList(&e);
	
	assert(b.getFrontPointer()() == &c);
	assert(b.getBackPointer()() == &d);
	assert(not b.getRemoveList().isNullPointer());

	TEST_DEBUG_LOG(TRACE)<< "Test set/get of front/back/removeList pointers:\t\t\t\tSuccess" << std::endl;

	Element<std::shared_ptr<int>>* elementArray[NUMELEMENTS];

	Element<std::shared_ptr<int>> source(std::make_shared<int>(0));

	for (int i = 0; i < NUMELEMENTS; ++i) {
		elementArray[i] = new Element<std::shared_ptr<int>>(std::make_shared<int>(i));
		if (i == 0) {
			continue;
		}
		elementArray[i-1]->setFrontPointer(elementArray[i-1]->getFrontPointer(), elementArray[i]);
		elementArray[i-1]->setBackPointer(elementArray[i-1]->getBackPointer(), &source);
	}

	source.setFrontPointer(source.getFrontPointer(), elementArray[NUMELEMENTS-1]);
	elementArray[NUMELEMENTS-1]->setBackPointer(elementArray[NUMELEMENTS-1]->getBackPointer(), &source);
	GenericPointer<Element<std::shared_ptr<int>>*, true> temp = elementArray[NUMELEMENTS-1]->getBackPointer();

	std::thread threads[NUMELEMENTS];

        std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();

	for (int i = 0; i < NUMELEMENTS-1; ++i) {
		temp = elementArray[NUMELEMENTS-1]->getBackPointer();
		threads[i] = std::thread(insertElement, &source, elementArray[i+1], elementArray[i], &temp, i);
	}

	for (int i = 0; i < NUMELEMENTS-1; ++i) {
		threads[i].join();
	}

        std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();

	Element<std::shared_ptr<int>> *current = &source;
	for (int i = 0; i < NUMELEMENTS; ++i) {
		assert (current->getFrontPointer()() == elementArray[i]);
		current = elementArray[i];
	}

	if (*(current->getPayload()) == NUMELEMENTS-1) {
		TEST_DEBUG_LOG(TRACE)<< "CAS on front pointer of Element<T>:\t\t\t\t\t\tSuccess" << std::endl;
		TEST_DEBUG_LOG(TRACE)<< "Total time taken for CAS operations:\t\t\t\t\t\t" << std::to_string(std::chrono::duration_cast<std::chrono::microseconds>(endTime-startTime).count()) << std::endl;
		TEST_DEBUG_LOG(TRACE)<< "Test uses compare_exchange_weak with release/consume semantics" << std::endl;
	} else {
		TEST_DEBUG_LOG(TRACE)<< "CAS on front pointer of Element<T>:\t\t\t\t\t\tFailed" << std::endl;
	}

	return 0;
}
